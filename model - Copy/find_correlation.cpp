#include <iostream>
#include <fstream>
#include <regex>
using namespace std;


// These need to be updated if the node files ever change the num vertices or faces
// add 1 for zero indexing
#define NUM_MESH_TETS 2955
#define NUM_ARM_POINTS 799
#define NUM_ULNA_POINTS 84
#define NUM_ULNA_FACES 163
#define NUM_RADIUS_POINTS 47
#define NUM_RADIUS_FACES 89
#define NUM_HUMERUS_POINTS 138
#define NUM_HUMERUS_FACES 271
#define NUM_BICEP_POINTS 143
#define NUM_BICEP_FACES 281
#define NUM_TRICEPT_POINTS 164
#define NUM_TRICEP_FACES 323
#define NUM_SKIN_POINTS 237
#define NUM_SKIN_FACES 469

#define NUM_TETGEN_WORDS 10
#define NUM_FACE_WORDS 9

#define OUTPUT_FILE "tet_correlation.txt"
#define OUTPUT_TRI_FILE "tri_correlation.txt"

char mesh_file[FILENAME_MAX] = "arm.2.mesh";
char arm_file[FILENAME_MAX] = "arm.2.node";
char skin_file[FILENAME_MAX] = "arm.1.node";
char skin_face_file[FILENAME_MAX] = "arm.1.face";
char ulna_file[FILENAME_MAX] = "R_Ulna.1.node";
char ulna_face_file[FILENAME_MAX] = "R_Ulna.1.face";
char radius_file[FILENAME_MAX] = "R_Radius.1.node";
char radius_face_file[FILENAME_MAX] = "R_Radius.1.face";
char humerus_file[FILENAME_MAX] = "R_Humerus.1.node";
char humerus_face_file[FILENAME_MAX] = "R_Humerus.1.face";
char bicep_file[FILENAME_MAX] = "R_bicep.1.node";
char bicep_face_file[FILENAME_MAX] = "R_bicep.1.face";
char tricep_file[FILENAME_MAX] = "R_Tricep.1.node";
char tricep_face_file[FILENAME_MAX] = "R_Tricep.1.face";
regex space_regex("\\s+");
regex slash_regex("\\/");
regex tet_regex("\\Tetrahedra");
regex corner_regex("\\Corners");
string all_points[NUM_ARM_POINTS];

vector<string> readInFile(char file_name[], regex regex) {
	string input_line;
	ifstream file_in(file_name);

	vector<string> file;
	string content((istreambuf_iterator<char>(file_in)),
		(istreambuf_iterator<char>()));
	file_in.close();
	copy(sregex_token_iterator(content.begin(), content.end(), regex, -1),
		sregex_token_iterator(), back_inserter(file));
	return file;
}

void parseMeshFile(string lines[])
{
	vector<string> file = readInFile(mesh_file, tet_regex);
	string tet_info = file.at(2);
	vector<string> split_corners;
	copy(sregex_token_iterator(tet_info.begin(), tet_info.end(), corner_regex, -1),
		sregex_token_iterator(), back_inserter(split_corners));
	string tet_section = split_corners.at(0);
	vector<string> tets;
	copy(sregex_token_iterator(tet_section.begin(), tet_section.end(), space_regex, -1),
		sregex_token_iterator(), back_inserter(tets));

	int index = 1;
	for (int i = 2; i < tets.size(); i = i + 5)
	{
		string vertices = tets.at(i).append("/").append(tets.at(i + 1)).append("/").append(tets.at(i + 2)).append("/").append(tets.at(i + 3));
		lines[index] = vertices;
		index++;
	}
}

void parseNodeFile(char file_name[], string lines[]) {
	vector<string> file = readInFile(file_name, space_regex);
	for (int i = 4; i < file.size() - NUM_TETGEN_WORDS; i = i + 4)
	{
		int index = stoi(file.at(i));
		string vertices = file.at(i + 1).append("/").append(file.at(i + 2)).append("/").append(file.at(i + 3));
		lines[index] = vertices;
	}
}

void findOuterFaces(char file_name[], string lines[]) {
	vector<string> file = readInFile(file_name, space_regex);
	for (int i = 2; i < file.size() - NUM_FACE_WORDS; i = i + 5)
	{
		if (stoi(file.at(i + 4)) == 1) {
			int index = stoi(file.at(i));
			string vertices = file.at(i + 1).append("/").append(file.at(i + 2)).append("/").append(file.at(i + 3));
			lines[index] = vertices;
		}
	}
}

void outputIndicesToFile(ofstream &file_out, vector<int> correlation) {
	for (int j = 0; j < correlation.size(); j++)
	{
		file_out << correlation.at(j);
		file_out << " ";
	}
}

void outputTrianglesToFile(ofstream &file_out, vector<vector<int>> triangles) {
	for (int j = 0; j < triangles.size(); j++)
	{
		vector<int> current = triangles.at(j);

			file_out << current[0];
			file_out << " ";
			file_out << current[1];
			file_out << " ";
			file_out << current[2] << endl;
	}

}

int findClosestPoint(string referencePoint) {
	int closestPoint = 100000000;
	int closestDistance = 10000;
	vector<string> reference;
	copy(sregex_token_iterator(referencePoint.begin(), referencePoint.end(), slash_regex, -1),
		sregex_token_iterator(), back_inserter(reference));
	for (int i = 1; i < NUM_ARM_POINTS; i++) {
		string point = all_points[i];
		vector<string> vals;
		copy(sregex_token_iterator(point.begin(), point.end(), slash_regex, -1),
			sregex_token_iterator(), back_inserter(vals));
		int distance = sqrt(pow(stoi(reference[0]) - stoi(vals[0]), 2) + pow(stoi(reference[1]) - stoi(vals[1]), 2) + pow(stoi(reference[2]) - stoi(vals[2]), 2));
		if (distance < closestDistance) {
			closestDistance = distance; 
			closestPoint = i;
		}
	}
	return closestPoint;
	
}

vector<int> populateTriangles(string faces, vector<vector<int>> correlation, string points[]) {
		vector<string> nums;
		copy(sregex_token_iterator(faces.begin(), faces.end(), slash_regex, -1),
			sregex_token_iterator(), back_inserter(nums));
		vector<int> currentTri;
		int first_index = stoi(nums.at(0));
		int second_index = stoi(nums.at(1));
		int third_index = stoi(nums.at(2));
		if (nums.at(0) != "") {
			int first = 0;
			int second = 0;
			int third = 0;
			for (int j = 0; j < correlation.size(); j++) {
				if (first == 0 || second == 0 || third == 0) {
					vector<int> currentIndices = correlation.at(j);
					if (first_index == currentIndices[1]) {
						first = currentIndices[0];
					}
					if (second_index == currentIndices[1]) {
						second = currentIndices[0];
					}
					if (third_index == currentIndices[1]) {
						third = currentIndices[0];
					}
				}
			}
			if (first == 0) {
				string point = points[first_index];
				first = findClosestPoint(point);
			}
			if (second == 0) {
				string point = points[second_index];
				second = findClosestPoint(point);
			}
			if (third == 0) {
				string point = points[third_index];
				third = findClosestPoint(point);
				
			}
			currentTri.push_back(first);
			currentTri.push_back(second);
			currentTri.push_back(third);
		}
		return currentTri;
}

bool isIn(int num, vector<int> correlation)
{
	if (find(correlation.begin(), correlation.end(), num) != correlation.end())
	{
		return true;
	} else
	{
		return false;
	}
}

bool findTetIn(vector<string> nums, vector<int> correlation)
{
	if(isIn(stoi(nums.at(0)), correlation) && isIn(stoi(nums.at(1)), correlation) && isIn(stoi(nums.at(2)), correlation) && isIn(stoi(nums.at(3)), correlation))
	{
		return true;
	} else
	{
		return false;
	}
}

int main() {
	string mesh_tets[NUM_MESH_TETS];

	string arm_points[NUM_ARM_POINTS];
	string ulna_points[NUM_ULNA_POINTS];
	string radius_points[NUM_RADIUS_POINTS];
	string humerus_points[NUM_HUMERUS_POINTS];
	string bicep_points[NUM_BICEP_POINTS];
	string tricep_points[NUM_TRICEPT_POINTS];
	string skin_points[NUM_SKIN_POINTS];

	string ulna_faces[NUM_ULNA_FACES];
	string radius_faces[NUM_RADIUS_FACES];
	string humerus_faces[NUM_HUMERUS_FACES];
	string bicep_faces[NUM_BICEP_FACES];
	string tricep_faces[NUM_TRICEP_FACES];
	string skin_faces[NUM_SKIN_FACES];

	parseMeshFile(mesh_tets);

	parseNodeFile(arm_file, arm_points);
	parseNodeFile(ulna_file, ulna_points);
	parseNodeFile(radius_file, radius_points);
	parseNodeFile(humerus_file, humerus_points);
	parseNodeFile(bicep_file, bicep_points);
	parseNodeFile(tricep_file, tricep_points);
	parseNodeFile(skin_file, skin_points);

	findOuterFaces(radius_face_file, radius_faces);
	findOuterFaces(ulna_face_file, ulna_faces);
	findOuterFaces(humerus_face_file, humerus_faces);
	findOuterFaces(bicep_face_file, bicep_faces);
	findOuterFaces(tricep_face_file, tricep_faces);
	findOuterFaces(skin_face_file, skin_faces);

	vector<vector<int>> ulna_correlation;
	vector<vector<int>> radius_correlation;
	vector<vector<int>> humerus_correlation;
	vector<vector<int>> bicep_correlation;
	vector<vector<int>> tricep_correlation;
	vector<vector<int>> skin_correlation;

	vector<int> ulna_new_points;
	vector<int> radius_new_points;
	vector<int> humerus_new_points;
	vector<int> bicep_new_points;
	vector<int> tricep_new_points;
	vector<int> skin_new_points;

	for (int i = 1; i < NUM_ARM_POINTS; i++)
	{
		all_points[i] = arm_points[i];
		for (int j = 1; j < NUM_ULNA_POINTS; j++)
		{
			if (arm_points[i] == ulna_points[j]) {
				ulna_correlation.push_back({ i,j });
				ulna_new_points.push_back(i);
			}
		}
		for (int j = 1; j < NUM_RADIUS_POINTS; j++)
		{
			if (arm_points[i] == radius_points[j]) {
				radius_correlation.push_back({ i,j });
				radius_new_points.push_back(i);
			}
		}
		for (int j = 1; j < NUM_HUMERUS_POINTS; j++)
		{
			if (arm_points[i] == humerus_points[j]) {
				humerus_correlation.push_back({ i,j });
				humerus_new_points.push_back(i);
			}
		}
		for (int j = 1; j < NUM_BICEP_POINTS; j++)
		{
			if (arm_points[i] == bicep_points[j]) {
				bicep_correlation.push_back({ i,j });
				bicep_new_points.push_back(i);
			}
		}
		for (int j = 1; j < NUM_TRICEPT_POINTS; j++)
		{
			if (arm_points[i] == tricep_points[j]) {
				tricep_correlation.push_back({ i,j });
				tricep_new_points.push_back(i);
			}
		}

		for (int j = 1; j < NUM_SKIN_POINTS; j++)
		{
			if (arm_points[i] == skin_points[j]) {
				skin_correlation.push_back({ i,j });
				skin_new_points.push_back(i);
			}
		}


	}

	vector<int> radius_tets;
	vector<int> humerus_tets;
	vector<int> ulna_tets;
	vector<int> bicep_tets;
	vector<int> tricep_tets;
	vector<int> skin_tets;

	for(int i = 1; i < NUM_MESH_TETS; i++)
	{
		vector<string> nums;
		copy(sregex_token_iterator(mesh_tets[i].begin(), mesh_tets[i].end(), slash_regex, -1),
			sregex_token_iterator(), back_inserter(nums));
		if(findTetIn(nums, ulna_new_points)){
			ulna_tets.push_back(i);
		} 
		else if (findTetIn(nums, radius_new_points)) {
			radius_tets.push_back(i);
		}
		else if (findTetIn(nums, humerus_new_points)) {
			humerus_tets.push_back(i);
		}
		else if (findTetIn(nums, bicep_new_points)) {
			bicep_tets.push_back(i);
		}
		else if (findTetIn(nums, tricep_new_points)) {
			tricep_tets.push_back(i);
		}
		else if (findTetIn(nums, skin_new_points)) {
			skin_tets.push_back(i);
		}

	}

	vector<vector<int>> radius_triangles;
	vector<vector<int>> humerus_triangles;
	vector<vector<int>> ulna_triangles;
	vector<vector<int>> bicep_triangles;
	vector<vector<int>> tricep_triangles;
	vector<vector<int>> skin_triangles;

	for (int i = 1; i < NUM_BICEP_FACES; i++) {
		string thing = bicep_faces[1];
		vector<int> triangles = populateTriangles(bicep_faces[i], bicep_correlation, bicep_points);
		bicep_triangles.push_back(triangles);
	}
	for (int i = 1; i < NUM_TRICEP_FACES; i++) {
		vector<int> triangles = populateTriangles(tricep_faces[i], tricep_correlation, tricep_points);
		tricep_triangles.push_back(triangles);
	}
	for (int i = 1; i < NUM_RADIUS_FACES; i++) {
		vector<int> triangles = populateTriangles(radius_faces[i], radius_correlation, radius_points);
		radius_triangles.push_back(triangles);
	}
	for (int i = 1; i < NUM_ULNA_FACES; i++) {
		vector<int> triangles = populateTriangles(ulna_faces[i], ulna_correlation, ulna_points);
		ulna_triangles.push_back(triangles);
	}
	for (int i = 1; i < NUM_HUMERUS_FACES; i++) {
		vector<int> triangles = populateTriangles(humerus_faces[i], humerus_correlation, humerus_points);
		humerus_triangles.push_back(triangles);
	}
	for (int i = 1; i < NUM_SKIN_FACES; i++) {
		vector<int> triangles = populateTriangles(skin_faces[i], skin_correlation, skin_points);
		skin_triangles.push_back(triangles);
	}

	ofstream file_out(OUTPUT_FILE);
	file_out << "# Tet of Ulna" << endl;
	file_out << "Ulna ";
	file_out << ulna_tets.size() << endl;
	outputIndicesToFile(file_out, ulna_tets);
	file_out << endl;
	file_out << "# Tet of Radius" << endl;
	file_out << "Radius ";
	file_out << radius_tets.size() << endl;
	outputIndicesToFile(file_out, radius_tets);
	file_out << endl;
	file_out << "# Tet of Humerus" << endl;
	file_out << "Humerus ";
	file_out << humerus_tets.size() << endl;
	outputIndicesToFile(file_out, humerus_tets);
	file_out << endl;
	file_out << "# Tet of Bicep" << endl;
	file_out << "Bicep ";
	file_out << bicep_tets.size() << endl;
	outputIndicesToFile(file_out, bicep_tets);
	file_out << endl;
	file_out << "# Tet of Tricep" << endl;
	file_out << "Tricep ";
	file_out << tricep_tets.size() << endl;
	outputIndicesToFile(file_out, tricep_tets);
	file_out << endl;
	file_out << "# Tet of Skin" << endl;
	file_out << "Skin ";
	file_out << skin_tets.size() << endl;
	outputIndicesToFile(file_out, skin_tets);
	file_out.close();

	ofstream tri_file_out(OUTPUT_TRI_FILE);
	tri_file_out << "# Triangles in Ulna" << endl;
	tri_file_out << "Ulna ";
	tri_file_out << ulna_triangles.size() << endl;
	outputTrianglesToFile(tri_file_out, ulna_triangles);
	tri_file_out << "# Triangles in Radius" << endl;
	tri_file_out << "Radius ";
	tri_file_out << radius_triangles.size() << endl;
	outputTrianglesToFile(tri_file_out, radius_triangles);
	tri_file_out << "# Triangles in Humerus" << endl;
	tri_file_out << "Humerus ";
	tri_file_out << humerus_triangles.size() << endl;
	outputTrianglesToFile(tri_file_out, humerus_triangles);
	tri_file_out << "# Triangles in Bicep" << endl;
	tri_file_out << "Bicep ";
	tri_file_out << bicep_triangles.size() << endl;
	outputTrianglesToFile(tri_file_out, bicep_triangles);
	tri_file_out << "# Triangles in Tricep" << endl;
	tri_file_out << "Tricep ";
	tri_file_out << tricep_triangles.size() << endl;
	outputTrianglesToFile(tri_file_out, tricep_triangles);
	tri_file_out << "# Triangles in Skin" << endl;
	tri_file_out << "Skin ";
	tri_file_out << skin_triangles.size() << endl;
	outputTrianglesToFile(tri_file_out,skin_triangles);
	tri_file_out.close();

	return 0;
}