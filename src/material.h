#pragma once

#include <Eigen/Dense>
#include "GLSLProgramObject.h"

class Material{
public:
	void setObject2WorldMatrix(const Eigen::Matrix4f &object2WorldMatrix);
	void setModelViewMatrix(const Eigen::Matrix4f &modelViewMatrix);
	void setModelViewProjectionMatrix(const Eigen::Matrix4f &modelViewProjectionMatrix);
	void setNormalTransformMatrix(const Eigen::Matrix4f &normalTransformMatrix);
	void setLightPosition(const Eigen::Vector3f &lightPosigion);
	//TODO to return the corresponding program in transparency. This is working now because frontInit and front Peel are using the same location for attrib
	const GLSLProgramObject *getProgram() const { return mPrograms[0]; } 
	void bind() { mPrograms[0]->bind();}
	virtual void setDiffuseColor(const Eigen::Vector3f &color) = 0;
	virtual void loadMtlFromFile(std::string filename) = 0;
	virtual Material *clone() = 0;

protected:
	Material() {}
	std::vector<GLSLProgramObject *> mPrograms;

	Eigen::Matrix4f mObject2WorldMatrix;
	Eigen::Matrix4f mModelViewMatrix;
	Eigen::Matrix4f mModelViewProjectionMatrix;
	Eigen::Matrix4f mNormalTransformMatrix;
	Eigen::Vector3f mLightPosition;
};

class Wireframe : public Material {
public:
	Wireframe();
	//TODO remove the 2 empty function
	void setDiffuseColor(const Eigen::Vector3f &color) {}
	void loadMtlFromFile(std::string filename) {}
	virtual Material *clone() { return new Wireframe(); }
	void bind() { mProgram->bind(); }
	void unbind() { mProgram->unbind(); }
private:
	GLSLProgramObject *mProgram;
};

class BlinnPhong : public Material {
public:
	BlinnPhong(){}
	BlinnPhong(const BlinnPhong *other);
	BlinnPhong(const std::string &filename);
	~BlinnPhong();

	void bind() { mProgram->bind(); }
	void loadMtlFromFile(std::string filename) override;
	Material *clone() { return new BlinnPhong(this); }

	void setFilename(const std::string &filename) { mFilename = filename; }
	void setName(const std::string &name) { mName = name; }
	void setKa(const Eigen::Vector3f &Ka) { mKa = Ka; }
	void setKd(const Eigen::Vector3f &Kd) { mKd = Kd; }
	void setKs(const Eigen::Vector3f &Ks) { mKs = Ks; }
	void setNs(float Ns) { mNs = Ns; }

	const std::string getFilename() const { return mFilename; }
	const std::string getName() const { return mName; }
	const Eigen::Vector3f getKa() const { return mKa; }
	const Eigen::Vector3f getKd() const { return mKd; }
	const Eigen::Vector3f getKs() const { return mKs; }
	float getNs() const { return mNs; }

	void setDiffuseColor(const Eigen::Vector3f &color) override { mProgram->setUniform("Kd", color); }

protected:
	GLSLProgramObject *mProgram;
	std::string mFilename; //mtl filename
	std::string mName;     //material name
	Eigen::Vector3f mKa;   //ambient color
	Eigen::Vector3f mKd;   //diffuse color
	Eigen::Vector3f mKs;   //specular color
	float mNs;             //specular exponent
};

class TexturedBlinnPhong : public BlinnPhong {
public:
	TexturedBlinnPhong(const std::string &filename);
	TexturedBlinnPhong(const TexturedBlinnPhong *other);
	~TexturedBlinnPhong();

	Material *clone() { return new TexturedBlinnPhong(this); }
	void loadMtlFromFile(std::string filename) override;

	void setMapKd(const std::string &mapKd) { mMapKd = mapKd; }
	void setMapKdID(GLuint mapKdID) { mMapKdId = mapKdID; }
	void setSampler(GLuint sampler) { mSampler = sampler; }

	const std::string getMapKd() const { return mMapKd; }
	GLuint getMapKdID() const { return mMapKdId; }
	GLuint getSampler() const { return mSampler; }

private:
	std::string mMapKd;   //diffuse color texture map
	GLuint mMapKdId;
	GLuint mSampler;
};



class Transparent : public BlinnPhong {
public:
	Transparent();
	~Transparent();
	Material *clone() { return new Transparent(); }

	const GLSLProgramObject *getShaderFrontInit() const { return mShaderFrontInit; }
	const GLSLProgramObject *getShaderFrontPeel() const { return mShaderFrontPeel; }
	const GLSLProgramObject *getShaderFrontBlend() const { return mShaderFrontBlend; }
	const GLSLProgramObject *getShaderFrontFinal() const { return mShaderFrontFinal; }
	const GLSLProgramObject *getShaderWireframe() const { return mShaderWireframe; }

	GLuint getFrontFboId(int i) const { return mFrontFboId[i]; }
	GLuint getFrontDepthTexId(int i) const { return mFrontDepthTexId[i]; }
	GLuint getFrontColorTexId(int i) const { return mFrontColorTexId[i]; }
	GLuint getFrontColorBlenderTexId() const { return mFrontColorBlenderTexId; }
	GLuint getFrontColorBlenderFboId() const { return mFrontColorBlenderFboId; }

	int getNumPasses() const { return mNumPasses; }

private:
	float mOpacity;
	int mNumPasses;

	GLSLProgramObject *mShaderFrontInit;
	GLSLProgramObject *mShaderFrontPeel;
	GLSLProgramObject *mShaderFrontBlend;
	GLSLProgramObject *mShaderFrontFinal;
	GLSLProgramObject *mShaderWireframe;

	GLuint mFrontFboId[2];
	GLuint mFrontDepthTexId[2];
	GLuint mFrontColorTexId[2];
	GLuint mFrontColorBlenderTexId;
	GLuint mFrontColorBlenderFboId;

	void InitFrontPeelingRenderTargets();
	void DeleteFrontPeelingRenderTargets();

};