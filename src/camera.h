#pragma once
#include <Eigen/core>
class MyGLCanvas;

class Camera{
public:
	Camera() = default;
	Camera(MyGLCanvas *canvas);
	~Camera();

	Eigen::Matrix4f getModelViewMatrix() const {return mModelViewMatrix;}
	Eigen::Matrix4f getProjectionMatrix() const {return mProjectionMatrix;}
	Eigen::Matrix4f getModelViewProjectionMatrix() const {return mModelViewProjectionMatrix;}
	Eigen::Vector3f getEye() const {return e;}
	float getDistance() const { return mDistance; }
	float getAspect() const {return mAspect;}
	float getFov() const { return mFov; }
	float getNear() const { return mNear; }
	float getFar() const { return mFar; }

	void setAspect(float aspect) {mAspect = aspect;}
	void setModelViewMatrix();
	void setPerspectiveProjectionMatrix(float fov, float aspect, float n, float f);
	void setModelViewProjectionMatrix(float distanceIncrement);

private:
	MyGLCanvas *mCanvas;
	float mAspect;
	float mFov;
	float mDistance; // distance from eye to origin
	float mNear;
	float mFar;

	Eigen::Vector3f e; //eye position
	Eigen::Vector3f g; //gaze direction
	Eigen::Vector3f t; //view up direction

	Eigen::Matrix4f mModelViewMatrix;
	Eigen::Matrix4f mProjectionMatrix;
	Eigen::Matrix4f mModelViewProjectionMatrix;

};