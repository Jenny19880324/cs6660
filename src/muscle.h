#pragma once
#include <Eigen/core>
#include <vector>
#include <map>
#include "tet.h"
#include "material.h"

class MyGLCanvas;

class Muscle {
public:
	struct Line {
		Eigen::Vector3d start;
		Eigen::Vector3d end;
	};

	Muscle(){ glGenVertexArrays(1, &mVertexArrayObject); mMaterial = new Wireframe();}
	Muscle(const std::vector<Tet> &muscleTets);
	void setMuscleTet(const std::vector<Tet> &muscleTets);
	void drawFiberDirection() const;
	void updateTransformationUniform(const MyGLCanvas *canvas);
	template <typename Matrix> void uploadAttrib(const std::string &name, const Matrix &M, int version = -1);
	void uploadAttrib(const std::string &name, size_t size, int dim,
		uint32_t compSize, GLuint glType, bool integral,
		const void *data, int version);

private:
	int mNumTets;

	Eigen::Matrix3Xd mPositions;
	Eigen::Matrix3Xd mVertexColors;

	Material *mMaterial;
	std::vector<Line> mLines;
	std::vector<Tet> mMuscleTets;

	struct Buffer {
		GLuint id;
		GLuint glType;
		GLuint dim;
		GLuint compSize;
		GLuint size;
		int version;
	};

	GLuint mVertexArrayObject;
	std::map<std::string, Buffer> mBufferObjects;

	void free();
};
