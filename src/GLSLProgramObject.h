#pragma once

#include <nanogui/opengl.h>
#include <Eigen/Geometry>
#include <string>
#include <iostream>
#include <vector>

// bypass template specializations

NAMESPACE_BEGIN(detail)
template <typename T> struct type_traits;
template <> struct type_traits<uint32_t> { enum { type = GL_UNSIGNED_INT, integral = 1 }; };
template <> struct type_traits<int32_t> { enum { type = GL_INT, integral = 1 }; };
template <> struct type_traits<uint16_t> { enum { type = GL_UNSIGNED_SHORT, integral = 1 }; };
template <> struct type_traits<int16_t> { enum { type = GL_SHORT, integral = 1 }; };
template <> struct type_traits<uint8_t> { enum { type = GL_UNSIGNED_BYTE, integral = 1 }; };
template <> struct type_traits<int8_t> { enum { type = GL_BYTE, integral = 1 }; };
template <> struct type_traits<double> { enum { type = GL_DOUBLE, integral = 0 }; };
template <> struct type_traits<float> { enum { type = GL_FLOAT, integral = 0 }; };
NAMESPACE_END(detail)


class GLSLProgramObject
{
public:
	GLSLProgramObject();
	virtual ~GLSLProgramObject();

	void destroy();

	void bind() const;

	void unbind() const;

	void setTextureUnit(std::string texname, int texunit) const;

	void bindTexture(GLenum target, std::string texname, GLuint texid, int texunit) const;

	void bindTexture2D(std::string texname, GLuint texid, int texunit) const {
		bindTexture(GL_TEXTURE_2D, texname, texid, texunit);
	}

	void bindTexture3D(std::string texname, GLuint texid, int texunit) const {
		bindTexture(GL_TEXTURE_3D, texname, texid, texunit);
	}

	void bindTextureRECT(std::string texname, GLuint texid, int texunit) const {
		bindTexture(GL_TEXTURE_RECTANGLE, texname, texid, texunit);
	} 

	void attachVertexShader(std::string filename);

	void attachFragmentShader(std::string filename);

	void link();

	inline GLuint getProgId() const { return mProgramId; }

	inline std::string getProgramName() const { return mProgramName; }
	void setProgramName(const std::string &programName) { mProgramName = programName; }

	/// Return the handle of a named shader attribute (-1 if it does not exist)
	GLint attrib(const std::string &name, bool warn = true) const;

	/// Return the handle of a uniform attribute (-1 if it does not exist)
	GLint uniform(const std::string &name, bool warn = true) const;

	/// Initialize a uniform parameter with a 4x4 matrix (float)
	template <typename T>
	void setUniform(const std::string &name, const Eigen::Matrix<T, 4, 4> &mat, bool warn = true) {
		glUseProgram(mProgramId);
		glUniformMatrix4fv(uniform(name, warn), 1, GL_FALSE, mat.template cast<float>().data());
	}

	/// Initialize a uniform parameter with an integer value
	template <typename T, typename std::enable_if<detail::type_traits<T>::integral == 1, int>::type = 0>
	void setUniform(const std::string &name, T value, bool warn = true) {
		glUseProgram(mProgramId);
		glUniform1i(uniform(name, warn), (int)value);
	}

	/// Initialize a uniform parameter with a floating point value
	template <typename T, typename std::enable_if<detail::type_traits<T>::integral == 0, int>::type = 0>
	void setUniform(const std::string &name, T value, bool warn = true) {
		glUseProgram(mProgramId);
		glUniform1f(uniform(name, warn), (float)value);
	}

	/// Initialize a uniform parameter with a 2D vector (int)
	template <typename T, typename std::enable_if<detail::type_traits<T>::integral == 1, int>::type = 0>
	void setUniform(const std::string &name, const Eigen::Matrix<T, 2, 1>  &v, bool warn = true) {
		glUseProgram(mProgramId);
		glUniform2i(uniform(name, warn), (int)v.x(), (int)v.y());
	}

	/// Initialize a uniform parameter with a 2D vector (float)
	template <typename T, typename std::enable_if<detail::type_traits<T>::integral == 0, int>::type = 0>
	void setUniform(const std::string &name, const Eigen::Matrix<T, 2, 1>  &v, bool warn = true) {
		glUseProgram(mProgramId);
		glUniform2f(uniform(name, warn), (float)v.x(), (float)v.y());
	}

	/// Initialize a uniform parameter with a 3D vector (int)
	template <typename T, typename std::enable_if<detail::type_traits<T>::integral == 1, int>::type = 0>
	void setUniform(const std::string &name, const Eigen::Matrix<T, 3, 1>  &v, bool warn = true) {
		glUseProgram(mProgramId);
		glUniform3i(uniform(name, warn), (int)v.x(), (int)v.y(), (int)v.z());
	}

	/// Initialize a uniform parameter with a 3D vector (float)
	template <typename T, typename std::enable_if<detail::type_traits<T>::integral == 0, int>::type = 0>
	void setUniform(const std::string &name, const Eigen::Matrix<T, 3, 1>  &v, bool warn = true) {
		glUseProgram(mProgramId);
		glUniform3f(uniform(name, warn), (float)v.x(), (float)v.y(), (float)v.z());
	}

	/// Initialize a uniform parameter with a 4D vector (int)
	template <typename T, typename std::enable_if<detail::type_traits<T>::integral == 1, int>::type = 0>
	void setUniform(const std::string &name, const Eigen::Matrix<T, 4, 1>  &v, bool warn = true) {
		glUseProgram(mProgramId);
		glUniform4i(uniform(name, warn), (int)v.x(), (int)v.y(), (int)v.z(), (int)v.w());
	}

	/// Initialize a uniform parameter with a 4D vector (float)
	template <typename T, typename std::enable_if<detail::type_traits<T>::integral == 0, int>::type = 0>
	void setUniform(const std::string &name, const Eigen::Matrix<T, 4, 1>  &v, bool warn = true) {
		glUseProgram(mProgramId);
		glUniform4f(uniform(name, warn), (float)v.x(), (float)v.y(), (float)v.z(), (float)v.w());
	}

	
protected:
	std::vector<GLuint>		mVertexShaders;
	std::vector<GLuint>		mFragmentShaders;
	GLuint					mProgramId;
	std::string             mProgramName;
};
