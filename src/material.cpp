#include <iostream>
#include <fstream>
#include <nanogui/opengl.h>
#include <stb_image.h>
#include "material.h"
#include "lodepng.h"


extern Eigen::Vector3f g_backgroundColor;

void Material::setObject2WorldMatrix(const Eigen::Matrix4f &object2WorldMatrix) {
	mObject2WorldMatrix = object2WorldMatrix;
	for (auto program : mPrograms) {
		program->bind();
		program->setUniform("object2World", mObject2WorldMatrix);
	}
};

void Material::setModelViewMatrix(const Eigen::Matrix4f &modelViewMatrix) {
	mModelViewMatrix = modelViewMatrix;
	for (auto program : mPrograms) {
		program->bind();
		program->setUniform("modelView", mModelViewMatrix);
	}
};

void Material::setModelViewProjectionMatrix(const Eigen::Matrix4f &modelViewProjectionMatrix) {
	mModelViewProjectionMatrix = modelViewProjectionMatrix;
	for (auto program : mPrograms) {
		program->bind();
		program->setUniform("modelViewProjection", mModelViewProjectionMatrix);
	}
}

void Material::setNormalTransformMatrix(const Eigen::Matrix4f &normalTransformMatrix) {
	mNormalTransformMatrix = normalTransformMatrix;
	for (auto program : mPrograms) {
		program->bind();
		program->setUniform("normalTransform", mNormalTransformMatrix);
	}
}

void Material::setLightPosition(const Eigen::Vector3f &lightPosition) {
	mLightPosition = lightPosition; 
	for (auto program : mPrograms) {
		program->bind();
		program->setUniform("lightPosition", mLightPosition);
	}
}

BlinnPhong::BlinnPhong(const std::string &filename) {
	loadMtlFromFile(filename);
	mProgram = new GLSLProgramObject();
	mProgram->attachVertexShader("../glsl/notexture.vert");
	mProgram->attachFragmentShader("../glsl/notexture.frag");
	mProgram->setProgramName("BlinnPhong");
	mProgram->link();
	mProgram->bind();
	mProgram->setUniform("Kd", mKd);
	mProgram->setUniform("Ka", mKa);
	mProgram->setUniform("Ks", mKs);
	mProgram->setUniform("Ns", mNs);
	mPrograms.push_back(mProgram);
}

BlinnPhong::BlinnPhong(const BlinnPhong *other) {
	mProgram = new GLSLProgramObject();
	mFilename = other->mFilename;
	mName = other->mName;
	mKa = other->mKa;
	mKd = other->mKd;
	mKs = other->mKs;
	mNs = other->mNs;

	mProgram->attachVertexShader("../glsl/notexture.vert");
	mProgram->attachFragmentShader("../glsl/notexture.frag");
	mProgram->link();
	mProgram->bind();
	mProgram->setUniform("Kd", mKd);
	mProgram->setUniform("Ka", mKa);
	mProgram->setUniform("Ks", mKs);
	mProgram->setUniform("Ns", mNs);
	mPrograms.push_back(mProgram);
}

BlinnPhong::~BlinnPhong() {
	delete mProgram;
	mPrograms.clear();
}

void BlinnPhong::loadMtlFromFile(std::string filename) {
	std::ifstream file;
	filename = "../model/" + filename;
	file.open(filename);

	if (!file.is_open()) {
		std::cout << "Could not open MTL file " << filename << std::endl;
		exit(1);
	}
	std::string s;
	while (file >> s) {
		if (s.compare("newmtl") == 0) {
			file >> mName;
		}
		else if (s.compare("illum") == 0) {
			int illum;
			file >> illum;
		}
		else if (s.compare("Kd") == 0) {
			float r, g, b;
			file >> r >> g >> b;
			mKd = Eigen::Vector3f(r, g, b);
		}
		else if (s.compare("Ka") == 0) {
			float r, g, b;
			file >> r >> g >> b;
			mKa = Eigen::Vector3f(r, g, b);
		}
		else if (s.compare("Ks") == 0) {
			float r, g, b;
			file >> r >> g >> b;
			mKs = Eigen::Vector3f(r, g, b);
		}
		else if (s.compare("Ns") == 0) {
			file >> mNs;
		}
		else {
			std::string skip;
			file >> skip;
		}
	}
	file.close();
}

TexturedBlinnPhong::TexturedBlinnPhong(const std::string &filename) {
	loadMtlFromFile(filename);
	mProgram = new GLSLProgramObject();
	mProgram->attachVertexShader("../glsl/texture.vert");
	mProgram->attachFragmentShader("../glsl/texture.frag");
	mProgram->setProgramName("TexturedBlinnPhong");
	mProgram->link();
	mProgram->setUniform("Kd", mKd);
	mProgram->setUniform("Ka", mKa);
	mProgram->setUniform("Ks", mKs);
	mProgram->setUniform("Ns", mNs);
	mPrograms.push_back(mProgram);

	unsigned width, height;
	std::vector<unsigned char> mapKdImage;
	unsigned error = lodepng::decode(mapKdImage, width, height, "../model/" + mMapKd);
	if (error) {
		std::cout << "decoder error " << error << ": " << lodepng_error_text(error) << std::endl;
	}

	assert(mapKdImage.size() == width * height * 4);
	GLubyte *mapKdData = (GLubyte *)malloc(sizeof(GLubyte) * width * height * 4);
	for (int i = 0; i < mapKdImage.size(); i++) {
		mapKdData[i] = mapKdImage[i];
	}

	glGenTextures(1, &mMapKdId);
	mProgram->bindTexture2D("map_Kd", mMapKdId, 0);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, mapKdData);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glGenerateMipmap(GL_TEXTURE_2D);

	free(mapKdData);
	glBindTexture(GL_TEXTURE_2D, 0);  //unbind the texture

}

TexturedBlinnPhong::TexturedBlinnPhong(const TexturedBlinnPhong *other) {
}

TexturedBlinnPhong::~TexturedBlinnPhong() {
	delete mProgram;
	mPrograms.clear();
}

void TexturedBlinnPhong::loadMtlFromFile(std::string filename) {
	std::ifstream file;
	filename = "../model/" + filename;
	file.open(filename);

	if (!file.is_open()) {
		std::cout << "Could not open MTL file " << filename << std::endl;
		exit(1);
	}
	std::string s;
	while (file >> s) {
		if (s.compare("newmtl") == 0) {
			file >> mName;
		}
		else if (s.compare("illum") == 0) {
		}
		else if (s.compare("Kd") == 0) {
			float r, g, b;
			file >> r >> g >> b;
			mKd = Eigen::Vector3f(r, g, b);
		}
		else if (s.compare("Ka") == 0) {
			float r, g, b;
			file >> r >> g >> b;
			mKa = Eigen::Vector3f(r, g, b);
		}
		else if (s.compare("Tf") == 0) {
		}
		else if (s.compare("map_Kd") == 0) {
			file >> mMapKd;
		}
		else if (s.compare("Ks") == 0) {
			float r, g, b;
			file >> r >> g >> b;
			mKs = Eigen::Vector3f(r, g, b);
		}
		else if (s.compare("Ns") == 0) {
			file >> mNs;
		}
		else {
			std::string skip;
			file >> skip;
		}
	}
	file.close();
}

Transparent::Transparent() :mNumPasses(10), mOpacity(0.6) {
	//font to back depth peeling
	mShaderFrontInit = new GLSLProgramObject();
	mShaderFrontPeel = new GLSLProgramObject();
	mShaderFrontBlend = new GLSLProgramObject();
	mShaderFrontFinal = new GLSLProgramObject();
	mShaderWireframe = new GLSLProgramObject();

	mShaderFrontInit->attachVertexShader("../glsl/init.vert");
	mShaderFrontInit->attachFragmentShader("../glsl/init.frag");
	mShaderFrontPeel->attachVertexShader("../glsl/peel.vert");
	mShaderFrontPeel->attachFragmentShader("../glsl/peel.frag");
	mShaderFrontBlend->attachVertexShader("../glsl/blend.vert");
	mShaderFrontBlend->attachFragmentShader("../glsl/blend.frag");
	mShaderFrontFinal->attachVertexShader("../glsl/final.vert");
	mShaderFrontFinal->attachFragmentShader("../glsl/final.frag");
	mShaderWireframe->attachVertexShader("../glsl/wireframe.vert");
	mShaderWireframe->attachFragmentShader("../glsl/wireframe.frag");

	mShaderFrontInit->setProgramName("Init");
	mShaderFrontPeel->setProgramName("Peel");
	mShaderFrontBlend->setProgramName("Blend");
	mShaderFrontFinal->setProgramName("Final");
	mShaderWireframe->setProgramName("wireframe");

	mShaderFrontInit->link();
	mShaderFrontInit->setUniform("Ns", 18.00);
	mShaderFrontInit->setUniform("Ka", Eigen::Vector3f(0.1, 0.1, 0.1));
	mShaderFrontInit->setUniform("Kd", Eigen::Vector3f(1.0, 0.5, 0.5));
	mShaderFrontInit->setUniform("Ks", Eigen::Vector3f(1.0, 1.0, 1.0));
	mShaderFrontInit->setUniform("Alpha", mOpacity);

	mShaderFrontPeel->link();
	mShaderFrontPeel->setUniform("Ns", 18.00);
	mShaderFrontPeel->setUniform("Ka", Eigen::Vector3f(0.1, 0.1, 0.1));
	mShaderFrontPeel->setUniform("Kd", Eigen::Vector3f(1.0, 0.5, 0.5));
	mShaderFrontPeel->setUniform("Ks", Eigen::Vector3f(1.0, 1.0, 1.0));
	mShaderFrontPeel->setUniform("Alpha", mOpacity);

	mShaderFrontBlend->link();
	mShaderFrontFinal->link();
	mShaderFrontFinal->setUniform("BackgroundColor", g_backgroundColor);
	mShaderWireframe->link();
	mPrograms.push_back(mShaderFrontInit);
	mPrograms.push_back(mShaderFrontPeel);
	//mPrograms.push_back(mShaderWireframe);

	InitFrontPeelingRenderTargets();

}

Transparent::~Transparent() {
	DeleteFrontPeelingRenderTargets();
	delete mShaderFrontInit;
	delete mShaderFrontPeel;
	delete mShaderFrontBlend;
	delete mShaderFrontFinal;
	mPrograms.clear();
}

void Transparent::InitFrontPeelingRenderTargets()
{
	int width = 1920; //TODO remove this, use mCanvas.width()
	int height = 1080; //TODO remove this, use mCanvas.height()
	glGenTextures(2, mFrontDepthTexId);
	glGenTextures(2, mFrontColorTexId);
	glGenFramebuffers(2, mFrontFboId);

	for (int i = 0; i < 2; i++)
	{
		glBindTexture(GL_TEXTURE_RECTANGLE, mFrontDepthTexId[i]);
		glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexImage2D(GL_TEXTURE_RECTANGLE, 0, GL_DEPTH_COMPONENT32F,
			width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);

		glBindTexture(GL_TEXTURE_RECTANGLE, mFrontColorTexId[i]);
		glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexImage2D(GL_TEXTURE_RECTANGLE, 0, GL_RGBA, width, height,
			0, GL_RGBA, GL_FLOAT, 0);

		glBindFramebuffer(GL_FRAMEBUFFER, mFrontFboId[i]);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
			GL_TEXTURE_RECTANGLE, mFrontDepthTexId[i], 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
			GL_TEXTURE_RECTANGLE, mFrontColorTexId[i], 0);
		bool complete = (glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);
		assert(complete);
	}

	glGenTextures(1, &mFrontColorBlenderTexId);
	glBindTexture(GL_TEXTURE_RECTANGLE, mFrontColorBlenderTexId);
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_RECTANGLE, 0, GL_RGBA, width, height,
		0, GL_RGBA, GL_FLOAT, 0);


	glGenFramebuffers(1, &mFrontColorBlenderFboId);
	glBindFramebuffer(GL_FRAMEBUFFER, mFrontColorBlenderFboId);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
		GL_TEXTURE_RECTANGLE, mFrontDepthTexId[0], 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
		GL_TEXTURE_RECTANGLE, mFrontColorBlenderTexId, 0);
	bool complete = (glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);
	assert(complete);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Transparent::DeleteFrontPeelingRenderTargets()
{
	glDeleteFramebuffers(2, mFrontFboId);
	glDeleteFramebuffers(1, &mFrontColorBlenderFboId);
	glDeleteTextures(2, mFrontDepthTexId);
	glDeleteTextures(2, mFrontColorTexId);
	glDeleteTextures(1, &mFrontColorBlenderTexId);
}

Wireframe::Wireframe() {
	mProgram = new GLSLProgramObject();
	mProgram->attachVertexShader("../glsl/wireframe.vert");
	mProgram->attachFragmentShader("../glsl/wireframe.frag");
	mProgram->setProgramName("Wireframe");
	mProgram->link();
	mProgram->bind();
	mPrograms.push_back(mProgram);
}

