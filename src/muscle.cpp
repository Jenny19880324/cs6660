#include "muscle.h"
#include "canvas.h"

extern BoundingBox *gBoundingBox;

Muscle::Muscle(const std::vector<Tet> &muscleTets) :mMuscleTets(muscleTets) {
	setMuscleTet(mMuscleTets);
	mMaterial = new Wireframe();
	glGenVertexArrays(1, &mVertexArrayObject);

}

void Muscle::setMuscleTet(const std::vector<Tet> &muscleTets) {
	mPositions.conservativeResize(3, muscleTets.size() * 2);
	mVertexColors.conservativeResize(3, muscleTets.size() * 2);
	mNumTets = muscleTets.size();
	for (int i = 0; i < muscleTets.size(); i++) {
		const Eigen::Vector3d v0 = muscleTets[i].getVertices(0);
		const Eigen::Vector3d v1 = muscleTets[i].getVertices(1);
		const Eigen::Vector3d v2 = muscleTets[i].getVertices(2);
		const Eigen::Vector3d v3 = muscleTets[i].getVertices(3);

		const Eigen::Vector3d center = (v0 + v1 + v2 + v3) / 4.0;
		const Eigen::Vector3d dir = Eigen::Vector3d(
			muscleTets[i].getFiberDirectionX(),
			muscleTets[i].getFiberDirectionY(),
			muscleTets[i].getFiberDirectionZ());
		const Eigen::Vector3d end = center + dir;
		mPositions.col(i * 2 + 0) = center;
		mPositions.col(i * 2 + 1) = end;
		mVertexColors.col(i * 2 + 0) = Eigen::Vector3d(1.0, 0.0, 0.0);
		mVertexColors.col(i * 2 + 1) = Eigen::Vector3d(0.0, 1.0, 0.0);
	}
	uploadAttrib("position", mPositions);
	uploadAttrib("vertexColor", mVertexColors);
}

template <typename Matrix>
void Muscle::uploadAttrib(const std::string &name, const Matrix &M, int version) {
	uint32_t compSize = sizeof(typename Matrix::Scalar);
	GLuint glType = (GLuint)detail::type_traits<typename Matrix::Scalar>::type;
	bool integral = (bool)detail::type_traits<typename Matrix::Scalar>::integral;

	uploadAttrib(name, (uint32_t)M.size(), (int)M.rows(), compSize,
		glType, integral, M.data(), version);
}

void Muscle::uploadAttrib(const std::string &name, size_t size, int dim,
	uint32_t compSize, GLuint glType, bool integral,
	const void *data, int version) {

	int attribID = 0;
	if (name != "indices") {
		attribID = mMaterial->getProgram()->attrib(name);
		if (attribID < 0)
			return;
	}

	GLuint bufferID;
	auto it = mBufferObjects.find(name);
	if (it != mBufferObjects.end()) {
		Buffer &buffer = it->second;
		bufferID = it->second.id;
		buffer.version = version;
		buffer.size = size;
		buffer.compSize = compSize;
	}
	else {
		glGenBuffers(1, &bufferID);
		Buffer buffer;
		buffer.id = bufferID;
		buffer.glType = glType;
		buffer.dim = dim;
		buffer.compSize = compSize;
		buffer.size = size;
		buffer.version = version;
		mBufferObjects[name] = buffer;
	}

	glBindVertexArray(mVertexArrayObject);
	size_t totalSize = size * (size_t)compSize;
	if (name == "indices") {
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferID);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, totalSize, data, GL_STATIC_DRAW);
	}
	else {
		glBindBuffer(GL_ARRAY_BUFFER, bufferID);
		glBufferData(GL_ARRAY_BUFFER, totalSize, data, GL_STATIC_DRAW);
		if (size == 0) {
			glDisableVertexAttribArray(attribID);
		}
		else {
			glEnableVertexAttribArray(attribID);
			glVertexAttribPointer(attribID, dim, glType, integral, 0, 0);
		}
	}
}

void Muscle::drawFiberDirection() const {
	mMaterial->bind();
	glBindVertexArray(mVertexArrayObject);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glDrawArrays(GL_LINES, 0, 2 * mNumTets);
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glBindVertexArray(0);
}

void Muscle::updateTransformationUniform(const MyGLCanvas *canvas) {
	const Camera &camera = canvas->getCamera();
	const nanogui::Arcball &arcball = canvas->getArcball();
	Eigen::Matrix4f object2WorldMatrix = arcball.matrix();
	Eigen::Matrix4f modelViewMatrix = camera.getModelViewMatrix();
	Eigen::Matrix4f modelViewProjectionMatrix = camera.getModelViewProjectionMatrix();
	Eigen::Matrix4f normalTransformMatrix = (modelViewMatrix * object2WorldMatrix).inverse().transpose();

	mMaterial->setObject2WorldMatrix(object2WorldMatrix);
	mMaterial->setModelViewProjectionMatrix(modelViewProjectionMatrix);
}

void Muscle::free() {
	if (mVertexArrayObject) {
		glDeleteVertexArrays(1, &mVertexArrayObject);
		mVertexArrayObject = 0;
	}
}

