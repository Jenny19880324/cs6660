#include <fstream>
#include <string>
#include <iostream>
#include <vector>
#include <Eigen/core>
std::vector<Eigen::Vector3d> node_list;
std::vector<Eigen::Vector3i> facet_list;
std::string dir = "C:\\Users\\SIGlab\\cs6660\\model\\adjusted_model\\";
int numUlnaNodes = 0;
int numRadiusNodes = 0;
int numHumerusNodes = 0;
int numBicepNodes = 0;
int numTricepNodes = 0;
int numSkinNodes = 0;

int numUlnaFaces = 0;
int numRadiusFaces = 0;
int numHumerusFaces = 0;
int numBicepFaces = 0;
int numTricepFaces = 0;
int numSkinFaces = 0;

int loadNodes(const std::string &filename) {
	std::ifstream file;
	std::string full = dir + filename;
	file.open(full);
	if (!file.is_open()) {
		std::cout << "Could not open file" << full << std::endl;
		exit(1);
	}

	std::cout << "Loading " << full << std::endl;

	int numNodes, dimension, numAttributes, numBoundaryMarkers;
	file >> numNodes >> dimension >> numAttributes >> numBoundaryMarkers;
	assert(dimension == 3);
	assert(numAttributes == 0);
	assert(numBoundaryMarkers == 0);

	std::string s;
	for (int i = 0; i < numNodes; i++) {
		int index;
		double x, y, z;
		file >> index >> x >> y >> z;
		node_list.push_back(Eigen::Vector3d(x, y, z));
	}
	file.close();

	return numNodes;
}

int  loadFaces(const std::string &filename) {
	std::ifstream file;
	std::string full = dir + filename;
	file.open(full);
	if (!file.is_open()) {
		std::cout << "Could not open file" << full << std::endl;
		exit(1);
	}

	std::cout << "Loading " << full << std::endl;

	int numFaces, isBoundaryMarkers;
	file >> numFaces >> isBoundaryMarkers;
	assert(isBoundaryMarkers == 1);

	std::string s;
	for (int i = 0; i < numFaces; i++) {
		int index, v1, v2, v3, b;
		file >> index >> v1 >> v2 >> v3 >> b;
		facet_list.push_back(Eigen::Vector3i(v1, v2, v3));
	}
	file.close();
	return numFaces;
}

Eigen::Vector3d computeOnePointInHole(const std::string &filename, int start, int row) {
	std::ifstream file;
	std::string full = dir + filename;
	file.open(full);
	if (!file.is_open()) {
		std::cout << "Could not open file" << full << std::endl;
		exit(1);
	}

	std::cout << "Loading " << full << std::endl;

	int numTet, dimension, boundaryMarker;
	file >> numTet >> dimension >> boundaryMarker;
	int index, i1, i2, i3, i4;
	for (int i = 0; i < row; i++) {
		file >> index >> i1 >> i2 >> i3 >> i4;
	}
	i1--;
	i2--;
	i3--;
	i4--;

	i1 += start;
	i2 += start;
	i3 += start;
	i4 += start;
	return (node_list[i1] + node_list[i2] + node_list[i3] + node_list[i4]) / 4.0;
}

void writeSmesh(const std::string &filename) {
	std::ofstream file;
	file.open(dir + filename);
	if (!file.is_open()) {
		std::cout << "Could not open file" << filename << std::endl;
		exit(1);
	}
	//////////////////////////////////////////////////////////////////////////
	//part 1 node list
	//////////////////////////////////////////////////////////////////////////
	file << "#Part 1 - node list" << std::endl;
	file << node_list.size() << " 3 0 0" << std::endl;
	for (int i = 0; i < node_list.size(); i++) {
		file << i << " " << node_list[i].transpose() << std::endl;
	}


	//////////////////////////////////////////////////////////////////////////
	//part 2 facet list
	//////////////////////////////////////////////////////////////////////////
	int numFaces = 0;
	int numNodes = -1;
	file << std::endl << "#Part 2 - facet list" << std::endl;
	file << facet_list.size() << " 0" << std::endl;
	for (int i = 0; i < numUlnaFaces; i++) {
		Eigen::Vector3i f = facet_list[i] + Eigen::Vector3i(numNodes, numNodes, numNodes);
		file << "3 " << f.transpose() << " 1" << std::endl;
	}
	numFaces += numUlnaFaces;
	numNodes += numUlnaNodes;
	for (int i = numFaces; i < numFaces + numRadiusFaces; i++) {
		Eigen::Vector3i f = facet_list[i] + Eigen::Vector3i(numNodes, numNodes, numNodes);
		file << "3 " << f.transpose() << " 2" << std::endl;
	}
	numFaces += numRadiusFaces;
	numNodes += numRadiusNodes;

	for (int i = numFaces; i < numFaces + numHumerusFaces; i++) {
		Eigen::Vector3i f = facet_list[i] + Eigen::Vector3i(numNodes, numNodes, numNodes);
		file << "3 " << f.transpose() << " 3" << std::endl;
	}

	numFaces += numHumerusFaces;
	numNodes += numHumerusNodes;
	for (int i = numFaces; i < numFaces + numBicepFaces; i++) {
		Eigen::Vector3i f = facet_list[i] + Eigen::Vector3i(numNodes, numNodes, numNodes);
		file << "3 " << f.transpose() << " 4" << std::endl;
	}

	numFaces += numBicepFaces;
	numNodes += numBicepNodes;
	for (int i = numFaces; i < numFaces + numTricepFaces; i++) {
		Eigen::Vector3i f = facet_list[i] + Eigen::Vector3i(numNodes, numNodes, numNodes);
		file << "3 " << f.transpose() << " 5" << std::endl;
	}

	numFaces += numTricepFaces;
	numNodes += numTricepNodes;
	for (int i = numFaces; i < numFaces + numSkinFaces; i++) {
		Eigen::Vector3i f = facet_list[i] + Eigen::Vector3i(numNodes, numNodes, numNodes);
		file << "3 " << f.transpose() << " 6" << std::endl;
	}


	//////////////////////////////////////////////////////////////////////////
	//part 3 hole list
	//////////////////////////////////////////////////////////////////////////
	file << std::endl << "#Part 3 - (volume) hole list" << std::endl;
	file << 0 << std::endl;


	//////////////////////////////////////////////////////////////////////////
	//part 4 region list
	//////////////////////////////////////////////////////////////////////////
	numNodes = 0;
	Eigen::Vector3d pointInUlna = computeOnePointInHole("ulna.1.ele", numNodes, 1);
	numNodes += numUlnaNodes;
	Eigen::Vector3d pointInRadius = computeOnePointInHole("radius.1.ele", numNodes, 1);
	numNodes += numRadiusNodes;
	Eigen::Vector3d pointInHumerus = computeOnePointInHole("humerus.1.ele", numNodes, 1);
	numNodes += numHumerusNodes;
	Eigen::Vector3d pointInBicep = computeOnePointInHole("bicep.1.ele", numNodes, 1);
	numNodes += numBicepNodes;
	Eigen::Vector3d pointInTricep = computeOnePointInHole("tricep.1.ele", numNodes, 1);
	file << std::endl << "#Part 4 - region attributes list" << std::endl;
	file << 5 << std::endl;
	file << "1 " << pointInUlna.transpose() << " 1  -1  #inside ulna" << std::endl;
	file << "2 " << pointInRadius.transpose() << " 2  -1  #inside radius" << std::endl;
	file << "3 " << pointInHumerus.transpose() << " 3  -1  #inside humerus" << std::endl;
	file << "4 " << pointInBicep.transpose() << " 4  -1  #inside bicep" << std::endl;
	file << "5 " << pointInTricep.transpose() << " 5  -1  #inside tricep" << std::endl;
	file.close();
}

int main()
{

	numUlnaNodes = loadNodes("ulna.1.node");
	numRadiusNodes = loadNodes("radius.1.node");
	numHumerusNodes = loadNodes("humerus.1.node");
	numBicepNodes = loadNodes("bicep.1.node");
	numTricepNodes = loadNodes("tricep.1.node");
	numSkinNodes = loadNodes("skin.1.node");



	numUlnaFaces = loadFaces("ulna.1.face");
	numRadiusFaces = loadFaces("radius.1.face");
	numHumerusFaces = loadFaces("humerus.1.face");
	numBicepFaces = loadFaces("bicep.1.face");
	numTricepFaces = loadFaces("tricep.1.face");
	numSkinFaces = loadFaces("skin.1.face");


	writeSmesh("arm.smesh");

	return 0;
}

