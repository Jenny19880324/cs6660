﻿#include <sstream>
#include <nanogui/nanogui.h>
#include <nanogui/window.h>
#include <nanogui/layout.h>
#include <nanogui/label.h>
#include <nanogui/button.h>
#include <nanogui/textbox.h>
#include <nanogui/checkbox.h>
#include <dirent.h>

#include "application.h"
#include "simulation.h"
#include "tetrahedralArmMesh.h"
#include "constraint.h"
#include "mesh.h"
SimulatableMesh *gSimulatableMesh;
SimulatableMesh *gRestposeSimulatableMesh;
ObjSurfaceMesh *gObjSurfaceMesh;
BoundingBox *gBoundingBox;
Simulation *gSimulation;
bool gTurnOnVertices = false;
extern Handle *gHandle;
std::mutex gMutex;
std::condition_variable gIntegrateCondVar;
extern std::condition_variable gRenderCondVar;
bool gDone = false;
bool gIntegrateNotified = false;
extern bool gRenderNotified;

Application::Application() : nanogui::Screen(Eigen::Vector2i(1920, 1080), "Muscle Activation", true, false) {
    using namespace nanogui;

    Window *window = new Window(this, "L_bicep");
    window->setPosition(Vector2i(0, 0));
    window->setLayout(new GroupLayout(0, 0, 0, 0));

    Widget *tools = new Widget(window);
    tools->setLayout(new BoxLayout(Orientation::Horizontal,
                                   Alignment::Middle, 0, 5));

    Button *b = new Button(tools, "vertices");
	b->setFlags(Button::ToggleButton);
	b->setChangeCallback([](bool state) {gTurnOnVertices = state; });

	b = new Button(tools, "restpose");
	b->setFlags(Button::ToggleButton);
	b->setChangeCallback([&](bool state) {mCanvas->setDrawingTarget(state); });

	b = new ToolButton(tools, ENTYPO_ICON_HEART_EMPTY);
	b->setCallback([&]() {mCanvas->setDrawingMode(SKIN_SURFACE); });
	b = new ToolButton(tools, ENTYPO_ICON_EYE);
	b->setCallback([&]() {mCanvas->setDrawingMode(TRANSPARENT); });
	b = new ToolButton(tools, ENTYPO_ICON_LINE_GRAPH);
	b->setCallback([&]() {mCanvas->setDrawingMode(FIBER_DIRECTION); });
	b = new ToolButton(tools, ENTYPO_ICON_TRIANGLE_LEFT);
	b->setCallback([&]() {mCanvas->setDrawingMode(INTERIOR_TETRAHEDRAL); });
	b = new ToolButton(tools, ENTYPO_ICON_TO_START);
	b->setCallback([&]() {gSimulation->reset(); });

	mCanvas = new MyGLCanvas(window);
	mCanvas->setBackgroundColor({ 100, 100, 100, 255 });
	mCanvas->setSize(Eigen::Vector2i(1920, 1020));

	///////////////////////////////////////////////////////////////////////////
	//File Open Tab
	///////////////////////////////////////////////////////////////////////////
	window = new Window(this, "Input Panel");
	window->setPosition(Vector2i(0, 0));
	window->setLayout(new GroupLayout());

	tools = new Widget(window);
	tools->setLayout(new BoxLayout(Orientation::Horizontal, Alignment::Middle, 0, 0));
	b = new Button(tools, "Open");
	b->setCallback([&] {
		std::string fullPath = file_dialog({ { "mesh", "Tetrahedral Mesh" },{ "obj", "Obj file" } }, false);
		std::size_t pathPos = fullPath.find_last_of("\\");
		std::size_t meshPos = fullPath.find(".mesh");
		std::size_t objPos = fullPath.find(".obj");
		std::string directory = fullPath.substr(0, pathPos);
		if (objPos != string::npos) {
			gObjSurfaceMesh = new ObjSurfaceMesh();
			mCanvas->resetCamera();
			gObjSurfaceMesh->updateTransformationUniform(mCanvas);
		}
		if (meshPos != string::npos) {
			std::string triCorrelationFilename = fullPath.substr(pathPos + 1, meshPos - pathPos - 1) + ".tricorrelation";
			DIR *dir;
			struct dirent *ent;
			bool hasTetCorrelation = false;
			bool hasTriCorrelation = false;
			if ((dir = opendir(directory.c_str())) != NULL) {
				while ((ent = readdir(dir)) != NULL) {
					std::string filename = ent->d_name;
					if (filename.compare(triCorrelationFilename) == 0) {
						hasTriCorrelation = true;
					}
				}
			}
			if (hasTriCorrelation) {
				gSimulatableMesh = new TetrahedralArmMesh();
				gSimulatableMesh->loadMeshFromFile(fullPath);
				mCanvas->resetCamera();
				gSimulatableMesh->updateTransformationUniform(mCanvas);

				gRestposeSimulatableMesh = new TetrahedralArmMesh(gSimulatableMesh);
			}
			else {
				gSimulatableMesh = new TetrahedralMesh();
				gSimulatableMesh->loadMeshFromFile(fullPath);
				mCanvas->resetCamera();
				gSimulatableMesh->updateTransformationUniform(mCanvas);

				gRestposeSimulatableMesh = new TetrahedralMesh(gSimulatableMesh);
			}
			gSimulation = new Simulation(gSimulatableMesh);
		}
	});
	b = new Button(tools, "Save");
	b->setCallback([&] {
		if (gObjSurfaceMesh) {
			std::string fullPath = file_dialog({ { "mesh", "Tetrahedral Mesh" },{ "obj", "Obj file" } }, true);
			gObjSurfaceMesh->saveObj2File(fullPath + ".obj");
		}
		if (gSimulatableMesh) {
			gSimulatableMesh->saveObjs();
		}
	});

	///////////////////////////////////////////////////////////////////////////
	//Config File Tab
	///////////////////////////////////////////////////////////////////////////
	window = new Window(this, "Config File");
	window->setPosition(Vector2i(15, 140));
	window->setLayout(new GroupLayout());

	new Label(window, "Config File dialog", "sans-bold");
	tools = new Widget(window);
	tools->setLayout(new BoxLayout(Orientation::Horizontal, Alignment::Middle, 0, 6));
	b = new Button(tools, "Open");
	b->setCallback([&] {
		std::string configFullPath = file_dialog({ { "config", "Configuration file" } }, false);
		if (!gSimulatableMesh) {
			std::cout << "must load mesh first" << std::endl;
			return;
		}
		gSimulatableMesh->loadConfigFromFile(configFullPath);
	});
	b = new Button(tools, "Save");
	b->setCallback([&] {
		std::string configFullPath = file_dialog({ {"config", "Configuration file" } }, true);
		if (!gSimulatableMesh) {
			std::cout << "must load mesh first" << std::endl;
			return;
		}
		gSimulatableMesh->saveConfigToFile(configFullPath);

	});

	///////////////////////////////////////////////////////////////////////////
	//Constitutive Model Tab
	///////////////////////////////////////////////////////////////////////////
		window = new Window(this, "Constitutive Model");
		window->setPosition(Vector2i(15, 180));
		GridLayout *layout =
			new GridLayout(Orientation::Horizontal, 2,
				Alignment::Middle, 15, 5);
		layout->setColAlignment(
		{ Alignment::Maximum, Alignment::Fill });
		layout->setSpacing(0, 10);
		window->setLayout(layout);

		new Label(window, "Material", "sans-bold");
		ComboBox *cb = new ComboBox(window, { "Linear", "Corotated", "STVK", "Neohookean" });
		cb->setSelectedIndex(1); //Corotated
		cb->setCallback([cb](int) {
			int index = cb->selectedIndex();
			if (index == 0) {
				TetConstraint::setConstitutiveModel(LINEAR_ELASTICITY);
			}
			else if (index == 1) {
				TetConstraint::setConstitutiveModel(COROTATED_LINEAR_ELASTICITY);
			}
			else if (index == 2) {
				TetConstraint::setConstitutiveModel(STVK);
			}
			else if (index == 3) {
				TetConstraint::setConstitutiveModel(NEOHOOKEAN_ELASTICITY);
			}
			else {
				TetConstraint::setConstitutiveModel(LINEAR_ELASTICITY);
			}
		});

		{   //set noneMu
			new Label(window, "noneMu", "sans-bold");
			TextBox *textBox = new TextBox(window);
			textBox->setEditable(true);
			textBox->setFixedSize(Vector2i(100, 20));
			textBox->setValue("1");
			textBox->setDefaultValue("1.0");
			textBox->setFontSize(16);
			textBox->setFormat("[0-9]*\\.?[0-9]+");
			textBox->setCallback([textBox](const std::string &str) {
				double mu = atof(str.c_str());
				TetConstraint::setNoneMu(mu);
				return true;
			});
		}

		{   //set noneLambda
			new Label(window, "noneLambda", "sans-bold");
			TextBox *textBox = new TextBox(window);
			textBox->setEditable(true);
			textBox->setFixedSize(Vector2i(100, 20));
			textBox->setValue("1");
			textBox->setDefaultValue("1.0");
			textBox->setFontSize(16);
			textBox->setFormat("[0-9]*\\.?[0-9]+");
			textBox->setCallback([textBox](const std::string &str) {
				double lambda = atof(str.c_str());
				TetConstraint::setNoneLambda(lambda);
				return true;
			});
		}

		{   //set boneMu
			new Label(window, "boneMu", "sans-bold");
			TextBox *textBox = new TextBox(window);
			textBox->setEditable(true);
			textBox->setFixedSize(Vector2i(100, 20));
			textBox->setValue("1000");
			textBox->setDefaultValue("1000.0");
			textBox->setFontSize(16);
			textBox->setFormat("[0-9]*\\.?[0-9]+");
			textBox->setCallback([textBox](const std::string &str) {
				double mu = atof(str.c_str());
				TetConstraint::setBoneMu(mu);
				return true;
			});
		}

		{   //set boneLambda
			new Label(window, "boneLambda", "sans-bold");
			TextBox *textBox = new TextBox(window);
			textBox->setEditable(true);
			textBox->setFixedSize(Vector2i(100, 20));
			textBox->setValue("1000");
			textBox->setDefaultValue("1000.0");
			textBox->setFontSize(16);
			textBox->setFormat("[0-9]*\\.?[0-9]+");
			textBox->setCallback([textBox](const std::string &str) {
				double lambda = atof(str.c_str());
				TetConstraint::setBoneLambda(lambda);
				return true;
			});
		}

		{   //set muscleMu
			new Label(window, "muscleMu", "sans-bold");
			TextBox *textBox = new TextBox(window);
			textBox->setEditable(true);
			textBox->setFixedSize(Vector2i(100, 20));
			textBox->setValue("10");
			textBox->setDefaultValue("10.0");
			textBox->setFontSize(16);
			textBox->setFormat("[0-9]*\\.?[0-9]+");
			textBox->setCallback([textBox](const std::string &str) {
				double mu = atof(str.c_str());
				TetConstraint::setMuscleMu(mu);
				return true;
			});
		}

		{   //set muscleLambda
			new Label(window, "muscleLambda", "sans-bold");
			TextBox *textBox = new TextBox(window);
			textBox->setEditable(true);
			textBox->setFixedSize(Vector2i(100, 20));
			textBox->setValue("10");
			textBox->setDefaultValue("10.0");
			textBox->setFontSize(16);
			textBox->setFormat("[0-9]*\\.?[0-9]+");
			textBox->setCallback([textBox](const std::string &str) {
				double lambda = atof(str.c_str());
				TetConstraint::setMuscleLambda(lambda);
				return true;
			});
		}

		{   //set skinMu
			new Label(window, "skinMu", "sans-bold");
			TextBox *textBox = new TextBox(window);
			textBox->setEditable(true);
			textBox->setFixedSize(Vector2i(100, 20));
			textBox->setValue("1");
			textBox->setDefaultValue("1.0");
			textBox->setFontSize(16);
			textBox->setFormat("[0-9]*\\.?[0-9]+");
			textBox->setCallback([textBox](const std::string &str) {
				double mu = atof(str.c_str());
				TetConstraint::setSkinMu(mu);
				return true;
			});
		}

		{   //set skinLambda
			new Label(window, "skinLambda", "sans-bold");
			TextBox *textBox = new TextBox(window);
			textBox->setEditable(true);
			textBox->setFixedSize(Vector2i(100, 20));
			textBox->setValue("1");
			textBox->setDefaultValue("1.0");
			textBox->setFontSize(16);
			textBox->setFormat("[0-9]*\\.?[0-9]+");
			textBox->setCallback([textBox](const std::string &str) {
				double lambda = atof(str.c_str());
				TetConstraint::setSkinLambda(lambda);
				return true;
			});
		}


	///////////////////////////////////////////////////////////////////////////
	//derivative checker
	///////////////////////////////////////////////////////////////////////////

	{
		window = new Window(this, "Derivative Checker");
		window->setPosition(Vector2i(15, 480));
		window->setLayout(new GroupLayout());
		//derivative checker
		CheckBox *cbg = new CheckBox(window, "gradient checker",
			[](bool state) { gSimulation->setCheckGradient(state); }
		);
		cbg->setChecked(false);
		CheckBox *cbh = new CheckBox(window, "hessian checker",
			[](bool state) { gSimulation->setCheckHessian(state); }
		);
		cbh->setChecked(false);
	}

	///////////////////////////////////////////////////////////////////////////
	//optimization method
	///////////////////////////////////////////////////////////////////////////
	window = new Window(this, "Optimization");
	window->setPosition(Vector2i(15, 580));
	layout =
		new GridLayout(Orientation::Horizontal, 2,
			Alignment::Middle, 15, 5);
	layout->setColAlignment(
	{ Alignment::Maximum, Alignment::Fill });
	layout->setSpacing(0, 10);
	window->setLayout(layout);

	cb = new ComboBox(window, { "Newton's Method", "Projective Dynamics" });
	cb->setSelectedIndex(0); //Newton's Method
	cb->setCallback([cb](int) {
		int index = cb->selectedIndex();
		if (index == 0) {
			gSimulation->setOptimizationMethod(NEWTON);
		}
		else if (index == 1) {
			gSimulation->setOptimizationMethod(PROJECTIVE_DYNAMICS);
		}
	});
    performLayout();
}

bool Application::keyboardEvent(int key, int scancode, int action, int modifiers) {
    if (Screen::keyboardEvent(key, scancode, action, modifiers))
        return true;
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
        setVisible(false);
        return true;
    }
    else if(key == GLFW_KEY_SPACE && action == GLFW_PRESS) {
		if (gIntegrateNotified == false) {
			std::unique_lock<std::mutex> lock(gMutex);
			std::cout << "producing " << std::endl;
			gIntegrateNotified = true;
			gIntegrateCondVar.notify_one();
		}
		else {
 			gIntegrateNotified = false;
			gSimulation->setStop(true);
		}

    }
	else if (key == GLFW_KEY_F && action == GLFW_PRESS) {
		TetrahedralArmMesh *gTetrahedralArmMesh = dynamic_cast<TetrahedralArmMesh *>(gSimulatableMesh);
		gTetrahedralArmMesh->computeFiberDirection();
	}
    return false;
}

void Application::draw(NVGcontext *ctx) {
    /* Draw the user interface */
    Screen::draw(ctx);
}

