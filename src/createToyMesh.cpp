#include <iostream>
#include <fstream>
#include <Eigen/core>
#include <vector>
#include <unordered_set>

int length = 10;
int width = 3;
int height = 3;
int numVertices = (length + 1) * (width + 1) * (height + 1);
int numTetrahedral = length * width *height * 5;;
int numTriangles = length * width * height * 16;

int bone1BeginX = 1, bone1EndX = 4;
int bone1BeginY = 1, bone1EndY = 2;
int bone1BeginZ = 1, bone1EndZ = 2;
int numBone1Cube = (bone1EndX - bone1BeginX) * (bone1EndY - bone1BeginY) * (bone1EndZ - bone1BeginZ);

int bone2BeginX = 6, bone2EndX = 9;
int bone2BeginY = 1, bone2EndY = 2;
int bone2BeginZ = 1, bone2EndZ = 2;
int numBone2Cube = (bone2EndX - bone2BeginX) * (bone2EndY - bone2BeginY) * (bone2EndZ - bone2BeginZ);
std::unordered_set<int> tets;

int inline vertexIndex(int i, int j, int k) {
	return (width + 1) * (length + 1) * k + (length + 1) * j + i;
}

int inline cubeIndex(int i, int j, int k) {
	return width * length * k + length * j + i;
}

void writeMesh() {
	std::ofstream file("../model/toy.mesh", std::ofstream::out);
	file << "MeshVersionFormatted 1" << std::endl << std::endl;
	file << "Dimension" << std::endl << "3" << std::endl << std::endl;

	file << "# Set of mesh vertices" << std::endl;
	file << "Vertices" << std::endl;


	std::vector<Eigen::Vector3d> vertices(numVertices);
	file << numVertices << std::endl;
	for (int k = 0; k <= height; k++) { //length
		for (int j = 0; j <= width; j++) { //width
			for (int i = 0; i <= length; i++) { //height
				vertices[vertexIndex(i, j, k) - 1] = Eigen::Vector3d((double)i, (double)j, (double)k);
				file << vertices[vertexIndex(i, j, k) - 1].transpose() << "    " << "0" << std::endl;
			}
		}
	}

	file << std::endl;
	file << "# Set of Triangles" << std::endl;
	file << "Triangles" << std::endl;
	file << numTriangles << std::endl;
	for (int k = 0; k < height; k++) {
		for (int j = 0; j < width; j++) {
			for (int i = 0; i < length; i++) {
				//first tetrahedron
				file << vertexIndex(i, j, k) << " " << vertexIndex(i + 1, j, k) << " " << vertexIndex(i, j, k + 1);
				if (j == 0) {
					file << " 1" << std::endl;
				}
				else {
					file << " 0" << std::endl;
				}

				file << vertexIndex(i, j, k) << " " << vertexIndex(i, j, k + 1) << " " << vertexIndex(i, j + 1, k);
				if (i == 0) {
					file << " 1" << std::endl;
				}
				else {
					file << " 0" << std::endl;
				}

				file << vertexIndex(i, j, k) << " " << vertexIndex(i, j + 1, k) << " " << vertexIndex(i + 1, j, k);
				if (k == 0) {
					file << " 1" << std::endl;
				}
				else {
					file << " 0" << std::endl;
				}

				file << vertexIndex(i + 1, j, k) << " " << vertexIndex(i, j, k + 1) << " " << vertexIndex(i, j + 1, k) << " 0" << std::endl;


				//second tetrahedra
				file << vertexIndex(i + 1, j + 1, k) << " " << vertexIndex(i, j + 1, k) << " " << vertexIndex(i + 1, j + 1, k + 1);
				if (j + 1 == width) {
					file << " 1" << std::endl;
				}
				else {
					file << " 0" << std::endl;
				}

				file << vertexIndex(i + 1, j + 1, k) << " " << vertexIndex(i + 1, j + 1, k + 1) << " " << vertexIndex(i + 1, j, k);
				if (i + 1 == length) {
					file << " 1" << std::endl;
				}
				else {
					file << " 0" << std::endl;
				}

				file << vertexIndex(i + 1, j + 1, k) << " " << vertexIndex(i + 1, j, k) << " " << vertexIndex(i, j + 1, k);
				if (k == 0) {
					file << " 1" << std::endl;
				}
				else {
					file << " 0" << std::endl;
				}

				file << vertexIndex(i + 1, j + 1, k + 1) << " " << vertexIndex(i, j + 1, k) << " " << vertexIndex(i + 1, j, k) << " 0" << std::endl;

				//third tetrahedra
				file << vertexIndex(i + 1, j, k + 1) << " " << vertexIndex(i, j, k + 1) << " " << vertexIndex(i + 1, j, k);
				if (j == 0) {
					file << " 1" << std::endl;
				}
				else {
					file << " 0" << std::endl;
				}

				file << vertexIndex(i + 1, j, k + 1) << " " << vertexIndex(i + 1, j, k) << " " << vertexIndex(i + 1, j + 1, k + 1);
				if (i + 1 == length) {
					file << " 1" << std::endl;
				}
				else {
					file << " 0" << std::endl;
				}

				file << vertexIndex(i + 1, j, k + 1) << " " << vertexIndex(i + 1, j + 1, k + 1) << " " << vertexIndex(i, j, k + 1);
				if (k + 1 == height) {
					file << " 1" << std::endl;
				}
				else {
					file << " 0" << std::endl;
				}

				file << vertexIndex(i + 1, j, k) << " " << vertexIndex(i + 1, j + 1, k + 1) << " " << vertexIndex(i, j, k + 1) << " 0" << std::endl;

				//fourth tetrahedra
				file << vertexIndex(i, j + 1, k + 1) << " " << vertexIndex(i + 1, j + 1, k + 1) << " " << vertexIndex(i, j + 1, k);
				if (j + 1 == width) {
					file << " 1" << std::endl;
				}
				else {
					file << " 0" << std::endl;
				}

				file << vertexIndex(i, j + 1, k + 1) << " " << vertexIndex(i, j + 1, k) << " " << vertexIndex(i, j, k + 1);
				if (i == 0) {
					file << " 1" << std::endl;
				}
				else {
					file << " 0" << std::endl;
				}

				file << vertexIndex(i, j + 1, k + 1) << " " << vertexIndex(i, j, k + 1) << " " << vertexIndex(i + 1, j + 1, k + 1);
				if (k + 1 == height) {
					file << " 1" << std::endl;
				}
				else {
					file << " 0" << std::endl;
				}

				file << vertexIndex(i, j + 1, k) << " " << vertexIndex(i + 1, j + 1, k + 1) << " " << vertexIndex(i, j, k + 1) << " 0" << std::endl;
			}
		}
	}
	file << std::endl;

	file << "# Set of Tetrahedra" << std::endl;
	file << "Tetrahedra" << std::endl;
	file << numTetrahedral << std::endl;
	for (int k = 0; k < height; k++) {
		for (int j = 0; j < width; j++) {
			for (int i = 0; i < length; i++) {
				if (i >= bone1BeginX && i <= bone1EndX &&
					j >= bone1BeginY && j <= bone1EndY &&
					k >= bone1BeginZ && k <= bone1EndZ) {
					file << vertexIndex(i, j, k) << " " << vertexIndex(i + 1, j, k) << " " << vertexIndex(i, j, k + 1) << " " << vertexIndex(i, j + 1, k) << " 1" << std::endl;
					file << vertexIndex(i + 1, j + 1, k) << " " << vertexIndex(i + 1, j, k) << " " << vertexIndex(i + 1, j + 1, k + 1) << " " << vertexIndex(i, j + 1, k) << " 1" << std::endl;
					file << vertexIndex(i + 1, j, k + 1) << " " << vertexIndex(i + 1, j + 1, k + 1) << " " << vertexIndex(i, j, k + 1) << " " << vertexIndex(i + 1, j, k) << " 1" << std::endl;
					file << vertexIndex(i, j + 1, k + 1) << " " << vertexIndex(i, j, k + 1) << " " << vertexIndex(i + 1, j + 1, k + 1) << " " << vertexIndex(i, j + 1, k) << " 1" << std::endl;
					file << vertexIndex(i, j + 1, k) << " " << vertexIndex(i + 1, j, k) << " " << vertexIndex(i, j, k + 1) << " " << vertexIndex(i + 1, j + 1, k + 1) << " 1" << std::endl;
				}
				else if(i >= bone2BeginX && i <= bone2EndX &&
					j >= bone2BeginY && j <= bone2EndY &&
					k >= bone2BeginZ && k <= bone2EndZ) {
					file << vertexIndex(i, j, k) << " " << vertexIndex(i + 1, j, k) << " " << vertexIndex(i, j, k + 1) << " " << vertexIndex(i, j + 1, k) << " 2" << std::endl;
					file << vertexIndex(i + 1, j + 1, k) << " " << vertexIndex(i + 1, j, k) << " " << vertexIndex(i + 1, j + 1, k + 1) << " " << vertexIndex(i, j + 1, k) << " 2" << std::endl;
					file << vertexIndex(i + 1, j, k + 1) << " " << vertexIndex(i + 1, j + 1, k + 1) << " " << vertexIndex(i, j, k + 1) << " " << vertexIndex(i + 1, j, k) << " 2" << std::endl;
					file << vertexIndex(i, j + 1, k + 1) << " " << vertexIndex(i, j, k + 1) << " " << vertexIndex(i + 1, j + 1, k + 1) << " " << vertexIndex(i, j + 1, k) << " 2" << std::endl;
					file << vertexIndex(i, j + 1, k) << " " << vertexIndex(i + 1, j, k) << " " << vertexIndex(i, j, k + 1) << " " << vertexIndex(i + 1, j + 1, k + 1) << " 2" << std::endl;
				}
				else {
					file << vertexIndex(i, j, k) << " " << vertexIndex(i + 1, j, k) << " " << vertexIndex(i, j, k + 1) << " " << vertexIndex(i, j + 1, k) << " 6" << std::endl;
					file << vertexIndex(i + 1, j + 1, k) << " " << vertexIndex(i + 1, j, k) << " " << vertexIndex(i + 1, j + 1, k + 1) << " " << vertexIndex(i, j + 1, k) << " 6" << std::endl;
					file << vertexIndex(i + 1, j, k + 1) << " " << vertexIndex(i + 1, j + 1, k + 1) << " " << vertexIndex(i, j, k + 1) << " " << vertexIndex(i + 1, j, k) << " 6" << std::endl;
					file << vertexIndex(i, j + 1, k + 1) << " " << vertexIndex(i, j, k + 1) << " " << vertexIndex(i + 1, j + 1, k + 1) << " " << vertexIndex(i, j + 1, k) << " 6" << std::endl;
					file << vertexIndex(i, j + 1, k) << " " << vertexIndex(i + 1, j, k) << " " << vertexIndex(i, j, k + 1) << " " << vertexIndex(i + 1, j + 1, k + 1) << " 6" << std::endl;
				}
			}
		}
	}
	file.close();

}

void writeTetCorrelation() {
	std::ofstream file("../model/toy.tetcorrelation", std::ofstream::out);


	file << "# Tet of Ulna" << std::endl;
	file << "Ulna " << numBone1Cube * 5 << std::endl;
	for (int k = bone1BeginZ; k < bone1EndZ; k++) {
		for (int j = bone1BeginY; j < bone1EndY; j++) {
			for (int i = bone1BeginX; i < bone1EndX; i++) {
				file << cubeIndex(i, j, k) * 5 + 1 << " ";
				file << cubeIndex(i, j, k) * 5 + 2 << " ";
				file << cubeIndex(i, j, k) * 5 + 3 << " ";
				file << cubeIndex(i, j, k) * 5 + 4 << " ";
				file << cubeIndex(i, j, k) * 5 + 5;

				if (k == bone1EndZ - 1 && j == bone1EndY - 1 && i == bone1EndX - 1) {

				}
				else {
					file << " ";
				}
				tets.erase(cubeIndex(i, j, k) * 5 + 1);
				tets.erase(cubeIndex(i, j, k) * 5 + 2);
				tets.erase(cubeIndex(i, j, k) * 5 + 3);
				tets.erase(cubeIndex(i, j, k) * 5 + 4);
				tets.erase(cubeIndex(i, j, k) * 5 + 5);
			}
		}
	}
	file << std::endl;

	file << "# Tet of Radius" << std::endl;
	file << "Radius " << numBone2Cube * 5 << std::endl;
	for (int k = bone2BeginZ; k < bone2EndZ; k++) {
		for (int j = bone2BeginY; j < bone2EndY; j++) {
			for (int i = bone2BeginX; i < bone2EndX; i++) {
				file << cubeIndex(i, j, k) * 5 + 1 << " ";
				file << cubeIndex(i, j, k) * 5 + 2 << " ";
				file << cubeIndex(i, j, k) * 5 + 3 << " ";
				file << cubeIndex(i, j, k) * 5 + 4 << " ";
				file << cubeIndex(i, j, k) * 5 + 5;

				if (k == bone2EndZ - 1 && j == bone2EndY - 1 && i == bone2EndX - 1) {

				}
				else {
					file << " ";
				}
				tets.erase(cubeIndex(i, j, k) * 5 + 1);
				tets.erase(cubeIndex(i, j, k) * 5 + 2);
				tets.erase(cubeIndex(i, j, k) * 5 + 3);
				tets.erase(cubeIndex(i, j, k) * 5 + 4);
				tets.erase(cubeIndex(i, j, k) * 5 + 5);
			}
		}
	}
	file << std::endl;

	file << "# Tet of Skin" << std::endl;
	file << "Skin " << tets.size() << std::endl;
	auto last = tets.end();
	last--;
	for (auto it = tets.begin(); it != tets.end(); it++) {
		if (it == last) {
			file << *it;
		}
		else {
			file << *it << " ";
		}

	}

	file.close();
}

void writeTriCorrelation() {
	std::ofstream file("../model/toy.tricorrelation", std::ofstream::out);
	file << "# Triangles in Ulna" << std::endl;
	int numTrianglesBone1 = (bone1EndX - bone1BeginX) * (bone1EndY - bone1BeginY) * 2 * 2 +
							(bone1EndX - bone1BeginX) * (bone1EndZ - bone1BeginZ) * 2 * 2 +
							(bone1EndY - bone1BeginY) * (bone1EndZ - bone1BeginZ) * 2 * 2;
	file << "Ulna " << numTrianglesBone1 << std::endl;

	for (int i = bone1BeginX; i < bone1EndX; i++) {
		for (int k = bone1BeginZ; k < bone1EndZ; k++) {
			int j = bone1BeginY;
			file << vertexIndex(i, j, k)     << " " << vertexIndex(i + 1, j, k) << " " << vertexIndex(i, j, k + 1) << std::endl;
			file << vertexIndex(i + 1, j, k + 1) << " " << vertexIndex(i, j, k + 1) << " " << vertexIndex(i + 1, j, k) << std::endl;

			j = bone1EndY;
			file << vertexIndex(i, j, k) << " " << vertexIndex(i + 1, j, k) << " " << vertexIndex(i, j, k + 1) << std::endl;
			file << vertexIndex(i + 1, j, k + 1) << " " << vertexIndex(i, j, k + 1) << " " << vertexIndex(i + 1, j, k) << std::endl;
		}
	}

	for (int i = bone1BeginX; i < bone1EndX; i++) {
		for (int j = bone1BeginY; j < bone1EndY; j++) {
			int k = bone1BeginZ;
			file << vertexIndex(i, j, k) << " " << vertexIndex(i, j + 1, k) << " " << vertexIndex(i + 1, j, k) << std::endl;
			file << vertexIndex(i + 1, j + 1, k) << " " << vertexIndex(i + 1, j, k) << " " << vertexIndex(i, j + 1, k) << std::endl;

			k = bone1EndZ;
			file << vertexIndex(i, j, k) << " " << vertexIndex(i + 1, j, k) << " " << vertexIndex(i, j + 1, k) << std::endl;
			file << vertexIndex(i + 1, j + 1, k) << " " << vertexIndex(i, j + 1, k) << " " << vertexIndex(i + 1, j, k) << std::endl;
		}
	}

	for (int j = bone1BeginY; j < bone1EndY; j++) {
		for (int k = bone1BeginZ; k < bone1EndZ; k++) {
			int i = bone1BeginX;
			file << vertexIndex(i, j, k) << " " << vertexIndex(i, j , k + 1) << " " << vertexIndex(i, j + 1, k) << std::endl;
			file << vertexIndex(i, j + 1, k + 1) << " " << vertexIndex(i, j + 1, k) << " " << vertexIndex(i, j, k + 1) << std::endl;

			i = bone1EndX;
			file << vertexIndex(i, j, k) << " " << vertexIndex(i, j + 1, k) << " " << vertexIndex(i, j, k + 1) << std::endl;
			file << vertexIndex(i, j + 1, k + 1) << " " << vertexIndex(i, j, k + 1) << " " << vertexIndex(i, j + 1, k) << std::endl;
		}
	}
	file << std::endl;

	//////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////
	int numTrianglesBone2 = (bone2EndX - bone2BeginX) * (bone2EndY - bone2BeginY) * 2 * 2 +
		(bone2EndX - bone2BeginX) * (bone2EndZ - bone2BeginZ) * 2 * 2 +
		(bone2EndY - bone2BeginY) * (bone2EndZ - bone2BeginZ) * 2 * 2;
	file << "# Triangles in Radius" << std::endl;
	file << "Radius " << numTrianglesBone1 << std::endl;

	for (int i = bone2BeginX; i < bone2EndX; i++) {
		for (int k = bone2BeginZ; k < bone2EndZ; k++) {
			int j = bone2BeginY;
			file << vertexIndex(i, j, k) << " " << vertexIndex(i + 1, j, k) << " " << vertexIndex(i, j, k + 1) << std::endl;
			file << vertexIndex(i + 1, j, k + 1) << " " << vertexIndex(i, j, k + 1) << " " << vertexIndex(i + 1, j, k) << std::endl;

			j = bone2EndY;
			file << vertexIndex(i, j, k) << " " << vertexIndex(i + 1, j, k) << " " << vertexIndex(i, j, k + 1) << std::endl;
			file << vertexIndex(i + 1, j, k + 1) << " " << vertexIndex(i, j, k + 1) << " " << vertexIndex(i + 1, j, k) << std::endl;
		}
	}

	for (int i = bone2BeginX; i < bone2EndX; i++) {
		for (int j = bone2BeginY; j < bone2EndY; j++) {
			int k = bone2BeginZ;
			file << vertexIndex(i, j, k) << " " << vertexIndex(i, j + 1, k) << " " << vertexIndex(i + 1, j, k) << std::endl;
			file << vertexIndex(i + 1, j + 1, k) << " " << vertexIndex(i + 1, j, k) << " " << vertexIndex(i, j + 1, k) << std::endl;

			k = bone2EndZ;
			file << vertexIndex(i, j, k) << " " << vertexIndex(i + 1, j, k) << " " << vertexIndex(i, j + 1, k) << std::endl;
			file << vertexIndex(i + 1, j + 1, k) << " " << vertexIndex(i, j + 1, k) << " " << vertexIndex(i + 1, j, k) << std::endl;
		}
	}

	for (int j = bone2BeginY; j < bone2EndY; j++) {
		for (int k = bone2BeginZ; k < bone2EndZ; k++) {
			int i = bone2BeginX;
			file << vertexIndex(i, j, k) << " " << vertexIndex(i, j, k + 1) << " " << vertexIndex(i, j + 1, k) << std::endl;
			file << vertexIndex(i, j + 1, k + 1) << " " << vertexIndex(i, j + 1, k) << " " << vertexIndex(i, j, k + 1) << std::endl;

			i = bone2EndX;
			file << vertexIndex(i, j, k) << " " << vertexIndex(i, j + 1, k) << " " << vertexIndex(i, j, k + 1) << std::endl;
			file << vertexIndex(i, j + 1, k + 1) << " " << vertexIndex(i, j, k + 1) << " " << vertexIndex(i, j + 1, k) << std::endl;
		}
	}
	file << std::endl;
	//////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////

	file << "# Triangles in Skin" << std::endl;
	int numTrianglesSkin = length * width * 2 * 2 +
		length * height * 2 * 2 +
		width * height * 2 * 2;
	file << "Skin " << numTrianglesSkin << std::endl;

	for (int i = 0; i < length; i++) {
		for (int k = 0; k < height; k++) {
			int j = 0;
			file << vertexIndex(i, j, k) << " " << vertexIndex(i + 1, j, k) << " " << vertexIndex(i, j, k + 1) << std::endl;
			file << vertexIndex(i + 1, j, k + 1) << " " << vertexIndex(i, j, k + 1) << " " << vertexIndex(i + 1, j, k) << std::endl;

			j = width;
			file << vertexIndex(i, j, k) << " " << vertexIndex(i + 1, j, k) << " " << vertexIndex(i, j, k + 1) << std::endl;
			file << vertexIndex(i + 1, j, k + 1) << " " << vertexIndex(i, j, k + 1) << " " << vertexIndex(i + 1, j, k) << std::endl;
		}
	}

	for (int i = 0; i < length; i++) {
		for (int j = 0; j < width; j++) {
			int k = 0;
			file << vertexIndex(i, j, k) << " " << vertexIndex(i, j + 1, k) << " " << vertexIndex(i + 1, j, k) << std::endl;
			file << vertexIndex(i + 1, j + 1, k) << " " << vertexIndex(i + 1, j, k) << " " << vertexIndex(i, j + 1, k) << std::endl;

			k = height;
			file << vertexIndex(i, j, k) << " " << vertexIndex(i + 1, j, k) << " " << vertexIndex(i, j + 1, k) << std::endl;
			file << vertexIndex(i + 1, j + 1, k) << " " << vertexIndex(i, j + 1, k) << " " << vertexIndex(i + 1, j, k) << std::endl;
		}
	}

	for (int j = 0; j < width; j++) {
		for (int k = 0; k < height; k++) {
			int i = 0;
			file << vertexIndex(i, j, k) << " " << vertexIndex(i, j, k + 1) << " " << vertexIndex(i, j + 1, k) << std::endl;
			file << vertexIndex(i, j + 1, k + 1) << " " << vertexIndex(i, j + 1, k) << " " << vertexIndex(i, j, k + 1) << std::endl;

			i = length;
			file << vertexIndex(i, j, k) << " " << vertexIndex(i, j + 1, k) << " " << vertexIndex(i, j, k + 1) << std::endl;
			file << vertexIndex(i, j + 1, k + 1) << " " << vertexIndex(i, j, k + 1) << " " << vertexIndex(i, j + 1, k) << std::endl;
		}
	}

	file.close();
}

int main(int argc, char **argv) {
	//writeMesh();
	//writeTetCorrelation();
	writeTriCorrelation();
}