//
// GLSLProgramObject.cpp - Wrapper for GLSL program objects
//
// Author: Louis Bavoil
// Email: sdkfeedback@nvidia.com
//
// Copyright (c) NVIDIA Corporation. All rights reserved.
////////////////////////////////////////////////////////////////////////////////

#include <fstream>
#include "GLSLProgramObject.h"

static GLuint createShaderFromFile(GLint type, const std::string &filename) {
	auto fileToString = [](const std::string &filename) -> std::string {
		if (filename.empty()) {
			return "";
		}
		std::ifstream t(filename);
		return std::string((std::istreambuf_iterator<char>(t)),
							std::istreambuf_iterator<char>());
	};

	std::string shaderString = fileToString(filename);
	if (shaderString.empty()) {
		return (GLuint)0;
	}
		 
	GLuint id = glCreateShader(type);
	const char *shaderStringConst = shaderString.c_str();
	glShaderSource(id, 1, &shaderStringConst, nullptr);
	glCompileShader(id);

	GLint status;
	glGetShaderiv(id, GL_COMPILE_STATUS, &status);
	if (status != GL_TRUE) {
		char buffer[512];
		std::cerr << "Error while compiling ";
		if (type == GL_VERTEX_SHADER) {
			std::cerr << "vertex shader";
		}
		else if (type == GL_FRAGMENT_SHADER) {
			std::cerr << "fragment shader";
		}
		else if (type == GL_GEOMETRY_SHADER) {
			std::cerr << "geometry shader";
		}
		std::cerr << " \"" << filename << "\"" << std::endl;
		std::cerr << shaderString << std::endl << std::endl;
		glGetShaderInfoLog(id, 512, nullptr, buffer);
		std::cerr << "Error: " << std::endl << buffer << std::endl;
		throw std::runtime_error("Shader compilation failed!");
	}
	return id;
}

GLSLProgramObject::GLSLProgramObject() :
	mProgramId(0)
{
}

GLSLProgramObject::~GLSLProgramObject()
{
	destroy();
}

void GLSLProgramObject::destroy()
{
	for (unsigned i = 0; i < mVertexShaders.size(); i++) {
		glDeleteShader(mVertexShaders[i]);
	}
	for (unsigned i = 0; i < mFragmentShaders.size(); i++) {
		glDeleteShader(mFragmentShaders[i]);
	}
	if (mProgramId != 0) {
		glDeleteProgram(mProgramId);
	}
}

void GLSLProgramObject::attachVertexShader(std::string filename)
{
	GLuint shaderId = createShaderFromFile(GL_VERTEX_SHADER, filename);
	if (shaderId == 0) {
		std::cerr << "Error: Vertex shader failed to compile" << std::endl;
		exit(1);
	}
	mVertexShaders.push_back(shaderId);
}

void GLSLProgramObject::attachFragmentShader(std::string filename)
{
	GLuint shaderId = createShaderFromFile(GL_FRAGMENT_SHADER, filename);
	if (shaderId == 0) {
		std::cerr << "Error: Fragment shader failed to compile" << std::endl;
		exit(1);
	}
	mFragmentShaders.push_back(shaderId);
}

void GLSLProgramObject::link()
{
	mProgramId = glCreateProgram();
    for (unsigned i = 0; i < mVertexShaders.size(); i++) {
        glAttachShader(mProgramId, mVertexShaders[i]);
    }

    for (unsigned i = 0; i < mFragmentShaders.size(); i++) {
        glAttachShader(mProgramId, mFragmentShaders[i]);
    }

    glLinkProgram(mProgramId);

    GLint success = 0;
    glGetProgramiv(mProgramId, GL_LINK_STATUS, &success);

    if (!success) {
        char temp[1024];
        glGetProgramInfoLog(mProgramId, 1024, NULL, temp);
        printf("Failed to link program:\n%s\n", temp);
		exit(1);
    }
}

void GLSLProgramObject::bind() const
{
	glUseProgram(mProgramId);
}

void GLSLProgramObject::unbind() const
{
	glUseProgram(0);
}


void GLSLProgramObject::setTextureUnit(std::string texname, int texunit) const
{
	GLint linked;
	glGetProgramiv(mProgramId, GL_LINK_STATUS, &linked);
	if (linked != GL_TRUE) {
		std::cerr << "Error: setTextureUnit needs program to be linked." << std::endl;
		exit(1);
	}
	GLint id = glGetUniformLocation(mProgramId, texname.c_str());
	if (id == -1) {
		std::cerr << "Warning: Invalid texture " << texname << std::endl;
		return;
	}
	glUniform1i(id, texunit);
}

void GLSLProgramObject::bindTexture(GLenum target, std::string texname, GLuint texid, int texunit) const
{
	glActiveTexture(GL_TEXTURE0 + texunit);
	glBindTexture(target, texid);
	setTextureUnit(texname, texunit);
	glActiveTexture(GL_TEXTURE0);
}


GLint GLSLProgramObject::uniform(const std::string &name, bool warn) const {
	GLint linked;
	glGetProgramiv(mProgramId, GL_LINK_STATUS, &linked);
	if (linked != GL_TRUE) {
		std::cerr << "Error: uniform needs program to be linked." << std::endl;
		exit(1);
	}

	GLint id = glGetUniformLocation(mProgramId, name.c_str());
	if (id == -1 && warn)
		std::cerr << mProgramName << ": warning: did not find uniform " << name << std::endl;
	return id;
}

GLint GLSLProgramObject::attrib(const std::string &name, bool warn) const {
	GLint linked;
	glGetProgramiv(mProgramId, GL_LINK_STATUS, &linked);
	if (linked != GL_TRUE) {
		std::cerr << "Error: attrib needs program to be linked." << std::endl;
		exit(1);
	}

	GLint id = glGetAttribLocation(mProgramId, name.c_str());
	if (id == -1 && warn)
		std::cerr << mProgramName << ": warning: did not find attrib " << name << std::endl;
	return id;
}
