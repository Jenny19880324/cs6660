#pragma once
#undef max
#undef min

#include <Eigen/Core>

class BoundingBox{
public:
	BoundingBox() {
		minVertex << std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), std::numeric_limits<float>::max();
		maxVertex << std::numeric_limits<float>::min(), std::numeric_limits<float>::min(), std::numeric_limits<float>::min();
	}

	void addPoint(double x, double y, double z) {
		if (x < minVertex[0]) minVertex[0] = x;
		if (y < minVertex[1]) minVertex[1] = y;
		if (z < minVertex[2]) minVertex[2] = z;
		if (x > maxVertex[0]) maxVertex[0] = x;
		if (y > maxVertex[1]) maxVertex[1] = y;
		if (z > maxVertex[2]) maxVertex[2] = z;
		minX = minVertex[0];
		minY = minVertex[1];
		minZ = minVertex[2];
		maxX = maxVertex[0];
		maxY = maxVertex[1];
		maxZ = maxVertex[2];
	}

	void add(const BoundingBox &bbx) {
		minVertex[0] = bbx.minVertex[0] < minVertex[0] ? bbx.minVertex[0] : minVertex[0];
		minVertex[1] = bbx.minVertex[1] < minVertex[1] ? bbx.minVertex[1] : minVertex[1];
		minVertex[2] = bbx.minVertex[2] < minVertex[2] ? bbx.minVertex[2] : minVertex[2];

		maxVertex[0] = bbx.maxVertex[0] > maxVertex[0] ? bbx.maxVertex[0] : maxVertex[0];
		maxVertex[1] = bbx.maxVertex[1] > maxVertex[1] ? bbx.maxVertex[1] : maxVertex[1];
		maxVertex[2] = bbx.maxVertex[2] > maxVertex[2] ? bbx.maxVertex[2] : maxVertex[2];
		minX = minVertex[0];
		minY = minVertex[1];
		minZ = minVertex[2];
		maxX = maxVertex[0];
		maxY = maxVertex[1];
		maxZ = maxVertex[2];
	}

	double getXLength() const { return maxVertex(0) - minVertex(0); }
	double getYLength() const { return maxVertex(1) - minVertex(1); }
	double getZLength() const { return maxVertex(2) - minVertex(2); }
	double getMaxLength() const { return std::max(std::max(maxX - minX, maxY - minY), maxZ - minZ); }
	double getMinLength() const { return std::min(std::min(maxX - minX, maxY - minY), maxZ - minZ);}
	Eigen::Vector3d getCenter() const { return (minVertex + maxVertex) / 2; }
	void clear() {
		minVertex << std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), std::numeric_limits<float>::max();
		maxVertex << std::numeric_limits<float>::min(), std::numeric_limits<float>::min(), std::numeric_limits<float>::min();
	}

	double minX, minY, minZ;
	double maxX, maxY, maxZ;

private:
	Eigen::Vector3d minVertex;
	Eigen::Vector3d maxVertex;

};