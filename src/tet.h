#pragma once
#include <Eigen/Core>
#include <vector>

typedef enum {
	NONE,
	ULNA,
	RADIUS,
	HUMERUS,
	BICEP,
	TRICEP,
	SKIN
}TetType;

class Tet {
public:
	Tet(){}
	Tet(TetType type, int indexOfWholeMesh, const Eigen::Vector4i &indices, const std::vector<Eigen::Vector3d> &vertices):
		mIndices(indices), mType(type), mIndexOfWholeMesh(indexOfWholeMesh) {
			for (int i = 0; i < 4; i++) {
				mVertices[i] = vertices[mIndices(i)];
			}
			mTriangles[0] = Eigen::Vector3i(mIndices[0], mIndices[1], mIndices[2]);
			mTriangles[1] = Eigen::Vector3i(mIndices[1], mIndices[2], mIndices[3]);
			mTriangles[2] = Eigen::Vector3i(mIndices[2], mIndices[3], mIndices[0]);
			mTriangles[3] = Eigen::Vector3i(mIndices[3], mIndices[0], mIndices[1]);

			Eigen::Matrix3d matrix;
			matrix.col(0) = mVertices[0] - mVertices[3];
			matrix.col(1) = mVertices[1] - mVertices[3];
			matrix.col(2) = mVertices[2] - mVertices[3];
			mVolume = std::abs(1.0 / 6.0 * matrix.determinant());
		}

	Tet(const Tet &tet) {
		mType = tet.getType();
		mIndexOfWholeMesh = tet.getIndexOfWholeMesh();
		mVolume = tet.getVolume();
		mIndices = tet.getIndices();
		mRegionIndices = tet.getRegionIndices();
		for (int i = 0; i < 4; i++) {
			mVertices[i] = tet.getVertices(i);
			mTriangles[i] = tet.getTriangles(i);
		}
		mFiberDirection = Eigen::Vector3d(
			tet.getFiberDirectionX(),
			tet.getFiberDirectionY(),
			tet.getFiberDirectionZ());
	}

	Tet(const Tet &tet, const Eigen::Vector3d &fiberDirection) {
		mType = tet.getType();
		mIndexOfWholeMesh = tet.getIndexOfWholeMesh();
		mVolume = tet.getVolume();
		mIndices = tet.getIndices();
		mRegionIndices = tet.getRegionIndices();
		for (int i = 0; i < 4; i++) {
			mVertices[i] = tet.getVertices(i);
			mTriangles[i] = tet.getTriangles(i);
		}
		mFiberDirection = fiberDirection;
	}

	int getIndices(int i) const { return mIndices[i]; }
	const Eigen::Vector4i &getIndices() const { return mIndices; }
	const Eigen::Vector3d &getVertices(int i) const { return mVertices[i]; }
	const Eigen::Vector3i & getTriangles(int i) const { return mTriangles[i]; }
	TetType getType() const { return mType; }
	void setType(TetType type) { mType = type; }
	int getIndexOfWholeMesh() const { return mIndexOfWholeMesh; }
	void setIndexOfWholeMesh(int index) { mIndexOfWholeMesh = index; }
	double getVolume() const { return mVolume; }
	void setRegionIndices(const Eigen::Vector4i &regionIndices) { mRegionIndices = regionIndices; }
	const Eigen::Vector4i getRegionIndices() const { return mRegionIndices; }
	const Eigen::Vector3d getFiberDirection() const { return mFiberDirection; }
	void setFiberDirection(double x, double y, double z) { mFiberDirection = Eigen::Vector3d(x, y, z); }
	double getFiberDirectionX() const { return mFiberDirection.x(); }
	double getFiberDirectionY() const { return mFiberDirection.y(); }
	double getFiberDirectionZ() const { return mFiberDirection.z(); }

private:
	TetType mType;
	int mIndexOfWholeMesh;
	double mVolume;
	Eigen::Vector4i mIndices;
	Eigen::Vector4i mRegionIndices;
	Eigen::Vector3d mVertices[4];
	Eigen::Vector3i mTriangles[4];
	Eigen::Vector3d mFiberDirection;
};

inline bool operator==(Tet &lhs, Tet &rhs) {
	if (lhs.getIndices() == rhs.getIndices()) {
		return true;
	}
}

