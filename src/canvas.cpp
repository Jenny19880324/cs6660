#include <nanogui/glutil.h>
#include "canvas.h"
#include "simulation.h"
#include "tetrahedralArmMesh.h"

extern SimulatableMesh *gSimulatableMesh;
extern SimulatableMesh *gRestposeSimulatableMesh;
extern ObjSurfaceMesh *gObjSurfaceMesh;
extern BoundingBox *gBoundingBox;
extern Simulation *gSimulation;
extern bool gTurnOnVertices;
extern std::mutex gMutex;
extern std::condition_variable gIntegrateCondVar;
extern std::condition_variable gRenderCondVar;
extern bool gDone;
extern bool gIntegrateNotified;
extern bool gRenderNotified;


int gSelectedTetrahedralIndex = -1;
Eigen::Vector3f g_backgroundColor;
Handle *gHandle;

float g_opacity = 0.6;

MyGLCanvas::MyGLCanvas(Widget *parent) : nanogui::GLCanvas(parent), mDrawingMode(SKIN_SURFACE), mDrawingTarget(DEFORMED) {
    using namespace nanogui;
    gBoundingBox = new BoundingBox();

    mCamera = Camera(this);
    mArcball.setSize(this->size() * 4);
	mSelectionWindow = new SelectionWindow(this);
	g_backgroundColor = Eigen::Vector3f(backgroundColor().r(), backgroundColor().g(), backgroundColor().b());
}

MyGLCanvas::~MyGLCanvas(){
	delete mSelectionWindow;
}

void MyGLCanvas::drawRestPose() {
	if (gRestposeSimulatableMesh) {
		switch (mDrawingMode) {
			case SKIN_SURFACE: {
				gRestposeSimulatableMesh->drawExteriorSurface();
				break;
			}
			case TRANSPARENT: {
				gRestposeSimulatableMesh->drawTransparent();
				break;
			}
			case FIBER_DIRECTION: {
				TetrahedralArmMesh *gRestposeTetrahedralArmMesh = dynamic_cast<TetrahedralArmMesh *>(gRestposeSimulatableMesh);
				if (gRestposeTetrahedralArmMesh) {
					gRestposeTetrahedralArmMesh->drawFiberDirection();
				}
				break;
			}
			case INTERIOR_TETRAHEDRAL: {
				gRestposeSimulatableMesh->drawTetrahedra();
			}
		}
	}
}
void MyGLCanvas::drawDeformed() {
	if (gSimulatableMesh) {
		if (!gDone) {
			if (!gRenderNotified) {
			}
			else {
				gSimulatableMesh->surfaceMeshCreateAttrib();
				gRenderNotified = false;
			}
		}
		switch (mDrawingMode) {
			case SKIN_SURFACE: {
				gSimulatableMesh->drawExteriorSurface();
				gSimulatableMesh->drawInteriorSurface();
				break;
			}
			case TRANSPARENT: {
				gSimulatableMesh->drawTransparent();
				break;
			}
			case FIBER_DIRECTION: {
				TetrahedralArmMesh *gTetrahedralArmMesh = dynamic_cast<TetrahedralArmMesh *>(gSimulatableMesh);
				if (gTetrahedralArmMesh) {
					gTetrahedralArmMesh->drawFiberDirection();
				}
				break;
			}
			case INTERIOR_TETRAHEDRAL: {
				gSimulatableMesh->drawTetrahedra();
			}
		}
	}
}

void MyGLCanvas::drawGL() {	
    using namespace nanogui;

    glEnable(GL_DEPTH_TEST);

	switch (mDrawingTarget) {
		case RESTPOSE: {
			drawRestPose();
			break;
		}
		case DEFORMED: {
			drawDeformed();
			break;
		}		
	}

	if (gTurnOnVertices) {
		gSimulatableMesh->turnOnVertices();
	}

	if (gObjSurfaceMesh) {
		gObjSurfaceMesh->drawArray();
	}

	if (gHandle) {
		gHandle->draw();
	}

    glDisable(GL_DEPTH_TEST);
}

bool MyGLCanvas::mouseButtonEvent(const Eigen::Vector2i &p, int button, bool down, int modifiers){
    if(button == GLFW_MOUSE_BUTTON_LEFT) {
        if(modifiers == GLFW_MOD_ALT){
			if (down == true) {
				gSelectedTetrahedralIndex = highlightSelectedHandles(p);
				if (gSelectedTetrahedralIndex < 0) {
					mSelectionWindow->setBottomRight(p);
					mSelectionWindow->setRecord(true);
				}
			}
			if (down == false && gSelectedTetrahedralIndex < 0) {
				if (mSelectionWindow->isRecord()) {
					mSelectionWindow->setUpperLeft(p);
					mSelectionWindow->marqueeSelect();
					gSimulatableMesh->highlightSelectedHandles(mSelectionWindow->getSelectedSurfaceIndices());
					mSelectedTetrahedralIndices = mSelectionWindow->getSelectedTetrahedralIndices();
					for (int tetrahedralIndex : mSelectedTetrahedralIndices) {
						gSimulation->addAttachmentConstraint(tetrahedralIndex, gSimulatableMesh->getVertices(tetrahedralIndex));
					}
					mSelectionWindow->setRecord(false);
				}
			}
			if (down == false && gSelectedTetrahedralIndex >= 0) {
				gSimulation->addAttachmentConstraint(gSelectedTetrahedralIndex, gSimulatableMesh->getVertices(gSelectedTetrahedralIndex));
			}
        }
        else if(modifiers == GLFW_MOD_CONTROL) {
			int fixedTetrahedralIndex = -1;
			if (down == true) {
				fixedTetrahedralIndex = fixSelectedHandles(p);
				if (fixedTetrahedralIndex >= 0) {
					std::cout << "fixedTetrahedralIndex = " << fixedTetrahedralIndex << std::endl;
					gSimulation->addAttachmentConstraint(fixedTetrahedralIndex, gSimulatableMesh->getVertices(fixedTetrahedralIndex));
				}
				else {
					mSelectionWindow->setBottomRight(p);
					mSelectionWindow->setRecord(true);
				}
			}
			if (down == false && fixedTetrahedralIndex < 0) {
				if (mSelectionWindow->isRecord()) {
					mSelectionWindow->setUpperLeft(p);
					mSelectionWindow->marqueeSelect();
					gSimulatableMesh->fixSelectedHandles(mSelectionWindow->getSelectedSurfaceIndices());
					mFixedTetrahedralIndices = mSelectionWindow->getSelectedTetrahedralIndices();
					for (int tetrahedralIndex : mFixedTetrahedralIndices) {
						gSimulation->addAttachmentConstraint(tetrahedralIndex, gSimulatableMesh->getVertices(tetrahedralIndex));
					}
					mSelectionWindow->setRecord(false);
				}
			}
        }
        else{
           mArcball.button(p, down); 
		   if (gSimulatableMesh) {
			   gSimulatableMesh->updateTransformationUniform(this);
		   }
		   if (gRestposeSimulatableMesh) {
			   gRestposeSimulatableMesh->updateTransformationUniform(this);
		   }
		   if (gObjSurfaceMesh) {
			   gObjSurfaceMesh->updateTransformationUniform(this);
		   }

        }  
    }
    return true;
}

bool MyGLCanvas::mouseDragEvent(const Eigen::Vector2i &p, const Eigen::Vector2i &rel, int button, int modifier){
    if(modifier == GLFW_MOD_ALT){
		float operationSpeed = gBoundingBox->getMinLength() * 10;
		moveSelectedHandle(Eigen::Vector2f(rel.x() * operationSpeed, rel.y() * operationSpeed));
		moveSelectedHandles(Eigen::Vector2f(rel.x() * operationSpeed, rel.y() * operationSpeed));
    }
	else if (modifier == GLFW_MOD_CONTROL) {

	}
	else {
		mArcball.motion(p);
		mCamera.setModelViewProjectionMatrix(0);
		if (gSimulatableMesh) {
			gSimulatableMesh->updateTransformationUniform(this);
		}
		if (gRestposeSimulatableMesh) {
			gRestposeSimulatableMesh->updateTransformationUniform(this);
		}
		if (gObjSurfaceMesh){
			gObjSurfaceMesh->updateTransformationUniform(this);
		}
	}
    return true;
}

bool MyGLCanvas::scrollEvent(const Eigen::Vector2i &p, const Eigen::Vector2f &rel){
	float operationSpeed = gBoundingBox->getMinLength() * 0.1;
    mCamera.setModelViewProjectionMatrix(-(rel.x() + rel.y()) * operationSpeed); // hard coded constant
	if (gSimulatableMesh) {
		gSimulatableMesh->updateTransformationUniform(this);
	}
	if (gRestposeSimulatableMesh) {
		gRestposeSimulatableMesh->updateTransformationUniform(this);
	}
	if (gObjSurfaceMesh) {
		gObjSurfaceMesh->updateTransformationUniform(this);
	}
    return true;
}

int MyGLCanvas::highlightSelectedHandles(const Eigen::Vector2i &p){
    const Ray *r = computeRayFromClick(p);
    return gSimulatableMesh->highlightSelectedHandles(r);
    delete r;
}

void MyGLCanvas::moveSelectedHandle(const Eigen::Vector2f &rel){
    if(gSelectedTetrahedralIndex == -1){
        return;
    }
    float canvasWidth = (float)this->size().x();
    float canvasHeight = (float)this->size().y();

    Eigen::Vector4f t = Eigen::Vector4f(rel.x() / canvasWidth, -rel.y() / canvasHeight, 0.0, 0.0);
    Eigen::Matrix4f modelViewProjectionMatrix = mCamera.getModelViewProjectionMatrix();
    t = modelViewProjectionMatrix.inverse() * t;
    Eigen::Vector3d translation;
    translation << t.x(), t.y(), t.z();

	gSimulatableMesh->moveVertex(gSelectedTetrahedralIndex, translation);
}

void MyGLCanvas::moveSelectedHandles(const Eigen::Vector2f &rel) {
	if (mSelectedTetrahedralIndices.size() == 0) {
		return;
	}
	float canvasWidth = (float)this->size().x();
	float canvasHeight = (float)this->size().y();

	Eigen::Vector4f t = Eigen::Vector4f(rel.x() / canvasWidth, -rel.y() / canvasHeight, 0.0, 0.0);
	Eigen::Matrix4f modelViewProjectionMatrix = mCamera.getModelViewProjectionMatrix();
	t = modelViewProjectionMatrix.inverse() * t;
	Eigen::Vector3d translation;
	translation << t.x(), t.y(), t.z();

	gSimulatableMesh->moveVertices(mSelectedTetrahedralIndices, translation);
}

int MyGLCanvas::fixSelectedHandles(const Eigen::Vector2i &p){
    const Ray *r = computeRayFromClick(p);
    int index = gSimulatableMesh->fixSelectedHandles(r);
    delete r;
	return index;
}

const Ray *MyGLCanvas::computeRayFromClick(const Eigen::Vector2i &p){
    Eigen::Vector3f e = mCamera.getEye();
    float canvasWidth = (float)this->size().x();
    float canvasHeight = (float)this->size().y();
    float cursorX = (float)p.x();
    float cursorY = canvasHeight - (float)p.y() + 30.0;  //TODO to remove the tile of the window

    float canonicalX = (cursorX - 0.5 * canvasWidth) / canvasWidth * 2; // -1 ~ 1
    float canonicalY = (cursorY - 0.5 * canvasHeight) / canvasHeight * 2;

    float screenX = e.x() + canonicalX * mCamera.getDistance() * tan(mCamera.getFov() * 0.5);
    float screenY = e.y() + canonicalY * mCamera.getDistance() * tan(mCamera.getFov() * 0.5);
    Eigen::Vector3f screenPoint = Eigen::Vector3f(screenX, screenY, 0.0);
    Eigen::Vector3f d = screenPoint - e;
    d.normalize();

    Eigen::Matrix4f object2WorldMatrix = mArcball.matrix();
    Eigen::Vector4f instancedEye = object2WorldMatrix.inverse() * Eigen::Vector4f(e.x(), e.y(), e.z(), 1.0);
    Eigen::Vector4f instancedDir = object2WorldMatrix.inverse() * Eigen::Vector4f(d.x(), d.y(), d.z(), 0.0);

    e = Eigen::Vector3f(instancedEye.x() / instancedEye.w(), instancedEye.y() / instancedEye.w(), instancedEye.z() / instancedEye.w());
    d = Eigen::Vector3f(instancedDir.x(), instancedDir.y(), instancedDir.z());
    Ray *r = new Ray(Eigen::Vector3d(e.x(), e.y(), e.z()), Eigen::Vector3d(d.x(), d.y(), d.z()));
    return r;
}



