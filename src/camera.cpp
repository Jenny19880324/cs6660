#if defined(__APPLE__)
  #include <math.h>
#endif

#if defined(_WIN32)
 #define _USE_MATH_DEFINES
 #include <cmath>
#endif

#include "camera.h"
#include "canvas.h"

extern BoundingBox *gBoundingBox;

Camera::Camera(MyGLCanvas *canvas): mCanvas(canvas), 
e(gBoundingBox->getCenter().cast<float>()),
g(Eigen::Vector3f(0, 0, -1)), 
t(Eigen::Vector3f(0, 1, 0)), 
mAspect(mCanvas->getAspect()), 
mFov(M_PI/3),
mDistance(std::max(gBoundingBox->getXLength(), gBoundingBox->getYLength()) / tan(mFov * 0.5)), 
mModelViewMatrix(Eigen::Matrix4f::Identity()),
mProjectionMatrix(Eigen::Matrix4f::Identity()),
mModelViewProjectionMatrix(Eigen::Matrix4f::Identity()){
	setModelViewProjectionMatrix(0.0f);
}

void Camera::setModelViewMatrix(){
	e(2) = mDistance;

	Eigen::Vector3f w = -g / g.norm();
	Eigen::Vector3f u = t.cross(w) / t.cross(w).norm();
	Eigen::Vector3f v = w.cross(u);

	Eigen::Vector4f u_0;
	Eigen::Vector4f v_0;
	Eigen::Vector4f w_0;
	Eigen::Vector4f e_1;

	u_0 << u, 0;
	v_0 << v, 0;
	w_0 << w, 0;
	e_1 << e, 1;

	Eigen::Matrix4f temp;
	temp << u_0, v_0, w_0, e_1;
	mModelViewMatrix = temp.inverse();
}

Camera::~Camera(){}

void Camera::setPerspectiveProjectionMatrix(float fov, float aspect, float n, float f){
	mNear = n;
	mFar = f;
	const float yScale = 1.0 / tan(fov / 2);
	const float xScale = yScale / aspect;
	mProjectionMatrix << xScale,        0,                0,                   0,
	                          0,   yScale,                0,                   0,
	                          0,        0,(n + f) / (n - f), 2 * n * f / (n - f),
	                          0,        0,               -1,                   0;

	mModelViewProjectionMatrix = mProjectionMatrix * mModelViewMatrix;
}

void Camera::setModelViewProjectionMatrix(float distanceIncrement){
	mDistance += distanceIncrement;
	e(2) = mDistance;
	setModelViewMatrix();
	setPerspectiveProjectionMatrix(mFov, mAspect, 0.1f, 10000);

	//TODO setUniform here
	/*
		Eigen::Matrix4f object2WorldMatrix = mArcball.matrix();
	Eigen::Matrix4f modelViewProjectionMatrix = mCamera.getModelViewProjectionMatrix();
	Eigen::Matrix4f modelViewMatrix = mCamera.getModelViewMatrix();
	Eigen::Matrix4f normalTransformMatrix = (modelViewMatrix * object2WorldMatrix).inverse().transpose();
	Eigen::Vector3f lightPosition(-300, 2000, 0);
	lightPosition = (modelViewMatrix * Eigen::Vector4f(lightPosition(0), lightPosition(1), lightPosition(2), 1.0)).head<3>();*/
}
