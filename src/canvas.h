#pragma once
#include <nanogui/glcanvas.h>
#include <iostream>
#include <string>

// Includes for the GLTexture class.
#include <cstdint>
#include <memory>
#include <utility>

// Includes for multithreading
#include <condition_variable>
#include <mutex>
#include <thread>

#include "mesh.h"
#include "tetrahedralMesh.h"
#include "simulatableMesh.h"
#include "camera.h"

using std::cout;
using std::cerr;
using std::endl;
using std::string;
using std::vector;
using std::pair;
using std::to_string;

extern SimulatableMesh *gSimulatableMesh;
extern BoundingBox *gBoundingBox;


typedef enum {
	SKIN_SURFACE,
	TRANSPARENT,
	FIBER_DIRECTION,
	INTERIOR_TETRAHEDRAL
}DrawingMode;

typedef enum {
	DEFORMED,
	RESTPOSE
}DrawingTarget;

class MyGLCanvas : public nanogui::GLCanvas {
public:
    MyGLCanvas(Widget *parent);
	~MyGLCanvas();

	virtual void drawGL() override;
	virtual bool mouseButtonEvent(const Eigen::Vector2i &p, int button, bool down, int modifers) override;
	virtual bool mouseDragEvent(const Eigen::Vector2i &p, const Eigen::Vector2i &rel, int button, int modifier) override;
	virtual bool scrollEvent(const Eigen::Vector2i &p, const Eigen::Vector2f &rel) override;

    float getAspect() const {return mSize.x() / mSize.y();}
	const Camera &getCamera() const { return mCamera; }
	const nanogui::Arcball &getArcball() const { return mArcball; }
    int highlightSelectedHandles(const Eigen::Vector2i &p);
    void moveSelectedHandles(const Eigen::Vector2f &rel);
	void moveSelectedHandle(const Eigen::Vector2f &rel);
    int fixSelectedHandles(const Eigen::Vector2i &p);
    const Ray *computeRayFromClick(const Eigen::Vector2i &p);
	void resetCamera() { mCamera = Camera(this); }
	void setDrawingMode(DrawingMode drawingMode) { mDrawingMode = drawingMode; }
	void setDrawingTarget(bool drawingTarget) { mDrawingTarget = static_cast<DrawingTarget>(drawingTarget); }
	void drawRestPose();
	void drawDeformed();

private:
    Camera mCamera;
    nanogui::Arcball mArcball;

	class SelectionWindow {
	public:
		SelectionWindow(MyGLCanvas *canvas):mRecord(false), mIsUpperLeftSet(false), mCanvas(canvas), mCamera(canvas->getCamera()) {}
		bool isRecord() const {return mRecord; }
		const Eigen::Vector2i getBottomRight() const { return mBottomRight; }
		const Eigen::Vector2i getUpperLeft() const { return mUpperLeft; }
		bool isUpperLeftSet() const { return mIsUpperLeftSet; }
		const std::vector<int> getSelectedTetrahedralIndices() const { return mSelectedTetrahedralIndices; }
		const std::vector<int> getSelectedSurfaceIndices() const { return mSelectedSurfaceIndices; }
		int getSelectedTetrahedralIndicesSize() const { return mSelectedTetrahedralIndices.size(); }
		int getSelectedSurfaceIndicesSize() const { return mSelectedSurfaceIndices.size(); }
		void setBottomRight(const Eigen::Vector2i &bottomRight) { mBottomRight = bottomRight; }
		void setUpperLeft(const Eigen::Vector2i &upperLeft) { mUpperLeft = upperLeft; mIsUpperLeftSet = true; }
		void setRecord(bool record) { mRecord = record; if (mRecord) { mSelectedTetrahedralIndices.clear(); mSelectedSurfaceIndices.clear(); } }
		void marqueeSelect() {
			//std::cout << "gBoundingBox.min = (" << gBoundingBox->minX << ", " << gBoundingBox->minY << ", " << gBoundingBox->minZ << ")" << std::endl;
			//std::cout << "gBoundingBox.max = (" << gBoundingBox->maxX << ", " << gBoundingBox->maxY << ", " << gBoundingBox->maxZ << ")" << std::endl;
			if (mBottomRight == mUpperLeft) {
				return;
			}
			float fov = mCamera.getFov();
			const Eigen::Vector3f e = Eigen::Vector3f(0, 0, -1 / tan(fov / 2));
			const Eigen::Matrix4f object2WorldMatrix = mCanvas->getArcball().matrix();
			const Eigen::Matrix4f modelViewMatrix = mCamera.getModelViewMatrix() * object2WorldMatrix;
			const Eigen::Matrix4f modelViewMatrixInverse = modelViewMatrix.inverse();
			const float n = mCamera.getNear();
			const float f = mCamera.getFar();
			float halfWidth = (float)mCanvas->width() / 2.0;
			float halfHeight = (float)mCanvas->height() / 2.0;
			const Eigen::Vector3f screenPoints[4] = {
				Eigen::Vector3f((float)mBottomRight.x() / halfWidth - 1.0, -(float)(mBottomRight.y() - 30 ) / halfHeight + 1.0, 0.0), //bottomRight, corresponds to frustum points 0 1
				Eigen::Vector3f((float)mBottomRight.x() / halfWidth - 1.0, -(float)(mUpperLeft.y() - 30) / halfHeight + 1.0, 0.0), //upperRight, corresponds to frustum points 2 3
				Eigen::Vector3f((float)mUpperLeft.x() / halfWidth - 1.0, -(float)(mUpperLeft.y() - 30) / halfHeight + 1.0, 0.0) ,  //upperLeft, corresponds to frustum points 4 5
				Eigen::Vector3f((float)mUpperLeft.x() / halfWidth - 1.0, -(float)(mBottomRight.y() - 30) / halfHeight + 1.0, 0.0) //bottomLeft, correspoints to frustum points 6 7
			};

			for (int i = 0; i < 4; i++) {
				Eigen::Vector3f d = screenPoints[i] - e;
				float t = (n - e.z()) / d.z();
				Eigen::Vector3f temp3 = t * d;
				Eigen::Vector4f temp4 = Eigen::Vector4f(temp3.x(), temp3.y(), -temp3.z(), 1.0);
				temp4 = modelViewMatrixInverse * temp4;
				mFrustumPoints[i * 2] = Eigen::Vector3f(temp4.x() / temp4.w(), temp4.y() / temp4.w(), temp4.z() / temp4.w());
				t = (f - e.z()) / d.z();
				temp3 = t * d;
				temp4 = Eigen::Vector4f(temp3.x(), temp3.y(), -temp3.z(), 1.0);
				temp4 = modelViewMatrixInverse * temp4;
				mFrustumPoints[i * 2 + 1] = Eigen::Vector3f(temp4.x() / temp4.w(), temp4.y() / temp4.w(), temp4.z() / temp4.w());
			}

			for (int i = 0; i < 4; i++) {
				mFrustumNormals[i] = (mFrustumPoints[i * 2] - mFrustumPoints[i * 2 + 1]).cross(
					                  mFrustumPoints[(i * 2 + 2) % 8] - mFrustumPoints[(i * 2 + 3) % 8]);
				mFrustumNormals[i].normalize();
			}

			//Check which points are within the Frustum formed by the selected rectangle
			for (int i = 0; i < gSimulatableMesh->getNumHandles(); i++) {
				int j;
				for (j = 0; j < 4; j++) {
					const Eigen::Vector3d temp = gSimulatableMesh->getHandles(i)->getCenter();
					const Eigen::Vector3f p = Eigen::Vector3f((float)temp.x(), (float)temp.y(), (float)temp.z());
					float fDot = (p - mFrustumPoints[j * 2]).dot(mFrustumNormals[j]);
					if (fDot > 0) {
						break;
					}
				}
				if (j == 4) {
					mSelectedSurfaceIndices.push_back(i);
					mSelectedTetrahedralIndices.push_back(gSimulatableMesh->getHandles(i)->getTetrahedralIndex());
				}
			}
		}

	private:
		bool mRecord;
		bool mIsUpperLeftSet;
		Eigen::Vector2i mBottomRight;
		Eigen::Vector2i mUpperLeft;
		Eigen::Vector3f mFrustumPoints[8];  // 0 2 4 6 are on the near plane and 1 3 5 7 are on the far plane
		Eigen::Vector3f mFrustumNormals[4];
		const MyGLCanvas *mCanvas;
		const Camera &mCamera;
		std::vector<int> mSelectedTetrahedralIndices;
		std::vector<int> mSelectedSurfaceIndices;
	};

	SelectionWindow *mSelectionWindow;
	std::vector<int> mSelectedTetrahedralIndices;
	std::vector<int> mFixedTetrahedralIndices;

	DrawingMode mDrawingMode;
	DrawingTarget mDrawingTarget;
};
