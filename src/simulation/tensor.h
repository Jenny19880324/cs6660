#pragma once
#include <iostream>
#include <Eigen/Core>

class Matrix3333{  //3x3 matrix: each element is a 3x3 matrix
public:
	static Matrix3333 Identity();
	void setZero(); //[0 0 0 ; 0 0 0; 0 0 0]; 0 = 3x3 zeros
	void setIdentity(); 

	//operators
	Eigen::Matrix3d &operator() (int row, int col);
	const Eigen::Matrix3d &operator() (int row, int col) const;
	Matrix3333 operator+(const Matrix3333 &rhs);
	Matrix3333 operator-(const Matrix3333 &rhs);
	Matrix3333 operator*(const Eigen::Matrix3d &rhs);
	Matrix3333 operator*(double rhs);
	Matrix3333 transpose() const;
	Eigen::Matrix3d contract(const Eigen::Matrix3d &rhs);
	Matrix3333 contract(const Matrix3333 &rhs);

	friend std::ostream &operator<<(std::ostream &os, const Matrix3333 &matrix3333);
protected:
	Eigen::Matrix3d mat[3][3];
};
Matrix3333 operator+(const Matrix3333 &lhs, const Matrix3333 &rhs);
Matrix3333 operator-(const Matrix3333 &lhs, const Matrix3333 &rhs);
Matrix3333 operator*(const Eigen::Matrix3d &lhs, const Matrix3333 &rhs);
Matrix3333 operator*(double lhs, const Matrix3333 &rhs);
void directProduct(Matrix3333& dst, const Eigen::Matrix3d &src1, const Eigen::Matrix3d &src2);
