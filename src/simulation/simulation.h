#pragma once
#include <Eigen/Sparse>
#include <vector>
#include <unordered_map>


typedef enum {
	NEWTON,
	PROJECTIVE_DYNAMICS
}Optimization;

typedef enum {
	QUASI_STATIC,
	BACKWARD_EULER
}Numerical;

class SimulatableMesh;
class TetConstraint;
class AttachmentConstraint;

class Simulation{
public:
	Simulation(SimulatableMesh *simulatableMesh);
	~Simulation(){}

	int getSystemDimension() const {return mSystemDimension;}
	const SimulatableMesh *getSimulatableMesh() const {return mSimulatableMesh;}
	SimulatableMesh *getSimulatableMesh() {return mSimulatableMesh;}
	int getNumTetConstraints() const {return mTetConstraints.size();}
	const TetConstraint *getTetConstraint(int i) const {assert(i < mTetConstraints.size()); return mTetConstraints[i];}
	const VectorX &getX() const {return X;}
	const VectorX &getx() const {return x;}

	void setx(const VectorX xx) { x = xx; }
	void setupTetConstraints();
	void addAttachmentConstraint(int index, const Eigen::Vector3d &X);
	double computeEnergy(const VectorX &x) const;
	void precomputation(const VectorX &x);
	void computeGradientHessian(const VectorX &x, VectorX &gradient, std::vector<T> &hTriplets);
	void computeGradientLaplacian(const VectorX &x, VectorX &gradient, std::vector<T> &hTriplets);
	void computeGradient(const VectorX &x, VectorX &gradient);
	void computeHessian(const VectorX &x, std::vector<T> &hTriplets);
	void computeLaplacian(std::vector<T> &lTriplets);
	void integrateQuasiStatic();
	bool integrateQuasiStaticNewton();
	bool integrateQuasiStaticProjectiveDynamics();
	bool integrateImplicitEulerNewton();
    double lineSearch(const VectorX &x, const VectorX &searchDir, const VectorX &localGradientDir);
    void updateVertices();
	void updateMu(TetType tetType);
	void updateLambda(TetType tetType);
    void factorizeLLT(const SpMat &hessian, Eigen::SimplicialLLT<SpMat, Eigen::Upper> &solver);
	bool checkGradientHessian(const VectorX &x, const VectorX &gradient, const SpMat &hessian) const;
	bool checkGradient(const VectorX &x, const VectorX &gradient) const;
	bool checkHessian(const VectorX &x, const SpMat &hessian) const;
	void setCheckGradient(bool state) { mCheckGradient = state; }
	void setCheckHessian(bool state) { mCheckHessian = state; }
	void contractBicep(double alpha);
	void setStop(bool stop) { mStop = stop; }
	void reset();
	void setOptimizationMethod(Optimization optimization) { mOptimization = optimization; }
	void setNumericalMethod(Numerical numerical) { mNumerical = numerical; }



private:
	bool mStop;
	bool mRenderIntermediate;
	bool mCheckGradient;
	bool mCheckHessian;
	Optimization mOptimization;
	Numerical mNumerical;
	unsigned int mIterations;
	int mSystemDimension;
	double h;  //time step
	double hSquare;
	double g; //gravity constant
	VectorX X; //undeformed coordinates
	VectorX x; //deformed coordinates unknown
	VectorX xn; //store previous coordinates
	VectorX y; // y = xn + v * h
	VectorX v;
	VectorX gravity;
	SpMat M;   //mass matrix
	std::vector<TetConstraint *> mTetConstraints; 
	std::unordered_map<int, AttachmentConstraint *> mAttachmentConstraints;
	SimulatableMesh *mSimulatableMesh;
};