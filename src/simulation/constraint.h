#include <Eigen/Sparse>
#include "tensor.h"
#include "tet.h"
#include "typedef.h"
#include "simulation.h"

#define EPSILON 1e-8
extern Simulation *gSimulation;


typedef enum{
	LINEAR_ELASTICITY,
	COROTATED_LINEAR_ELASTICITY,
	STVK,
	NEOHOOKEAN_ELASTICITY
}ConstitutiveModel;

void singularValueDecomposition(const Eigen::Matrix3d &F, Eigen::Matrix3d &U, Eigen::Matrix3d &V, Eigen::Vector3d &Sigma, Eigen::Matrix3d &R);
Matrix3333 computedRdF(const Eigen::Matrix3d &U, const Eigen::Matrix3d &V, const Eigen::Vector3d &Sigma);

class Constraint{
public:
	virtual ~Constraint(){}
	virtual void precomputation(const VectorX &x) = 0;
	virtual double computeEnergy() = 0;
	virtual void computeGradient(VectorX &gradient) = 0;
	virtual void computeHessian(std::vector<T> &hTriplets) = 0;
	virtual void computeLaplacian(std::vector<T> &lTriplets) = 0;
};

class AttachmentConstraint : public Constraint{
public:
	AttachmentConstraint(int index, const Eigen::Vector3d &X);
	~AttachmentConstraint();

	virtual void precomputation(const VectorX &x) override;
	virtual double computeEnergy() override;
	virtual void computeGradient(VectorX &gradient) override;
	virtual void computeHessian(std::vector<T> &hTriplets) override;
	virtual void computeLaplacian(std::vector<T> &lTriplets) override;

	void setXi(const Eigen::Vector3d &X) { Xi = X; }
	static void setStiffness(double stiffness) { mStiffness = stiffness; }
private:
	static double mStiffness;
	int mIndex;
	Eigen::Vector3d Xi;
	Eigen::Vector3d xi;

};

class TetConstraint : public Constraint{
public:
	TetConstraint(const Tet &tet);
	virtual void precomputation(const VectorX &x) override;
	virtual double computeEnergy() override;
	virtual void computeGradient(VectorX &gradient) override;
	virtual void computeHessian(std::vector<T> &hTriplets) override;
	virtual void computeLaplacian(std::vector<T> &lTriplets) override;

	const Eigen::Matrix3d &getF() const {return F;}
	const Eigen::Matrix3d &getR() const {return R;}
	const Eigen::Matrix3d &getU() const {return U;}
	const Eigen::Matrix3d &getV() const {return V;}
	const Eigen::Matrix3d &getH() const {return H;}
	const Eigen::Matrix3d &getP() const {return P;}
	const Eigen::Vector3d &getSigma() const {return Sigma;}
	const Eigen::Matrix3d &getDm() const {return Dm;}
	const Eigen::Matrix3d &getBm() const {return Bm;}
	const Matrix3333 &getdRdF() const {return dRdF;}
	const Matrix3333 &getdPdF() const {return dPdF;}
	const TetType getTetType() const { return mTetType; }

	static void setConstitutiveModel(ConstitutiveModel constitutiveModel) { mConstitutiveModel = constitutiveModel; /*std::cout << "constitutiveModel = " << mConstitutiveModel << std::endl;*/ }
	static void setNoneMu(double mu) { noneMu = mu; gSimulation->updateMu(NONE); }
	static void setNoneLambda(double lambda) { noneLambda = lambda; gSimulation->updateLambda(NONE); }
	static void setBoneMu(double mu) { boneMu = mu; gSimulation->updateMu(ULNA); gSimulation->updateMu(RADIUS); gSimulation->updateMu(HUMERUS);}
	static void setBoneLambda(double lambda) { boneLambda = lambda; gSimulation->updateLambda(ULNA); gSimulation->updateLambda(RADIUS); gSimulation->updateLambda(HUMERUS);}
	static void setMuscleMu(double mu) { muscleMu = mu; gSimulation->updateMu(BICEP); gSimulation->updateMu(TRICEP);}
	static void setMuscleLambda(double lambda) { muscleLambda = lambda;  gSimulation->updateLambda(BICEP); gSimulation->updateLambda(TRICEP); }
	static void setSkinMu(double mu) { muscleMu = mu; gSimulation->updateMu(SKIN);}
	static void setSkinLambda(double lambda) { muscleLambda = lambda;  gSimulation->updateLambda(SKIN);}

	void updateMu();
	void updateLambda();

protected:
	static ConstitutiveModel mConstitutiveModel;  //constitutive model type
	static double noneMu;
	static double noneLambda;
	static double boneMu;
	static double boneLambda;
	static double muscleMu;
	static double muscleLambda;
	static double skinMu;
	static double skinLambda;

	TetType mTetType;
	double mu;                                    //Lame coefficients, related to Young's modulus and Poisson's ratio
	double lambda;                                //Lame coefficients, related to Young's modulus and Poisson't ratio
	double laplacianCoeff;
	const Eigen::Matrix3d I;                      //Identity matrix
	const Matrix3333 dFdF;                        //identity tensor used for compute hessian
	Matrix3333 dPdF;                              //stress derivative
	Matrix3333 dRdF;                              //R derivative only valid in corotated and Neohookean
	Eigen::Vector4i indices;                      //4 indices of a tet
	Eigen::Vector3d Xi, Xj, Xk, Xl;               //undeformed position
	Eigen::Vector3d xi, xj, xk, xl;               //deformed position
	Eigen::Matrix3d Dm;                           //reference shape matrix
	Eigen::Matrix3d Ds;                           //deformed shape matrix
	Eigen::Matrix3d Bm;                           //inverse of Dm
	Eigen::Matrix3d F;                            //deformation gradient
	Eigen::Matrix3d Ft;                           //transpose of F
	Eigen::Matrix3d Finv;                         //inverse of F
	Eigen::Matrix3d FinvT;                        //transpose of inverse of F 
	Eigen::Matrix3d U;                            //from singular value decomposition     
	Eigen::Matrix3d V;                            //from singular value decomposition
	Eigen::Vector3d Sigma;                        //from singular value decomposition
	Eigen::Matrix3d R;                            //rotation matrix from polar decomposition
	Eigen::Matrix3d P;                            //Piola stress
	Eigen::Matrix3d H;
	Eigen::Matrix3d e;                            //small strain tensor, used in linear elasticity
	Eigen::Matrix3d E;                            //Green strain tensor, used in St. Venant-Kirchoff model
	double W;                                      //undeformed volume
	double Energy;                                 //energy
	double phi;                                    //energy density
	double logJ;                                   //used in Neohookean elasticity
	
	inline void computeP();
	inline void computedPdF();
	virtual void computeF();

	inline void computeGradientFromP(const Eigen::Matrix3d &p, VectorX &gradient);
	inline void computeHessianFromdPdF(const Matrix3333 &dpdf, std::vector<T> &hTriplets);
};

class MuscleTetConstraint :public TetConstraint {
public:
	MuscleTetConstraint(const Tet &tet, const Eigen::Vector3d &direction):TetConstraint(tet), mFiberDirection(direction), alpha(1.0){}
	MuscleTetConstraint(const Tet &tet, double x, double y, double z) :TetConstraint(tet), alpha(1.0) {
		mFiberDirection.x() = x;
		mFiberDirection.y() = y;
		mFiberDirection.z() = z;
	}

	virtual void precomputation(const VectorX &x) override;
	virtual double computeEnergy() override;
	virtual void computeGradient(VectorX &gradient) override;
	virtual void computeHessian(std::vector<T> &hTriplets) override;
	virtual void computeLaplacian(std::vector<T> &lTriplets) override;

	void setAlpha(double a) { alpha = a; }



	
private:
	double alpha;                      //scaling factor in muscle activation matrix
	double mEnergy;                                 //energy
	double mphi;                                    //energy density
	double mlogJ;                                   //used in Neohookean elasticity

	Eigen::Vector3d mFiberDirection;   //normalized fiber direction of each tet, to be read from file
	Eigen::Matrix3d mD;                //transform tet from local coordinate to fiber direction coordinate
	Eigen::Matrix3d mA;                //matrix of muscle activation
	Eigen::Matrix3d mF;                //F * A.inverse()
	Eigen::Matrix3d mFt;
	Eigen::Matrix3d mFinv;
	Eigen::Matrix3d mFinvT;   
	Eigen::Matrix3d mU;                            //from singular value decomposition     
	Eigen::Matrix3d mV;                            //from singular value decomposition
	Eigen::Vector3d mSigma;                        //from singular value decomposition
	Eigen::Matrix3d mR;                            //rotation matrix from polar decomposition
	Eigen::Matrix3d mP;                            //Piola stress
	Eigen::Matrix3d mH;
	Eigen::Matrix3d me;                            //small strain tensor, used in linear elasticity
	Eigen::Matrix3d mE;                            //Green strain tensor, used in St. Venant-Kirchoff model

	Matrix3333 mdPdF;                              //stress derivative
	Matrix3333 mdRdF;                              //R derivative only valid in corotated and Neohookean


	inline void computeD();
	inline void computeA();
	virtual void computeF();
	inline void computeP();
	inline void computedPdF();

};