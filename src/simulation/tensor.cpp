#include "tensor.h"
#include <cassert>
#include <iomanip>

Matrix3333 Matrix3333::Identity(){
	Matrix3333 res;
	res.setIdentity();
	return res;
}

void Matrix3333::setZero(){
	for(int i = 0; i < 3; i++){
		for(int j = 0; j < 3; j++){
			mat[i][j] = Eigen::Matrix3d::Zero();
		}
	}
}

void Matrix3333::setIdentity(){
	for(int i = 0; i < 3; i++) {
		for(int j = 0; j < 3; j++) {
			mat[i][j] = Eigen::Matrix3d::Zero();
			mat[i][j](i, j) = 1.0;
		}
	}
}

//operators
Eigen::Matrix3d &Matrix3333::operator() (int row, int col){
	assert(row >= 0 && row < 3 && col >= 0 && col < 3);
	return mat[row][col];
}

const Eigen::Matrix3d &Matrix3333::operator()(int row, int col)const{
	assert(row >= 0 && row < 3 && col >= 0 && col < 3);
	return mat[row][col];
}

Matrix3333 Matrix3333::operator+(const Matrix3333 &rhs){
	Matrix3333 res;
	for(int i = 0; i < 3; i++) {
		for(int j = 0; j < 3; j++) {
			res.mat[i][j] = mat[i][j] + rhs(i, j);
		}
	}
	return res;
}

Matrix3333 Matrix3333::operator-(const Matrix3333 &rhs){
	Matrix3333 res;
	for(int i = 0; i < 3; i++) {
		for(int j = 0; j < 3; j++) {
			res(i, j) = mat[i][j] -rhs(i, j);
		}
	}
	return res;
}

Matrix3333 Matrix3333::operator*(const Eigen::Matrix3d &rhs){
	Matrix3333 res;
	for(int i = 0; i < 3; i++) {
		for(int j = 0; j < 3; j++) {
			res.mat[i][j].setZero();
			for(int k = 0; k < 3; k++) {
				res.mat[i][j] += mat[i][k] * rhs(k, j);
			}
		}
	}
	return res;
}

Matrix3333 Matrix3333::operator*(double rhs){
	Matrix3333 res;
	for(int i = 0; i < 3; i++){
		for(int j = 0; j < 3; j++) {
			res(i, j) = mat[i][j] * rhs;
		}
	}
	return res;
}

Matrix3333 Matrix3333::transpose() const {
	Matrix3333 res;
	for(int i = 0; i < 3; i++) {
		for(int j = 0; j < 3; j++) {
			res(i, j) = mat[j][i];
		}
	}
	return res;
}

Eigen::Matrix3d Matrix3333::contract(const Eigen::Matrix3d &rhs){
	Eigen::Matrix3d res;
	res.setZero();
	for(int i = 0; i < 3; i++) {
		for(int j = 0; j < 3; j++) {
			res += mat[i][j] * rhs(i, j);
		}
	}
	return res;
}

Matrix3333 Matrix3333::contract(const Matrix3333 &rhs){
	Matrix3333 res;
	for(int i = 0; i < 3; i++) {
		for(int j = 0; j < 3; j++) {
			res(i, j) = this->contract(rhs(i, j));
		}
	}
	return res;
}

Matrix3333 operator+(const Matrix3333 &lhs, const Matrix3333 &rhs) {
	Matrix3333 res;
	res.setZero();
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			res(i, j) = lhs(i, j) + rhs(i, j);
		}
	}
	return res;
}

Matrix3333 operator-(const Matrix3333 &lhs, const Matrix3333 &rhs){
	Matrix3333 res;
	res.setZero();
	for(int i = 0; i < 3; i++) {
		for(int j = 0; j < 3; j++) {
			res(i, j) = lhs(i, j) - rhs(i, j);
		}
	}
	return res;
}

Matrix3333 operator*(const Eigen::Matrix3d &lhs, const Matrix3333 &rhs){
	Matrix3333 res;
	for(int i = 0; i < 3; i++) {
		for(int j = 0; j < 3; j++) {
			res(i, j).setZero();
			for(int k = 0; k < 3; k++) {
				res(i, j) +=lhs(i, k) * rhs(k, j);
			}
		}
	}
	return res;
}

Matrix3333 operator*(double lhs, const Matrix3333 &rhs){
	Matrix3333 res;
	for(int i = 0; i < 3; i++){
		for(int j = 0; j < 3; j++) {
			res(i, j) = lhs * rhs(i, j);
		}
	}
	return res;
}

void directProduct(Matrix3333& dst, const Eigen::Matrix3d &src1, const Eigen::Matrix3d &src2){
	for(int i = 0; i < 3; i++) {
		for(int j = 0; j < 3; j++) {
			dst(i, j) = src1(i, j) * src2;
		}
	}
}

std::ostream &operator<<(std::ostream &os, const Matrix3333 &matrix3333){
	os << "|" << std::fixed << std::setw(6)  
	          << matrix3333.mat[0][0](0, 0) << " " << matrix3333.mat[0][0](0, 1) << " " << matrix3333.mat[0][0](0, 2) << "|"  << "|" << matrix3333.mat[0][1](0, 0) << " " << matrix3333.mat[0][1](0, 1) << " " << matrix3333.mat[0][1](0, 2) << "|"  << "|" << matrix3333.mat[0][2](0, 0) << " " << matrix3333.mat[0][2](0, 1) << " " << matrix3333.mat[0][2](0, 2) << "|"  << std::endl
	   << "|" << matrix3333.mat[0][0](1, 0) << " " << matrix3333.mat[0][0](1, 1) << " " << matrix3333.mat[0][0](1, 2) << "|"  << "|" << matrix3333.mat[0][1](1, 0) << " " << matrix3333.mat[0][1](1, 1) << " " << matrix3333.mat[0][1](1, 2) << "|"  << "|" << matrix3333.mat[0][2](1, 0) << " " << matrix3333.mat[0][2](1, 1) << " " << matrix3333.mat[0][2](1, 2) << "|"  << std::endl
	   << "|" << matrix3333.mat[0][0](2, 0) << " " << matrix3333.mat[0][0](2, 1) << " " << matrix3333.mat[0][0](2, 2) << "|"  << "|" << matrix3333.mat[0][1](2, 0) << " " << matrix3333.mat[0][1](2, 1) << " " << matrix3333.mat[0][1](2, 2) << "|"  << "|" << matrix3333.mat[0][2](2, 0) << " " << matrix3333.mat[0][2](2, 1) << " " << matrix3333.mat[0][2](2, 2) << "|"  << std::endl
	   << std::endl
	   << "|" << matrix3333.mat[1][0](0, 0) << " " << matrix3333.mat[1][0](0, 1) << " " << matrix3333.mat[1][0](0, 2) << "|"  << "|" << matrix3333.mat[1][1](0, 0) << " " << matrix3333.mat[1][1](0, 1) << " " << matrix3333.mat[1][1](0, 2) << "|"  << "|" << matrix3333.mat[1][2](0, 0) << " " << matrix3333.mat[1][2](0, 1) << " " << matrix3333.mat[1][2](0, 2) << "|"  << std::endl
	   << "|" << matrix3333.mat[1][0](1, 0) << " " << matrix3333.mat[1][0](1, 1) << " " << matrix3333.mat[1][0](1, 2) << "|"  << "|" << matrix3333.mat[1][1](1, 0) << " " << matrix3333.mat[1][1](1, 1) << " " << matrix3333.mat[1][1](1, 2) << "|"  << "|" << matrix3333.mat[1][2](1, 0) << " " << matrix3333.mat[1][2](1, 1) << " " << matrix3333.mat[1][2](1, 2) << "|"  << std::endl
	   << "|" << matrix3333.mat[1][0](2, 0) << " " << matrix3333.mat[1][0](2, 1) << " " << matrix3333.mat[1][0](2, 2) << "|"  << "|" << matrix3333.mat[1][1](2, 0) << " " << matrix3333.mat[1][1](2, 1) << " " << matrix3333.mat[1][1](2, 2) << "|"  << "|" << matrix3333.mat[1][2](2, 0) << " " << matrix3333.mat[1][2](2, 1) << " " << matrix3333.mat[1][2](2, 2) << "|"  << std::endl
	   << std::endl
	   << "|" << matrix3333.mat[2][0](0, 0) << " " << matrix3333.mat[2][0](0, 1) << " " << matrix3333.mat[2][0](0, 2) << "|"  << "|" << matrix3333.mat[2][1](0, 0) << " " << matrix3333.mat[2][1](0, 1) << " " << matrix3333.mat[2][1](0, 2) << "|"  << "|" << matrix3333.mat[2][2](0, 0) << " " << matrix3333.mat[2][2](0, 1) << " " << matrix3333.mat[2][2](0, 2) << "|"  << std::endl
	   << "|" << matrix3333.mat[2][0](1, 0) << " " << matrix3333.mat[2][0](1, 1) << " " << matrix3333.mat[2][0](1, 2) << "|"  << "|" << matrix3333.mat[2][1](1, 0) << " " << matrix3333.mat[2][1](1, 1) << " " << matrix3333.mat[2][1](1, 2) << "|"  << "|" << matrix3333.mat[2][2](1, 0) << " " << matrix3333.mat[2][2](1, 1) << " " << matrix3333.mat[2][2](1, 2) << "|"  << std::endl
	   << "|" << matrix3333.mat[2][0](2, 0) << " " << matrix3333.mat[2][0](2, 1) << " " << matrix3333.mat[2][0](2, 2) << "|"  << "|" << matrix3333.mat[2][1](2, 0) << " " << matrix3333.mat[2][1](2, 1) << " " << matrix3333.mat[2][1](2, 2) << "|"  << "|" << matrix3333.mat[2][2](2, 0) << " " << matrix3333.mat[2][2](2, 1) << " " << matrix3333.mat[2][2](2, 2) << "|"  << std::endl
	   << std::endl;
	   return os;
}