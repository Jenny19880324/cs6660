//#define EIGEN_USE_MKL_ALL

#include <iostream>
#include <cassert>
#include <Eigen/SparseCholesky>
#include <Eigen/SparseLU>
#include <Eigen/IterativeLinearSolvers>
#include <Eigen/Dense>
#include <condition_variable>
#include <fstream>

#include "simulation.h"
#include "SimulatableMesh.h"
#include "constraint.h"

extern SimulatableMesh *gRestposeSimulatableMesh;
std::condition_variable gRenderCondVar;
bool gRenderNotified = false;

Simulation::Simulation(SimulatableMesh *SimulatableMesh):mSimulatableMesh(SimulatableMesh), mIterations(100),
mCheckGradient(false), mCheckHessian(false), mStop(false), mRenderIntermediate(false), mOptimization(NEWTON), mNumerical(BACKWARD_EULER), h(0.033), hSquare(0.033 * 0.033), g(9.8) {
	mSystemDimension = SimulatableMesh->getNumVertices() * 3;
	X.conservativeResize(mSystemDimension, Eigen::NoChange);
	x.conservativeResize(mSystemDimension, Eigen::NoChange);
	xn.conservativeResize(mSystemDimension, Eigen::NoChange);
	v.conservativeResize(mSystemDimension, Eigen::NoChange);
	y.conservativeResize(mSystemDimension, Eigen::NoChange);
	gravity.conservativeResize(mSystemDimension, Eigen::NoChange);
	M.conservativeResize(mSystemDimension, mSystemDimension);
	X.setZero(); x.setZero(); xn.setZero();  v.setZero(); y.setZero();  gravity.setZero();  M.setIdentity();
	for(int i = 0; i < mSystemDimension / 3; i++) {
		X(i * 3 + 0) = SimulatableMesh->getVertices(i).x();
		X(i * 3 + 1) = SimulatableMesh->getVertices(i).y();
		X(i * 3 + 2) = SimulatableMesh->getVertices(i).z();
		gravity(i * 3 + 1) = -g;
	}
	setupTetConstraints();
	gravity = M * gravity;
}

void Simulation::updateVertices(){
	for(int i = 0; i < mSystemDimension / 3; i++) {
		x(i * 3 + 0) = mSimulatableMesh->getVertices(i).x();
		x(i * 3 + 1) = mSimulatableMesh->getVertices(i).y();
		x(i * 3 + 2) = mSimulatableMesh->getVertices(i).z();
	}
}

void Simulation::updateMu(TetType tetType) {
	for (int i = 0; i < mTetConstraints.size(); i++) {
		if (mTetConstraints[i]->getTetType() == tetType) {
			mTetConstraints[i]->updateMu();
		}
	}
}

void Simulation::updateLambda(TetType tetType) {
	for (int i = 0; i < mTetConstraints.size(); i++) {
		if (mTetConstraints[i]->getTetType() == tetType) {
			mTetConstraints[i]->updateLambda();
		}
	}
}

void Simulation::setupTetConstraints(){
	std::vector<T> m;
	for(int i = 0; i < mSimulatableMesh->getNumTetrahedra(); i++){
		const Tet tet = mSimulatableMesh->getTetrahedra(i);
		//if (tet.getType() == BICEP) {
		//	mTetConstraints.push_back(new MuscleTetConstraint(tet, 
		//		tet.getFiberDirectionX(),
		//		tet.getFiberDirectionY(), 
		//		tet.getFiberDirectionZ()));
		//}
		//else {
			mTetConstraints.push_back(new TetConstraint(tet));
		//}
		Eigen::Vector4i indices = tet.getIndices();
		double W = tet.getVolume();
		for (int j = 0; j < 4; j++) {
			int index = indices[j];
			m.push_back(T(index * 3 + 0, index * 3 + 0, 0.25 * W));
			m.push_back(T(index * 3 + 1, index * 3 + 1, 0.25 * W));
			m.push_back(T(index * 3 + 2, index * 3 + 2, 0.25 * W));
		}
	}
	M.setFromTriplets(m.begin(), m.end());
}

void Simulation::addAttachmentConstraint(int index, const Eigen::Vector3d &X) {
	std::unordered_map<int, AttachmentConstraint *>::const_iterator got = mAttachmentConstraints.find(index);
	if (got == mAttachmentConstraints.end()) {
		mAttachmentConstraints.insert({ index, new AttachmentConstraint(index, X) });
	}
	else {
		got->second->setXi(X);
	}
}

void Simulation::precomputation(const VectorX &x){
	for(int i = 0; i < mTetConstraints.size(); i++){
		mTetConstraints[i]->precomputation(x);
	}
	for (auto it = mAttachmentConstraints.begin(); it != mAttachmentConstraints.end(); it++) {
		it->second->precomputation(x);
	}
}

double Simulation::computeEnergy(const VectorX &x) const{
	double energy = 0.0;
	for(int i = 0; i < mTetConstraints.size(); i++){
		mTetConstraints[i]->precomputation(x);
		energy += mTetConstraints[i]->computeEnergy();
	}
	for (auto it = mAttachmentConstraints.begin(); it != mAttachmentConstraints.end(); it++) {
		it->second->precomputation(x);
		energy += it->second->computeEnergy();
	}

	energy -= gravity.dot(x);
	if (mNumerical == BACKWARD_EULER) {
		energy = 0.5 * (x - y).transpose() * M * (x - y) + energy * hSquare;
	}
	return energy;
}

void Simulation::computeGradientHessian(const VectorX &x, VectorX &gradient, std::vector<T> &hTriplets){
	for(int i = 0; i < mTetConstraints.size(); i++){
		mTetConstraints[i]->precomputation(x);
		mTetConstraints[i]->computeGradient(gradient);
		mTetConstraints[i]->computeHessian(hTriplets);
	}
	for (auto it = mAttachmentConstraints.begin(); it != mAttachmentConstraints.end(); it++) {
		it->second->precomputation(x);
		it->second->computeGradient(gradient);
		it->second->computeHessian(hTriplets);
	}
}

void Simulation::computeGradientLaplacian(const VectorX &x, VectorX &gradient, std::vector<T> &lTriplets) {
	for (int i = 0; i < mTetConstraints.size(); i++) {
		mTetConstraints[i]->precomputation(x);
		mTetConstraints[i]->computeGradient(gradient);
		mTetConstraints[i]->computeLaplacian(lTriplets);
	}
	for (auto it = mAttachmentConstraints.begin(); it != mAttachmentConstraints.end(); it++) {
		it->second->precomputation(x);
		it->second->computeGradient(gradient);
		it->second->computeLaplacian(lTriplets);
	}
}

void Simulation::computeGradient(const VectorX &x, VectorX &gradient){
	for(int i = 0; i < mTetConstraints.size(); i++){
		mTetConstraints[i]->precomputation(x);
		mTetConstraints[i]->computeGradient(gradient);
	}
	for (auto it = mAttachmentConstraints.begin(); it != mAttachmentConstraints.end(); it++) {
		it->second->precomputation(x);
		it->second->computeGradient(gradient);
	}
}

void Simulation::computeHessian(const VectorX &x, std::vector<T> &hTriplets){
	for(int i = 0; i < mTetConstraints.size(); i++){
		mTetConstraints[i]->precomputation(x);
		mTetConstraints[i]->computeHessian(hTriplets);
	}
	for (auto it = mAttachmentConstraints.begin(); it != mAttachmentConstraints.end(); it++) {
		it->second->precomputation(x);
		it->second->computeHessian(hTriplets);
	}
}

void Simulation::computeLaplacian(std::vector<T> &lTriplets) {
	for (int i = 0; i < mTetConstraints.size(); i++) {
		mTetConstraints[i]->computeLaplacian(lTriplets);
	}
	for (auto it = mAttachmentConstraints.begin(); it != mAttachmentConstraints.end(); it++) {
		it->second->computeLaplacian(lTriplets);
	}
 }


double Simulation::lineSearch(const VectorX &x, const VectorX &searchDir, const VectorX &localGradientDir){
	double energy = computeEnergy(x);
	double t = 1.0;
	double alpha = 0.01, beta = 0.5;

	assert(localGradientDir.transpose() * searchDir <= 0);
	while(!(computeEnergy(x + t * searchDir) < energy + alpha * t * localGradientDir.transpose() * searchDir)){
		t = beta * t;
		if(t < 1e-10){
			std::cout << "Warning: minimizer is not found. |g| = " << localGradientDir.norm() << std::endl;
			return 0.0;
		}
	}
	return t;
}

bool Simulation::integrateQuasiStaticNewton(){
	SpMat hessian(mSystemDimension, mSystemDimension);
	VectorX dx(mSystemDimension);
	VectorX gradient(mSystemDimension);
	unsigned int iterations = 0;
	mStop = false;

	std::cout << "Newton's method" << std::endl;

	do{
		gradient.setZero();
		hessian.setZero();
		std::vector<T> hTriplets;
		computeGradientHessian(x, gradient, hTriplets);
		std::cout << "energy = " << computeEnergy(x) << std::endl;
		hessian.setFromTriplets(hTriplets.begin(), hTriplets.end());
		if (mCheckGradient) {
			std::cout << "gradient checker " << checkGradient(x, gradient) << std::endl;
		}
		if (mCheckHessian) {
			std::cout << "hessian checker " << checkHessian(x, hessian) << std::endl;
		}

		Eigen::SimplicialLLT<SpMat, Eigen::Upper> solver;

		factorizeLLT(hessian, solver);

		dx = solver.solve(-gradient);
		if(solver.info() != Eigen::Success) {
			std::cout << "solving failed" << std::endl;
			return false;
		}
		double t = lineSearch(x, dx, gradient);
		std::cout << "t = " << t << std::endl;
		std::cout << "|g| = " << gradient.norm() << std::endl;

		if(t == 0) {
			break;
		}
		x += t * dx;
		iterations++;

		if(iterations >  mIterations){
			std::cout << "reaches max iterations. dx.norm() = " <<dx.norm() << std::endl;
			break;
		}
		if (mStop) {
			std::cout << "prematurally terminate" << std::endl;
			break;
		}

		if (mRenderIntermediate) {
#if !defined(BUILD_MUSCLE_ACTIVATION_LIB)
			mSimulatableMesh->setVertices(x);
			gRenderNotified = true;
			gRenderCondVar.notify_one();
#endif
		}
	}
	while(dx.norm() >= 1e-8);
	mSimulatableMesh->setVertices(x);
	return true;
}

bool Simulation::integrateQuasiStaticProjectiveDynamics() {
	SpMat laplacian(mSystemDimension, mSystemDimension);
	VectorX dx(mSystemDimension);
	VectorX gradient(mSystemDimension);
	unsigned int iterations = 0;
	mStop = false;
	double halfLambdaSquared;

	std::cout << "Projective Dynamics" << std::endl;
	gradient.setZero();
	laplacian.setZero();

	std::vector<T> lTriplets;
	computeLaplacian(lTriplets);
	laplacian.setFromTriplets(lTriplets.begin(), lTriplets.end());

	Eigen::SimplicialLLT<SpMat, Eigen::Upper> solver;

	factorizeLLT(laplacian, solver);

	do {

		computeGradient(x, gradient);
		std::cout << "energy = " << computeEnergy(x) << std::endl;

		if (mCheckGradient) {
			std::cout << "gradient checker " << checkGradient(x, gradient) << std::endl;
		}

		dx = solver.solve(-gradient);
		if (solver.info() != Eigen::Success) {
			std::cout << "solving failed" << std::endl;
			return false;
		}
		double t = lineSearch(x, dx, gradient);
		std::cout << "t = " << t << std::endl;

		if (t == 0) {
			break;
		}

		
		x += t * dx;
		iterations++;

		std::cout << "dx.norm() / x.norm() = " << dx.norm() / x.norm() << std::endl;
		if (iterations >  mIterations) {
			std::cout << "reaches max iterations. dx.norm() = " << dx.norm() << std::endl;
			break;
		}

		if (mStop) {
			std::cout << "prematurally terminate" << std::endl;
			break;
		}

		if (mRenderIntermediate) {
#if !defined(BUILD_MUSCLE_ACTIVATION_LIB)
			mSimulatableMesh->setVertices(x);
			gRenderNotified = true;
			gRenderCondVar.notify_one();
#endif
		}

		halfLambdaSquared = -0.5 * gradient.transpose() * dx;
		std::cout << "0.5 * lambda * lambda = " << halfLambdaSquared << std::endl;
	} while ( halfLambdaSquared >= 1e-12);
	mSimulatableMesh->setVertices(x);
	return true;
}

void Simulation::factorizeLLT(const SpMat &hessian, Eigen::SimplicialLLT<SpMat, Eigen::Upper> &solver){
	SpMat I(mSystemDimension, mSystemDimension);
	I.setIdentity();
	SpMat regHessian = hessian;
	solver.analyzePattern(regHessian);
	solver.factorize(regHessian);
	double regularization = 1e-8;
	bool success = true;

	while(solver.info() != Eigen::Success){
		regularization *= 10;
		regHessian = regHessian + regularization * I; 
		solver.factorize(regHessian);
		success = false;
	}

	if(!success){
		std::cout << "Warning: adding " << regularization << " identities.(llt solver)" << std::endl;
	}
}

bool Simulation::checkGradientHessian(const VectorX &x, const VectorX &gradient, const SpMat &hessian) const {
	return checkGradient(x, gradient) && checkHessian(x, hessian);
}

bool Simulation::checkGradient(const VectorX &x, const VectorX &gradient) const {
	VectorX finiteDifference(mSystemDimension);
	for (int i = 0; i < mSystemDimension; i++) {
		VectorX dx(mSystemDimension);
		dx.setZero();
		dx(i) = 1e-6;
		finiteDifference(i) = (computeEnergy(x + dx) - computeEnergy(x)) / dx(i);
		if (std::abs((gradient(i) - finiteDifference(i))/finiteDifference(i)) > 1e-2 &&
			!(gradient(i) < 1e-2 && finiteDifference(i) < 1e-2)) {
			std::cout << "gradient(" <<i << ") = " << std::endl << gradient(i) << std::endl;
			std::cout << "finite difference(" << i << ") = " << std::endl << finiteDifference(i) << std::endl;
			std::cout << "computeEnergy(x + dx) = " << std::endl << computeEnergy(x + dx) << std::endl;
			std::cout << "computeEnergy(x) = " << std::endl << computeEnergy(x) << std::endl;
			return false;
		}
	}
	return true;
}

bool Simulation::checkHessian(const VectorX &x, const SpMat &hessian) const {
	Eigen::MatrixXd finiteDifference(mSystemDimension, mSystemDimension);
	Eigen::MatrixXd denseHessian(hessian);
	for (int i = 0; i < mSystemDimension; i++) {
		for (int j = 0; j < mSystemDimension; j++) {
			VectorX dxi(mSystemDimension);
			VectorX dxj(mSystemDimension);
			dxi.setZero();
			dxj.setZero();
			dxi(i) = 1e-2;
			dxj(j) = 1e-2;
			//central difference approximation
			//https://v8doc.sas.com/sashtml/ormp/chap5/sect28.htm
			if (i == j) {
				finiteDifference(i, j) = (
					- computeEnergy(x + 2 * dxi)
					+ 16 * computeEnergy(x + dxi)
					- 30 * computeEnergy(x)
					+ 16 * computeEnergy(x - dxi)
					- computeEnergy(x - dxi))/ (12 * dxi(i) * dxi(i));
			}
			else {
				finiteDifference(i, j) = (
					  computeEnergy(x + dxi + dxj) 
					- computeEnergy(x + dxi - dxj) 
					- computeEnergy(x - dxi + dxj) 
					+ computeEnergy(x - dxi - dxj)) / (4 * dxi(i) * dxj(j));
			}

			if (std::abs((denseHessian(i, j) - finiteDifference(i, j)) / finiteDifference(i, j)) > 0.1 &&
				!(denseHessian(i, j) < 1e-2 && finiteDifference(i, j) < 1e-2)) {
				std::cout << "denseHessian(" << i << ", " << j << ") = " << std::endl << denseHessian(i, j) << std::endl;
				std::cout << "finiteDifference(" << i << ", " << j << ") = " << std::endl << finiteDifference(i, j) << std::endl;
			}
		}
	}
	return true;
}

void Simulation::contractBicep(double alpha) {
	for (int i = 0; i < mTetConstraints.size(); i++) {
		TetConstraint *tc = mTetConstraints[i];
		if (tc->getTetType() == BICEP) {
			MuscleTetConstraint *mtc = dynamic_cast<MuscleTetConstraint *>(tc);
			mtc->setAlpha(alpha);
		}
	}
}

void Simulation::reset() {
	if (mSimulatableMesh && gRestposeSimulatableMesh) {
		mSimulatableMesh->copyVerticesFrom(gRestposeSimulatableMesh);
		mAttachmentConstraints.clear();
		mSimulatableMesh->unfixUnDragAllHandles();
		gRenderNotified = true;
		gRenderCondVar.notify_one();
	}
}

void Simulation::integrateQuasiStatic() {
	switch (mOptimization) {
		case NEWTON: {
			integrateQuasiStaticNewton();
		}break;
		case PROJECTIVE_DYNAMICS: {
			integrateQuasiStaticProjectiveDynamics();
		}break;
	}
}

bool Simulation::integrateImplicitEulerNewton() {
	SpMat hessian(mSystemDimension, mSystemDimension);
	VectorX dx(mSystemDimension);
	VectorX gradient(mSystemDimension);
	unsigned int iterations = 0;
	mStop = false;

	std::cout << "Implicit Euler Newton's method" << std::endl;
	xn = x;
	y = x + v * h;
	do {
		gradient.setZero();
		hessian.setZero();
		std::vector<T> hTriplets;
		computeGradientHessian(x, gradient, hTriplets);

		
		gradient = M * (x - y) + gradient * hSquare - gravity * hSquare;

		std::cout << "energy = " << computeEnergy(x) << std::endl;
		hessian.setFromTriplets(hTriplets.begin(), hTriplets.end());

		hessian = M + hessian * hSquare;

		if (mCheckGradient) {
			std::cout << "gradient checker " << checkGradient(x, gradient) << std::endl;
		}
		if (mCheckHessian) {
			std::cout << "hessian checker " << checkHessian(x, hessian) << std::endl;
		}

		Eigen::SimplicialLLT<SpMat, Eigen::Upper> solver;

		factorizeLLT(hessian, solver);

		dx = solver.solve(-gradient);
		if (solver.info() != Eigen::Success) {
			std::cout << "solving failed" << std::endl;
			return false;
		}
		double t = lineSearch(x, dx, gradient);
		std::cout << "t = " << t << std::endl;
		std::cout << "|g| = " << gradient.norm() << std::endl;

		if (t == 0) {
			break;
		}
		x += t * dx;
		iterations++;

		if (iterations >  mIterations) {
			std::cout << "reaches max iterations. dx.norm() = " << dx.norm() << std::endl;
			break;
		}
		if (mStop) {
			std::cout << "prematurally terminate" << std::endl;
			break;
		}

		if (mRenderIntermediate) {
#if !defined(BUILD_MUSCLE_ACTIVATION_LIB)
			mSimulatableMesh->setVertices(x);
			gRenderNotified = true;
			gRenderCondVar.notify_one();
#endif
		}
	} while (dx.norm() >= 1e-8);
	mSimulatableMesh->setVertices(x);
	v = (x - xn) / h;
	return true;
}