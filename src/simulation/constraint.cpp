#if defined(__APPLE__)
  #include <math.h>
#endif

#if defined(_WIN32)
 #define _USE_MATH_DEFINES
 #include <cmath>
#endif

#include <iostream>
#include <Eigen/Core>
#include <Eigen/Dense>
#include <Eigen/SVD>
#include <Eigen/Geometry> 
#include "constraint.h"


double AttachmentConstraint::mStiffness = 10000.0;
AttachmentConstraint::AttachmentConstraint(int index, const Eigen::Vector3d &X):mIndex(index), Xi(X){}
AttachmentConstraint::~AttachmentConstraint(){}

void AttachmentConstraint::precomputation(const VectorX &x){
	xi = x.block<3, 1>(mIndex * 3, 0);
}

double AttachmentConstraint::computeEnergy(){
	return 0.5 * mStiffness * (xi - Xi).squaredNorm();
}

void AttachmentConstraint::computeGradient(VectorX &gradient){
	gradient.block<3, 1>(mIndex * 3, 0) += mStiffness * (xi - Xi);
}

void AttachmentConstraint::computeHessian(std::vector<T> &hTriplets){
	hTriplets.push_back(T(mIndex * 3 + 0, mIndex * 3 + 0, mStiffness));
	hTriplets.push_back(T(mIndex * 3 + 1, mIndex * 3 + 1, mStiffness));
	hTriplets.push_back(T(mIndex * 3 + 2, mIndex * 3 + 2, mStiffness));
}

void AttachmentConstraint::computeLaplacian(std::vector<T> &lTriplets) {
	lTriplets.push_back(T(mIndex * 3 + 0, mIndex * 3 + 0, mStiffness));
	lTriplets.push_back(T(mIndex * 3 + 1, mIndex * 3 + 1, mStiffness));
	lTriplets.push_back(T(mIndex * 3 + 2, mIndex * 3 + 2, mStiffness));
}

ConstitutiveModel TetConstraint::mConstitutiveModel = COROTATED_LINEAR_ELASTICITY;
double TetConstraint::noneMu = 1000.0;
double TetConstraint::noneLambda = 1000.0;
double TetConstraint::boneMu = 1000.0;
double TetConstraint::boneLambda = 1000.0;
double TetConstraint::muscleMu = 1000.0;
double TetConstraint::muscleLambda = 1000.0;
double TetConstraint::skinMu = 1000.0;
double TetConstraint::skinLambda = 1000.0;


TetConstraint::TetConstraint(const Tet &tet): 
	mTetType(tet.getType()),
	I(Eigen::Matrix3d::Identity()), 
	indices(tet.getIndices()), 
	dFdF(Matrix3333::Identity()){
	switch (mTetType) {
	case NONE: {
		lambda = noneLambda;
		mu = noneMu;
	}break;
	case ULNA:
	case RADIUS:
	case HUMERUS:{
		lambda = boneLambda;
		mu = boneMu;
	}break;
	case BICEP:
	case TRICEP:{
		lambda = muscleLambda;
		mu = muscleMu;
	}break;
	case SKIN:{
		lambda = skinLambda;
		mu = skinMu;
	}break;
	}
	laplacianCoeff = 2 * mu + lambda;


	Xi = tet.getVertices(0);
	Xj = tet.getVertices(1);
	Xk = tet.getVertices(2);
	Xl = tet.getVertices(3);

	Dm.col(0) = Xi - Xl;
	Dm.col(1) = Xj - Xl;
	Dm.col(2) = Xk - Xl;
	Bm = Dm.inverse();

	W = 1.0 / 6.0 * std::abs(Dm.determinant());

}

void TetConstraint::updateMu() {
	switch (mTetType) {
		case NONE: {
			mu = noneMu; std::cout << "noneMu = " << noneMu << std::endl;
		}break;
		case ULNA:
		case RADIUS:
		case HUMERUS:{
			mu = boneLambda; std::cout << "boneMu = " << boneMu << std::endl;
		}break;
		case BICEP:
		case TRICEP:{
			mu = muscleLambda; std::cout << "muscleMu = " << muscleMu << std::endl;
		}break;
		case SKIN: {
			mu = skinMu; std::cout << "skinMu = " << skinMu << std::endl;
		}break;
	}
}
void TetConstraint::updateLambda() {
	switch (mTetType) {
		case NONE: {
			lambda = noneLambda; std::cout << "noneLambda = " << noneLambda << std::endl;
		}break;
		case ULNA:
		case RADIUS:
		case HUMERUS:{
			lambda = boneLambda; std::cout << "boneLambda = " << boneLambda << std::endl;
		}break;
		case BICEP:
		case TRICEP:{
			lambda = muscleLambda; std::cout << "muscleLambda = " << muscleLambda << std::endl;
		}break;
		case SKIN: {
			lambda = skinLambda; std::cout << "muscleLambda = " << muscleLambda << std::endl;
		}break;
	}
}

void TetConstraint::computeF(){
	Ds.col(0) = xi - xl;
	Ds.col(1) = xj - xl;
	Ds.col(2) = xk - xl;

	F = Ds * Bm;
}

void TetConstraint::computeP(){
	switch (mConstitutiveModel){
	case LINEAR_ELASTICITY: {
		P = mu * (F + F.transpose() - 2 * I) + lambda * (F - I).trace() * I;
	}break;
	case COROTATED_LINEAR_ELASTICITY:{
		P = 2 * mu * (F - R) + lambda * ((R.transpose() * F).trace() - 3) * R;
	}break;
	case STVK:{
		P = F * (2 * mu * E + lambda * E.trace() * I);
	}break;
	case NEOHOOKEAN_ELASTICITY:{
		P = mu * (F - FinvT) + lambda * logJ * FinvT;
	}break;
	default:
		break;
	}
}



void TetConstraint::computedPdF(){
	switch (mConstitutiveModel){
	case LINEAR_ELASTICITY: {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				Matrix3333 I_kron_I;
				directProduct(I_kron_I, I, I);
				dPdF = mu * (dFdF + dFdF.transpose()) + lambda * I_kron_I;
			}
		}
	}break;
	case COROTATED_LINEAR_ELASTICITY:{
		dRdF = computedRdF(U, V, Sigma);
		dPdF = 2 * mu * (dFdF - dRdF); // first mu term
		Matrix3333 R_kron_R;
		directProduct(R_kron_R, R, R);
		Matrix3333 F_kron_R;
		directProduct(F_kron_R, F, R);
		dPdF = dPdF + lambda * ((F_kron_R + ((R.transpose() * F).trace() - 3) * dFdF).contract(dRdF) + R_kron_R);
		//dPdF.setIdentity(); dPdF = (2.0 * mu + lambda) * dPdF;
	}break;
	case STVK:{
		for(int i = 0; i < 3; i++){
			for(int j = 0; j < 3; j++){
				Eigen::Matrix3d deltaF = dFdF(i, j);
				Eigen::Matrix3d deltaE = 0.5 * (deltaF.transpose() * F + Ft * deltaF);
				dPdF(i, j) = deltaF * 2 * mu * E + lambda * E.trace() * I + F * 2 * mu * deltaE + lambda * deltaE.trace() * I;
			}
		}

	}break;
	case NEOHOOKEAN_ELASTICITY:{
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < 3; j++) {
				Eigen::Matrix3d deltaF = dFdF(i, j);
				dPdF(i, j) = deltaF * mu + (mu - lambda * logJ) * FinvT * deltaF.transpose() * FinvT + lambda * (Finv * deltaF).trace() * FinvT;
			}
		}

	}break;
	default:
	break;
	}
}

void TetConstraint::precomputation(const VectorX &x){
	xi = x.block<3, 1>(indices[0] * 3, 0);
	xj = x.block<3, 1>(indices[1] * 3, 0);
	xk = x.block<3, 1>(indices[2] * 3, 0);
	xl = x.block<3, 1>(indices[3] * 3, 0);

	computeF();

	switch(mConstitutiveModel){
	case LINEAR_ELASTICITY: {
		e = 0.5 * (F + F.transpose()) - I;
	}break;
	case COROTATED_LINEAR_ELASTICITY:{
		singularValueDecomposition(F, U, V, Sigma, R);
	}break;
	case STVK:{
		E = 1.0 / 2 * (F.transpose() * F - I);
	}break;
	case NEOHOOKEAN_ELASTICITY:{
		singularValueDecomposition(F, U, V, Sigma, R);
		//When J is really close to 0, it is advised that the deformation gradient F be temporarily replaced by the nearest physically plausible value F̃ (with detF̃ > ε)
		bool physicallyPlausible = true;
		for(int i = 0; i < 3; i++) {
			if(Sigma(i) < EPSILON){
				Sigma(i) = EPSILON;
				physicallyPlausible = false;
			}
		}
		if(!physicallyPlausible){
			F = U * Sigma.asDiagonal() * V.transpose();
		}
		double J = F.determinant();
		logJ = std::log(J);
		Ft = F.transpose();
		Finv = F.inverse();
		FinvT = Finv.transpose();
	}break;
	}
}

double TetConstraint::computeEnergy(){
	switch (mConstitutiveModel){
	case LINEAR_ELASTICITY: {
		phi = mu * e.squaredNorm() + 0.5 * lambda * std::pow(e.trace(), 2);
	}break;
	case COROTATED_LINEAR_ELASTICITY:{
		phi = mu * (F - R).squaredNorm() + 0.5 * lambda * std::pow((R.transpose() * F).trace() - 3, 2);
	}break;
	case STVK:{
		phi = mu * E.squaredNorm() + 0.5 * lambda * std::pow(E.trace(), 2);
	}break;
	case NEOHOOKEAN_ELASTICITY:{
		double I1 = (F.transpose() * F).trace();
		phi = 0.5 * mu * (I1 - 3) - mu * logJ + 0.5 * lambda * logJ * logJ;
	}break;
	default:
		break;
	}
	return W * phi;
}

void TetConstraint::computeGradientFromP(const Eigen::Matrix3d &p, VectorX &gradient) {
	H = W * P * Bm.transpose();
	Eigen::Vector3d h1 = H.block<3, 1>(0, 0);
	Eigen::Vector3d h2 = H.block<3, 1>(0, 1);
	Eigen::Vector3d h3 = H.block<3, 1>(0, 2);
	Eigen::Vector3d h4 = -h1 - h2 - h3;

	gradient.block<3, 1>(indices[0] * 3, 0) += h1;
	gradient.block<3, 1>(indices[1] * 3, 0) += h2;
	gradient.block<3, 1>(indices[2] * 3, 0) += h3;
	gradient.block<3, 1>(indices[3] * 3, 0) += h4;
}

void TetConstraint::computeHessianFromdPdF(const Matrix3333 &dpdf, std::vector<T> &hTriplets) {
	Matrix3333 dH = W * dpdf * Bm.transpose();
	Eigen::Matrix3d hBlock[4][4];

	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			hBlock[i][j].col(0) = (dH(0, j) * Bm.transpose()).col(i);
			hBlock[i][j].col(1) = (dH(1, j) * Bm.transpose()).col(i);
			hBlock[i][j].col(2) = (dH(2, j) * Bm.transpose()).col(i);
		}
		hBlock[i][3] = -hBlock[i][0] - hBlock[i][1] - hBlock[i][2];
	}
	for (int j = 0; j < 4; j++) {
		hBlock[3][j] = -hBlock[0][j] - hBlock[1][j] - hBlock[2][j];
	}

	//per tet regularization
	Eigen::Matrix<double, 9, 9> hMatrix;
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			hMatrix.block<3, 3>(i * 3, j * 3) = hBlock[i][j];
		}
	}

	Eigen::SelfAdjointEigenSolver<Eigen::Matrix<double, 9, 9>> es;
	es.compute(hMatrix);
	if (es.info() != Eigen::Success) {
		std::cout << "eigen decomposition failed" << std::endl;
		return;
	}
	Eigen::Matrix<double, 9, 9> eigenValues;
	eigenValues.setZero();
	for (int i = 0; i < es.eigenvalues().rows(); i++) {
		if (es.eigenvalues()(i) < 1e-8) {
			eigenValues(i, i) = 1e-8;
		}
		else {
			eigenValues(i, i) = es.eigenvalues()(i);
		}
	}
	Eigen::Matrix<double, 9, 9> eigenVectors = es.eigenvectors();
	hMatrix = eigenVectors * eigenValues * eigenVectors.inverse();

	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			hBlock[i][j] = hMatrix.block<3, 3>(i * 3, j * 3);
		}
		hBlock[i][3] = -hBlock[i][0] - hBlock[i][1] - hBlock[i][2];
	}
	for (int j = 0; j < 4; j++) {
		hBlock[3][j] = -hBlock[0][j] - hBlock[1][j] - hBlock[2][j];
	}

	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			const Eigen::Matrix3d block = hBlock[i][j];
			for (int row = 0; row < 3; row++) {
				for (int col = 0; col < 3; col++) {
					hTriplets.push_back(T(indices[i] * 3 + row, indices[j] * 3 + col, block(row, col)));
				}
			}
		}
	}
}

void TetConstraint::computeGradient(VectorX &gradient){
	computeP();
	computeGradientFromP(P, gradient);
}

void TetConstraint::computeHessian(std::vector<T> &hTriplets) {
	computedPdF();
	computeHessianFromdPdF(dPdF, hTriplets);
}

void TetConstraint::computeLaplacian(std::vector<T> &lTriplets) {
	double ks = laplacianCoeff * W;

	Matrix3333 dH;
	dH = ks * dFdF * Bm.transpose(); //dFdF is the identity tensor

	Eigen::Matrix3d hBlock[4][4];

	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			hBlock[i][j].col(0) = (dH(0, j) * Bm.transpose()).col(i);
			hBlock[i][j].col(1) = (dH(1, j) * Bm.transpose()).col(i);
			hBlock[i][j].col(2) = (dH(2, j) * Bm.transpose()).col(i);
		}
		hBlock[i][3] = -hBlock[i][0] - hBlock[i][1] - hBlock[i][2];
	}
	for (int j = 0; j < 4; j++) {
		hBlock[3][j] = -hBlock[0][j] - hBlock[1][j] - hBlock[2][j];
	}

	// set to triplets
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			const Eigen::Matrix3d block = hBlock[i][j];
			for (int row = 0; row < 3; row++) {
				for (int col = 0; col < 3; col++) {
					lTriplets.push_back(T(indices[i] * 3 + row, indices[j] * 3 + col, block(row, col)));
				}
			}
		}
	}
}

void MuscleTetConstraint::computeD() {
	mD = Eigen::AngleAxisd(
		acos(mFiberDirection.dot(Eigen::Vector3d::UnitX())), 
		mFiberDirection.cross(Eigen:: Vector3d::UnitX()).normalized()).matrix();

	//Eigen::Vector3d x = mD * mFiberDirection;
	//std::cout << "D = " << std::endl << mD << std::endl;
	//std::cout << "mFiberDirection = " << mFiberDirection.transpose() << std::endl;
	//std::cout << "x = " << x.transpose() << std::endl;
}

void MuscleTetConstraint::computeA() {
	computeD();
	Eigen::Matrix3d S = Eigen::Matrix3d::Identity();
	S(0, 0) = alpha;
	mA = mD.transpose() * S * mD;
	//std::cout << "A = " << mA << std::endl;
}

void MuscleTetConstraint::computeF() {
	TetConstraint::computeF();
	//computeA();
	mF = F;
	//mF= F * mA.inverse();
	//debug
}

void MuscleTetConstraint::computeP() {
	switch (mConstitutiveModel) {
	case LINEAR_ELASTICITY: {
		P = mu * (mF + mF.transpose() - 2 * I) + lambda * (F - I).trace() * I;
	}break;
	case COROTATED_LINEAR_ELASTICITY: {
		P = 2 * mu * (mF - mR) + lambda * ((R.transpose() * F).trace() - 3) * R;
	}break;
	case STVK: {
		P = 2 * mu * mF * mE + lambda * E.trace() * F;
	}break;
	case NEOHOOKEAN_ELASTICITY: {
		P = mu * (mF - mFinvT) + lambda * logJ * FinvT;
	}break;
	default:
		break;
	}
}

void MuscleTetConstraint::computedPdF(){
	switch (mConstitutiveModel) {
	case LINEAR_ELASTICITY: {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				Matrix3333 I_kron_I;
				directProduct(I_kron_I, I, I);
				dPdF = mu * (dFdF + dFdF.transpose()) + lambda * I_kron_I;
			}
		}
	}break;
	case COROTATED_LINEAR_ELASTICITY: {
		dRdF = computedRdF(U, V, Sigma);
		mdRdF = computedRdF(mU, mV, mSigma);
		dPdF = 2 * mu * (dFdF - mdRdF); // first mu term
		Matrix3333 R_kron_R;
		directProduct(R_kron_R, R, R);
		Matrix3333 F_kron_R;
		directProduct(F_kron_R, F, R);
		dPdF = dPdF + lambda * ((F_kron_R + ((R.transpose() * F).trace() - 3) * dFdF).contract(dRdF) + R_kron_R);
	}break;
	case STVK: {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				Eigen::Matrix3d deltaF = dFdF(i, j);
				Eigen::Matrix3d deltaE = 0.5 * (deltaF.transpose() * F + Ft * deltaF);
				Eigen::Matrix3d mdeltaE = 0.5 * (deltaF.transpose() * mF + mFt * deltaF);
				dPdF(i, j) = deltaF * 2 * mu * mE + lambda * E.trace() * I + mF * 2 * mu * mdeltaE + lambda * deltaE.trace() * I;
			}
		}

	}break;
	case NEOHOOKEAN_ELASTICITY: {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				Eigen::Matrix3d deltaF = dFdF(i, j);
				dPdF(i, j) = deltaF * mu + mu * mFinvT * deltaF.transpose() * mFinvT 
					         - lambda * logJ * FinvT * deltaF.transpose() * FinvT + lambda * (Finv * deltaF).trace() * FinvT;
			}
		}

	}break;
	default:
		break;
	}

}

void MuscleTetConstraint::precomputation(const VectorX &x) {
	TetConstraint::precomputation(x);
	computeF();

	switch (mConstitutiveModel) {
	case LINEAR_ELASTICITY: {
		me = 0.5 * (mF + mF.transpose()) - I;
	}break;
	case COROTATED_LINEAR_ELASTICITY: {
		singularValueDecomposition(mF, mU, mV, mSigma, mR);
	}break;
	case STVK: {
		mE = 1.0 / 2 * (mF.transpose() * mF - I);
	}break;
	case NEOHOOKEAN_ELASTICITY: {
		singularValueDecomposition(mF, mU, mV, mSigma, mR);
		//When J is really close to 0, it is advised that the deformation gradient F be temporarily replaced by the nearest physically plausible value F̃ (with detF̃ > ε)
		bool physicallyPlausible = true;
		for (int i = 0; i < 3; i++) {
			if (mSigma(i) < EPSILON) {
				mSigma(i) = EPSILON;
				physicallyPlausible = false;
			}
		}
		if (!physicallyPlausible) {
			mF = mU * mSigma.asDiagonal() * mV.transpose();
		}
		double mJ = mF.determinant();
		mlogJ = std::log(mJ);
		mFt = mF.transpose();
		mFinv = mF.inverse();
		mFinvT = mFinv.transpose();
	}break;
	}
}

double MuscleTetConstraint::computeEnergy() {
	switch (mConstitutiveModel) {
	case LINEAR_ELASTICITY: {
		phi = mu * me.squaredNorm() + 0.5 * lambda * std::pow(e.trace(), 2);
	}break;
	case COROTATED_LINEAR_ELASTICITY: {
		phi = mu * (mF - mR).squaredNorm() + 0.5 * lambda * std::pow((R.transpose() * F).trace() - 3, 2);
	}break;
	case STVK: {
		phi = mu * mE.squaredNorm() + 0.5 * lambda * std::pow(E.trace(), 2);
	}break;
	case NEOHOOKEAN_ELASTICITY: {
		double I1 = (mF.transpose() * mF).trace();
		phi = 0.5 * mu * (I1 - 3) - mu * mlogJ + 0.5 * lambda * logJ * logJ;
	}break;
	default:
		break;
	}
	return W * phi;
}


void MuscleTetConstraint::computeGradient(VectorX &gradient) {
	TetConstraint::computeGradient(gradient);
}

void MuscleTetConstraint::computeHessian(std::vector<T> &hTriplets) {
	computedPdF();
	TetConstraint::computeHessianFromdPdF(dPdF, hTriplets);
}

void MuscleTetConstraint::computeLaplacian(std::vector<T> &lTriplets) {
	//TODO 
	TetConstraint::computeLaplacian(lTriplets);
}

void singularValueDecomposition(const Eigen::Matrix3d &f, Eigen::Matrix3d &u, Eigen::Matrix3d &v, Eigen::Vector3d &sigma, Eigen::Matrix3d &r) {
	Eigen::JacobiSVD<Eigen::Matrix3d> svd;
	svd.compute(f, Eigen::ComputeFullU | Eigen::ComputeFullV);

	u = svd.matrixU();
	v = svd.matrixV();
	sigma = svd.singularValues();

	//for R to be rotation, no reflection, det(R) = det(V * U.transpose()) = +1
	//https://igl.ethz.ch/projects/ARAP/svd_rot.pdf Orientation rectification

	if (u.determinant() < 0.0) {
		u.block<3, 1>(0, 2) *= -1.0;
		sigma[2] *= -1.0;
	}
	if (v.determinant() < 0.0) {
		v.block<3, 1>(0, 2) *= -1.0;
		sigma[2] *= -1.0;
	}
	r = u * v.transpose();
}

Matrix3333 computedRdF(const Eigen::Matrix3d &u, const Eigen::Matrix3d &v, const Eigen::Vector3d &sigma) {
	const Matrix3333 dFdF = Matrix3333::Identity();
	Matrix3333 dRdF;
	//to compute dFdF using derivative of SVD
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			Eigen::Matrix3d deltaF = dFdF(i, j);
			Eigen::Matrix3d M = u.transpose() * deltaF * v;
			//special case for uniform scaling including restpose, restpose is a common pose
			if (std::abs(sigma(0) - sigma(1)) < EPSILON && std::abs(sigma(0) - sigma(2)) < EPSILON) {
				double s = sigma(0); //Sigma = s * Identity
				if (s < EPSILON) {
					s = EPSILON;
				}
				Eigen::Matrix3d offDiagM;
				offDiagM.setZero();
				for (int row = 0; row < 3; row++) {
					for (int col = 0; col < 3; col++) {
						if (row == col) {
							continue;
						}
						else {
							offDiagM(row, col) = M(row, col) / s;
						}
					}
				}
				dRdF(i, j) = u * offDiagM * v.transpose();
			}
			else {
				Eigen::Vector2d unknown, known;
				Eigen::Matrix2d knownMatrix;
				Eigen::Matrix3d tildeU, tildeV;
				tildeU.setZero(); tildeV.setZero();
				Eigen::Matrix2d reg;
				reg.setZero();
				reg(0, 0) = reg(1, 1) = 1e-8;
				for (int row = 0; row < 3; row++) {
					for (int col = 0; col < row; col++) {
						known = Eigen::Vector2d(M(col, row), M(row, col));
						knownMatrix.block<2, 1>(0, 0) = Eigen::Vector2d(-sigma[row], sigma[col]);
						knownMatrix.block<2, 1>(0, 1) = Eigen::Vector2d(-sigma[col], sigma[row]);
						if (std::abs(sigma[row] - sigma[col]) < 1e-8) {
							knownMatrix += reg;
						}
						else {
							assert(std::abs(knownMatrix.determinant()) > 1e-8);
						}
						unknown = knownMatrix.inverse() * known;
						Eigen::Vector2d test = knownMatrix * unknown;
						tildeU(row, col) = unknown[0];
						tildeU(col, row) = -tildeU(row, col);
						tildeV(row, col) = unknown[1];
						tildeV(col, row) = -tildeV(row, col);
					}
				}
				Eigen::Matrix3d deltaU = u * tildeU;
				Eigen::Matrix3d deltaV = tildeV * v.transpose();
				dRdF(i, j) = deltaU * v.transpose() + u * deltaV;
			}
		}
	}
	return dRdF;
}




