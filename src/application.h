#pragma once
#include <nanogui/screen.h>
#include <nanogui/combobox.h>
#include "canvas.h"

class Application : public nanogui::Screen {
public:
    Application();
    virtual bool keyboardEvent(int key, int scancode, int action, int modifiers);
    virtual void draw(NVGcontext *ctx);

private:
    MyGLCanvas *mCanvas;
};
