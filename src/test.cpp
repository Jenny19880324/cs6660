#include <gtest/gtest.h>
#include <Eigen/Dense>
#include "simulation.h"
#include "tetrahedralMesh.h"
#include "tensor.h"
#include "constraint.h"

int main(int argc, char **argv){
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}


class SimulationTest : public ::testing::Test{
protected:
	Simulation *testSimulation;
	SimulationTest(){
		TetrahedralMesh *testTetrahedralMesh = new TetrahedralMesh();
        testTetrahedralMesh->loadMeshFromFile("../model/tet.1.mesh");
		testSimulation = new Simulation(testTetrahedralMesh);
	}
	virtual ~SimulationTest(){
		delete testSimulation;
	}
};

TEST_F(SimulationTest, ifCorrectGradient){
	VectorX dx(testSimulation->getSystemDimension());
	dx.setZero();
	VectorX x = testSimulation->getX() + dx;

	VectorX gradient(testSimulation->getSystemDimension());
	gradient.setZero();
	testSimulation->computeGradient(x, gradient);

	VectorX finiteDifference(testSimulation->getSystemDimension());

	bool almostEqual = true;
	for(int i = 0; i < testSimulation->getSystemDimension(); i++){
		VectorX dxi(testSimulation->getSystemDimension());
		dxi.setZero();
		dxi(i) = 1e-5;
		finiteDifference(i) = (testSimulation->computeEnergy(x + dxi) - testSimulation->computeEnergy(x)) / dxi(i);
		if(std::abs(gradient(i) - finiteDifference(i)) > 1e-2) almostEqual = false;
	}

	EXPECT_TRUE(almostEqual) << std::endl << "gradient = " << std::endl << gradient << std::endl << 
	"finiteDifference = " <<std::endl << finiteDifference;		
}

TEST_F(SimulationTest, ifCorrectHessian){
	VectorX dx(testSimulation->getSystemDimension());
	dx.setZero();
	dx(9) = 0.5;

	VectorX x = testSimulation->getX() + dx;
	std::cout << "x = " << x << std::endl;

	VectorX gradient(testSimulation->getSystemDimension());
	SpMat hessian(testSimulation->getSystemDimension(), testSimulation->getSystemDimension());
	gradient.setZero();
	hessian.setZero();
	std::vector<T> hTriplets;
	testSimulation->computeGradientHessian(x, gradient, hTriplets);
	hessian.setFromTriplets(hTriplets.begin(), hTriplets.end());
	std::cout << "gradient = " << gradient << std::endl;

	Eigen::MatrixXd denseHessian(hessian);
	Eigen::MatrixXd finiteDifference(testSimulation->getSystemDimension(), testSimulation->getSystemDimension());

	bool almostEqual = true;

	for(int i = 0; i < testSimulation->getSystemDimension(); i++){
		for(int j = 0; j < testSimulation->getSystemDimension(); j++){
			VectorX dxi(testSimulation->getSystemDimension());
			VectorX dxj(testSimulation->getSystemDimension());
			dxi.setZero();
			dxj.setZero();
			dxi(i) = 1e-3;
			dxj(j) = 1e-3;
			VectorX g0(testSimulation->getSystemDimension()); g0.setZero();
			VectorX g1(testSimulation->getSystemDimension()); g1.setZero();
			VectorX g2(testSimulation->getSystemDimension()); g2.setZero();
			VectorX g3(testSimulation->getSystemDimension()); g3.setZero();
			testSimulation->computeGradient(x + dxj, g0);
			testSimulation->computeGradient(x - dxj, g1);
			testSimulation->computeGradient(x + dxi, g2);
			testSimulation->computeGradient(x - dxi, g3);
			//std::cout << "g0 = " << g0 << std::endl;
			//std::cout << "g1 = " << g1 << std::endl;
			//std::cout << "g2 = " << g2 << std::endl;
			//std::cout << "g3 = " << g3 << std::endl;

			finiteDifference(i, j) = (g0(i) - g1(i)) / (4 * dxj(j)) + (g2(j) - g3(j)) / (4 * dxi(i));
			if (std::abs((denseHessian(i, j) - finiteDifference(i, j)) / finiteDifference(i, j)) > 0.1 &&
				!(denseHessian(i, j) < 1e-2 && finiteDifference(i, j) < 1e-2)) {
				almostEqual = false;
			}
		}
	}

	Eigen::SimplicialLLT<SpMat, Eigen::Upper> solver;
	solver.compute(hessian);
	testSimulation->factorizeLLT(hessian, solver);
	dx = solver.solve(-gradient);

	Eigen::IOFormat CleanFmt(4, 0, ", ", "\n", "[", "]");
	EXPECT_TRUE(false) << std::endl << "denseHessian = " << std::endl << denseHessian.format(CleanFmt) << std::endl <<
		"finiteDifference = " << std::endl << finiteDifference.format(CleanFmt) << std::endl <<
		"dx = " << dx << std::endl;
}

TEST_F(SimulationTest, ifCorrectLaplacian) {
	VectorX dx(testSimulation->getSystemDimension());
	dx.setZero();
	dx(8) = 0.5;

	VectorX x = testSimulation->getX() + dx;

	VectorX gradient(testSimulation->getSystemDimension());
	SpMat laplacian(testSimulation->getSystemDimension(), testSimulation->getSystemDimension());
	gradient.setZero();
	laplacian.setZero();
	std::vector<T> lTriplets;
	std::cout << "x = " << x << std::endl;
	testSimulation->computeGradientLaplacian(x, gradient, lTriplets);
	laplacian.setFromTriplets(lTriplets.begin(), lTriplets.end());

	Eigen::MatrixXd denseLaplacian(laplacian);

	Eigen::SimplicialLLT<SpMat, Eigen::Upper> solver;
	solver.compute(laplacian);
	testSimulation->factorizeLLT(laplacian, solver);
	dx = solver.solve(-gradient);


	bool almostEqual = true;
	Eigen::IOFormat CleanFmt(4, 0, ", ", "\n", "[", "]");
	EXPECT_TRUE(false) << std::endl << "denseLaplacian = " << std::endl << denseLaplacian.format(CleanFmt) << std::endl << 
		"dx = " << dx << std::endl;
}


TEST_F(SimulationTest, ifCorrectSingularValueDecomposition){
	VectorX x = testSimulation->getX() + VectorX::Random(testSimulation->getSystemDimension());
	testSimulation->precomputation(x);
	for(int i = 0; i < testSimulation->getNumTetConstraints(); i++){
		const TetConstraint *constraint = testSimulation->getTetConstraint(i);
		const TetConstraint *tc = dynamic_cast<const TetConstraint *>(constraint);
		Eigen::Matrix3d F = tc->getF();
		Eigen::Matrix3d R = tc->getR();
		Eigen::Matrix3d U = tc->getU();
		Eigen::Matrix3d V = tc->getV();
		Eigen::Vector3d Sigma = tc->getSigma();

		Eigen::Vector3d r1 = R.col(0);
		Eigen::Vector3d r2 = R.col(1);
		Eigen::Vector3d r3 = R.col(2);

		EXPECT_NEAR(r1.transpose() * r2, 0, 1e-6) << "r1.transpose() * r2 = " << r1.transpose() * r2 << std::endl;
		EXPECT_NEAR(r2.transpose() * r3, 0, 1e-6) << "r2.transpose() * r3 = " << r2.transpose() * r3 << std::endl;
		EXPECT_NEAR(r3.transpose() * r1, 0, 1e-6) << "r3.transpose() * r1 = " << r3.transpose() * r1 << std::endl;
		EXPECT_NEAR(r1.norm(), 1.0, 1e-6) << "r1.norm() = " << r1.norm() << std::endl;
		EXPECT_NEAR(r2.norm(), 1.0, 1e-6) << "r2.norm() = " << r2.norm() << std::endl;
		EXPECT_NEAR(r3.norm(), 1.0, 1e-6) << "r3.norm() = " << r3.norm() << std::endl;

		Eigen::Matrix3d S;
		S.setZero();
		S(0, 0) = Sigma(0);
		S(1, 1) = Sigma(1);
		S(2, 2) = Sigma(2);

		Eigen::Matrix3d recomposedF = U * S * V.transpose();
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < 3; j++) {
				EXPECT_NEAR(F(i, j), recomposedF(i, j), 1e-6);
			}
		}
	}
}

TEST_F(SimulationTest, ifCorrectdRdF){
	VectorX dx(testSimulation->getSystemDimension());
	dx.setZero();
	dx(0) = 1;
	VectorX x = testSimulation->getX() + dx;

	std::vector<T> hTriplets;
	testSimulation->computeHessian(x, hTriplets);

	for(int i = 0; i < testSimulation->getNumTetConstraints(); i++){
		const TetConstraint *tc = testSimulation->getTetConstraint(i);
		Matrix3333 dRdF = tc->getdRdF();
		Matrix3333 dPdF = tc->getdPdF();
		EXPECT_TRUE(dRdF(0, 0).isApprox(Eigen::Matrix3d::Zero()));

		Eigen::Matrix3d ans01;
		ans01 << 0, 1.0/3, 0, -1.0/3, 0, 0, 0, 0, 0;
		Matrix3333 dFdF = Matrix3333::Identity();

		EXPECT_TRUE(dRdF(0, 1).isApprox(ans01));
	}
}

TEST_F(SimulationTest, integrateQuasiStatic){
	VectorX dx(testSimulation->getSystemDimension());
	dx.setZero();
	dx(9) = 0.5;
	VectorX x = testSimulation->getX() + dx;
	testSimulation->setx(x);
	testSimulation->integrateQuasiStaticProjectiveDynamics();

	VectorX x1 = testSimulation->getx();
	std::cout << "x = "  << std::endl << x << std::endl;
	std::cout << "x1 = " << std::endl << x1 << std::endl;
}

TEST_F(SimulationTest, attachmentConstraint){

	testSimulation->getSimulatableMesh()->getHandles(1)->fix();
	VectorX dx(testSimulation->getSystemDimension());
	dx.setZero();
	VectorX x = testSimulation->getX() + dx;
	testSimulation->integrateQuasiStaticNewton();

	VectorX x1 = testSimulation->getx();
	std::cout << "x = "  << std::endl << x << std::endl;
	std::cout << "x1 = " << std::endl << x1 << std::endl;
}

TEST(EigenTest, noAns){
	Eigen::Vector2f b1;
	Eigen::Vector2f b2;
	b1 << 1, 0;
	b2 << 0, 1;
	Eigen::Matrix2f A;
	A.col(0) = Eigen::Vector2f(1, -1);
	A.col(1) = Eigen::Vector2f(1, -1);

	Eigen::Vector2f x1 = A.partialPivLu().solve(b1);
	Eigen::Vector2f x2 = A.partialPivLu().solve(b2);


	std::cout << "x1 = " << std::endl << x1 << std::endl;
	std::cout << "x2 = " << std::endl << x2 << std::endl;

}

