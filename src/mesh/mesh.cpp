#include <iostream>
#include <fstream>
#include "mesh.h"
#include "Camera.h"
#include "canvas.h"
#include "GLSLProgramObject.h"

extern SimulatableMesh *gSimulatableMesh;

SurfaceMesh::SurfaceMesh(const SurfaceMesh *other) {
	if (!other) {
		std::cout << "base class SurfaceMesh pointer is null" << std::endl;
		return;
	}
	containsNormals = other->containsNormals;

	mNumVertices = other->mNumVertices;
	mNumNormals = other->mNumNormals;
	mNumFaces = other->mNumFaces;

	mPositions = other->mPositions;
	mNormals = other->mNormals;
	mIndices = other->mIndices;

	mTriangles = other->mTriangles;

#if !defined(BUILD_MUSCLE_ACTIVATION_LIB)
	mBufferObjects = other->mBufferObjects;
	mMaterial = other->mMaterial->clone();
	glGenVertexArrays(1, &mVertexArrayObject);
#endif
}

SurfaceMesh::SurfaceMesh(const SurfaceMesh &other) {
	containsNormals = other.containsNormals;

	mNumVertices = other.mNumVertices;
	mNumNormals = other.mNumNormals;
	mNumFaces = other.mNumFaces;

	mPositions = other.mPositions;
	mNormals = other.mNormals;
	mIndices = other.mIndices;

	mTriangles = other.mTriangles;

#if !defined(BUILD_MUSCLE_ACTIVATION_LIB)
	mBufferObjects = other.mBufferObjects;
	mMaterial = other.mMaterial->clone();
	glGenVertexArrays(1, &mVertexArrayObject);
#endif
}

SurfaceMesh::~SurfaceMesh() {
	free();
}

void SurfaceMesh::free() {
	if (mVertexArrayObject) {
		glDeleteVertexArrays(1, &mVertexArrayObject);
		mVertexArrayObject = 0;
	}
}

void ObjSurfaceMesh::createAttrib() {
	mPositions.conservativeResize(Eigen::NoChange, mNumFaces * 3);
	mTexCoords.conservativeResize(Eigen::NoChange, mNumFaces * 3);
	mNormals.conservativeResize(Eigen::NoChange, mNumFaces * 3);
	for (int i = 0; i < mNumFaces; i++) {
		for (int j = 0; j < 3; j++) {
			mPositions.col(i * 3 + j) = mTriangles[i].vertices[j];
			if (containsTexCoords) {
				mTexCoords.col(i * 3 + j) = mTriangles[i].texCoords[j];
			}
			if (containsNormals) {
				mNormals.col(i * 3 + j) = mTriangles[i].normals[j];
			}
		}
	}
	uploadAttrib("position", mPositions);
	if (containsNormals) uploadAttrib("inputNormal", mNormals);
	if (containsTexCoords) uploadAttrib("inputTexCoord", mTexCoords);
}

void MeshSurfaceMesh::createAttrib(const std::vector<Eigen::Vector3d> &inputPositions,
	const std::vector<Eigen::Vector3i> &inputIndices) {
	mNumVertices = inputPositions.size();
	mNumFaces = inputIndices.size();
	std::vector<Eigen::Vector3d> inputNormals(mNumVertices);
	for (int i = 0; i < inputNormals.size(); i++) {
		inputNormals[i] = Eigen::Vector3d(0, 0, 0);
	}
	for (int i = 0; i < inputIndices.size(); i++) {
		Eigen::Vector3d v1 = inputPositions[inputIndices[i][0]];
		Eigen::Vector3d v2 = inputPositions[inputIndices[i][1]];
		Eigen::Vector3d v3 = inputPositions[inputIndices[i][2]];
		Eigen::Vector3d normal = (v1 - v3).cross(v1 - v2);

		inputNormals[inputIndices[i][0]] += normal;
		inputNormals[inputIndices[i][1]] += normal;
		inputNormals[inputIndices[i][2]] += normal;
	}
	for (int i = 0; i < inputNormals.size(); i++) {
		inputNormals[i].normalize();
	}

	assert(inputPositions.size() == inputNormals.size());
	mPositions.conservativeResize(Eigen::NoChange, inputPositions.size());
	mNormals.conservativeResize(Eigen::NoChange, inputNormals.size());
	mIndices.conservativeResize(Eigen::NoChange, inputIndices.size());

	for (int i = 0; i < inputPositions.size(); i++) {
		mPositions.col(i) = inputPositions[i];
		mNormals.col(i) = inputNormals[i];
	}

	for (int i = 0; i < inputIndices.size(); i++) {
		mIndices.col(i) = inputIndices[i];
	}

	//initialize mTriangles for tetrahedral mesh
	for (int i = 0; i < inputIndices.size(); i++) {
		Triangle triangle;
		triangle.vertexIndices[0] = inputIndices[i][0];
		triangle.vertexIndices[1] = inputIndices[i][1];
		triangle.vertexIndices[2] = inputIndices[i][2];

		triangle.normalIndices[0] = inputIndices[i][0];
		triangle.normalIndices[1] = inputIndices[i][1];
		triangle.normalIndices[2] = inputIndices[i][2];

		triangle.vertices[0] = inputPositions[inputIndices[i][0]];
		triangle.vertices[1] = inputPositions[inputIndices[i][1]];
		triangle.vertices[2] = inputPositions[inputIndices[i][2]];

		triangle.normals[0] = inputNormals[inputIndices[i][0]];
		triangle.normals[1] = inputNormals[inputIndices[i][1]];
		triangle.normals[2] = inputNormals[inputIndices[i][2]];

		mTriangles.push_back(triangle);
	}
	uploadAttrib("position", mPositions);
	uploadAttrib("inputNormal", mNormals);
	uploadAttrib("indices", mIndices);
}

void SurfaceMesh::updateTransformationUniform(const MyGLCanvas *canvas) {
	if (!mMaterial) {
		return;
	}
	const Camera &camera = canvas->getCamera();
	const nanogui::Arcball &arcball = canvas->getArcball();
	Eigen::Matrix4f object2WorldMatrix = arcball.matrix();
	Eigen::Matrix4f modelViewMatrix = camera.getModelViewMatrix();
	Eigen::Matrix4f modelViewProjectionMatrix = camera.getModelViewProjectionMatrix();
	Eigen::Matrix4f normalTransformMatrix = (modelViewMatrix * object2WorldMatrix).inverse().transpose();

	mMaterial->setObject2WorldMatrix(object2WorldMatrix);
	mMaterial->setModelViewMatrix(modelViewMatrix);
	mMaterial->setModelViewProjectionMatrix(modelViewProjectionMatrix);
	mMaterial->setNormalTransformMatrix(normalTransformMatrix);

	Eigen::Vector3f lightPosition(-300, 2000, 0);
	lightPosition = (modelViewMatrix * Eigen::Vector4f(lightPosition(0), lightPosition(1), lightPosition(2), 1.0)).head<3>();
	mMaterial->setLightPosition(lightPosition);
}

void SurfaceMesh::createBlinnPhongMaterialFromFile(const std::string &filename) {
	//TODO remove this if statement. It's here because when sSphere is initialized, gl context hasn't been initialized
	if (filename.compare("sphere.mtl") == 0) {
		mMaterial = new BlinnPhong();
		mMaterial->loadMtlFromFile(filename);
		return;
	}
	mMaterial = new BlinnPhong(filename);
}

void SurfaceMesh::createTexturedBlinnPhongMaterialFromFile(const std::string &filename) {
	mMaterial = new TexturedBlinnPhong(filename);
}

void SurfaceMesh::createTransparentMaterial() {
	mMaterial = new Transparent();
}

template <typename Matrix>
void SurfaceMesh::uploadAttrib(const std::string &name, const Matrix &M, int version) {
	uint32_t compSize = sizeof(typename Matrix::Scalar);
	GLuint glType = (GLuint)detail::type_traits<typename Matrix::Scalar>::type;
	bool integral = (bool)detail::type_traits<typename Matrix::Scalar>::integral;

	uploadAttrib(name, (uint32_t)M.size(), (int)M.rows(), compSize,
		glType, integral, M.data(), version);
}

void SurfaceMesh::uploadAttrib(const std::string &name, size_t size, int dim,
	uint32_t compSize, GLuint glType, bool integral,
	const void *data, int version) {

	int attribID = 0;
	if (name != "indices") {
		if (!mMaterial) return;
		mMaterial->bind();
		attribID = mMaterial->getProgram()->attrib(name);
		if (attribID < 0)
			return;
	}

	GLuint bufferID;
	auto it = mBufferObjects.find(name);
	if (it != mBufferObjects.end()) {
		Buffer &buffer = it->second;
		bufferID = it->second.id;
		buffer.version = version;
		buffer.size = size;
		buffer.compSize = compSize;
	}
	else {
		glGenBuffers(1, &bufferID);
		Buffer buffer;
		buffer.id = bufferID;
		buffer.glType = glType;
		buffer.dim = dim;
		buffer.compSize = compSize;
		buffer.size = size;
		buffer.version = version;
		mBufferObjects[name] = buffer;
	}

	glBindVertexArray(mVertexArrayObject);
	size_t totalSize = size * (size_t)compSize;
	if (name == "indices") {
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferID);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, totalSize, data, GL_STATIC_DRAW);
	}
	else {
		glBindBuffer(GL_ARRAY_BUFFER, bufferID);
		glBufferData(GL_ARRAY_BUFFER, totalSize, data, GL_STATIC_DRAW);
		if (size == 0) {
			glDisableVertexAttribArray(attribID);
		}
		else {
			glEnableVertexAttribArray(attribID);
			glVertexAttribPointer(attribID, dim, glType, integral, 0, 0);
		}
	}
}


void SurfaceMesh::setMaterial(const Material *material) {
	//we know there BlinnPhong material need copying only. TODO template
	const BlinnPhong *bp = dynamic_cast<const BlinnPhong *>(material);
	if (bp) {
		mMaterial = new BlinnPhong(bp);
		return;
	}
}

bool ObjSurfaceMesh::intersect(const Ray *r, Intersection &minTimeIntersection) const {
	minTimeIntersection.t = std::numeric_limits<float>::max();
	bool ifIntersect = false;

	for (int i = 0; i < mTriangles.size(); i++) {
		Triangle triangle = mTriangles[i];
		Intersection intersection;
		if (triangle.intersect(r, intersection)) {
			if (intersection.t < minTimeIntersection.t) {
				minTimeIntersection.t = intersection.t;
				ifIntersect = true;
			}
		}
	}
	return ifIntersect;
}

void ObjSurfaceMesh::loadObjFromFile(const std::string &filename) {
	std::ifstream file;
	file.open(filename);

	if (!file.is_open()) {
		std::cout << "Could not open OBJ file " << filename << std::endl;
		exit(1);
	}

	//std::cout << "Loading obj file " << filename << std::endl;

	std::string s;
	while (file >> s) {
		if (s.compare("mtllib") == 0) {
			std::string filename;
			file >> filename;
			//mMaterial->loadMtlFromFile(filename);
		}
		else if (s.compare("v") == 0) {
			double x, y, z;
			file >> x >> y >> z;
			mInputPositions.push_back(Eigen::Vector3d(x, y, z));
			++mNumVertices;
		}
		else if (s.compare("vt") == 0) {
			containsTexCoords = true;
			float u, v;
			file >> u >> v;
			mInputTexCoords.push_back(Eigen::Vector2d(u, v));
		}
		else if (s.compare("vn") == 0) {
			containsNormals = true;
			float x, y, z;
			file >> x >> y >> z;
			mInputNormals.push_back(Eigen::Vector3d(x, y, z));
		}
		else if (s.compare("f") == 0) {
			Triangle triangle;

			std::string s1, s2, s3;
			file >> s1 >> s2 >> s3;
			size_t s1FoundSlash1 = s1.find("/");
			size_t s2FoundSlash1 = s2.find("/");
			size_t s3FoundSlash1 = s3.find("/");

			unsigned int v1 = std::stoi(s1.substr(0, s1FoundSlash1)) - 1;
			unsigned int v2 = std::stoi(s2.substr(0, s2FoundSlash1)) - 1;
			unsigned int v3 = std::stoi(s3.substr(0, s3FoundSlash1)) - 1;

			triangle.vertexIndices[0] = v1;
			triangle.vertexIndices[1] = v2;
			triangle.vertexIndices[2] = v3;
			triangle.vertices[0] = mInputPositions[v1];
			triangle.vertices[1] = mInputPositions[v2];
			triangle.vertices[2] = mInputPositions[v3];


			size_t s1FoundSlash2 = s1.find("/", s1FoundSlash1 + 1, 1);
			size_t s2FoundSlash2 = s2.find("/", s2FoundSlash1 + 1, 1);
			size_t s3FoundSlash2 = s3.find("/", s3FoundSlash1 + 1, 1);

			if (containsTexCoords) {

				unsigned int vt1 = std::stoi(s1.substr(s1FoundSlash1 + 1, s1FoundSlash2 - s1FoundSlash1 - 1)) - 1;
				unsigned int vt2 = std::stoi(s2.substr(s2FoundSlash1 + 1, s2FoundSlash2 - s2FoundSlash1 - 1)) - 1;
				unsigned int vt3 = std::stoi(s3.substr(s3FoundSlash1 + 1, s3FoundSlash2 - s3FoundSlash1 - 1)) - 1;
				triangle.texCoordIndices[0] = vt1;
				triangle.texCoordIndices[1] = vt2;
				triangle.texCoordIndices[2] = vt3;
				triangle.texCoords[0] = mInputTexCoords[vt1];
				triangle.texCoords[1] = mInputTexCoords[vt2];
				triangle.texCoords[2] = mInputTexCoords[vt3];
			}
			if (containsNormals) {
				unsigned int vn1 = std::stoi(s1.substr(s1FoundSlash2 + 1)) - 1;
				unsigned int vn2 = std::stoi(s2.substr(s2FoundSlash2 + 1)) - 1;
				unsigned int vn3 = std::stoi(s3.substr(s3FoundSlash2 + 1)) - 1;
				triangle.normalIndices[0] = vn1;
				triangle.normalIndices[1] = vn2;
				triangle.normalIndices[2] = vn3;
				triangle.normals[0] = mInputNormals[vn1];
				triangle.normals[1] = mInputNormals[vn2];
				triangle.normals[2] = mInputNormals[vn3];
			}
			mTriangles.push_back(triangle);
			mNumFaces++;
		}
	}
	file.close();
}

void ObjSurfaceMesh::addPoint(double x, double y, double z) {
	mBoundingBox.addPoint(x, y, z);
}

void ObjSurfaceMesh::loadBoundingBoxedObjFromFile(const std::string &filename) {
	std::ifstream file;
	file.open(filename);

	if (!file.is_open()) {
		std::cout << "Could not open OBJ file " << filename << std::endl;
		exit(1);
	}

	std::cout << "Loading obj file " << filename << std::endl;

	std::vector<Eigen::Vector3d> mInputPositions;
	std::vector<Eigen::Vector3d> mInputNormals;
	std::vector<Eigen::Vector2d> mInputTexCoords;

	std::string s;
	while (file >> s) {
		if (s.compare("mtllib") == 0) {
			std::string filename;
			file >> filename;
			createBlinnPhongMaterialFromFile(filename);
		}
		else if (s.compare("v") == 0) {
			double x, y, z;
			file >> x >> y >> z;
			mInputPositions.push_back(Eigen::Vector3d(x, y, z));
			addPoint(x, y, z);
			++mNumVertices;
		}
		else if (s.compare("vt") == 0) {
			containsTexCoords = true;
			float u, v;
			file >> u >> v;
			mInputTexCoords.push_back(Eigen::Vector2d(u, v));
		}
		else if (s.compare("vn") == 0) {
			containsNormals = true;
			float x, y, z;
			file >> x >> y >> z;
			mInputNormals.push_back(Eigen::Vector3d(x, y, z));
		}
		else if (s.compare("f") == 0) {
			Triangle triangle;

			std::string s1, s2, s3;
			file >> s1 >> s2 >> s3;
			size_t s1FoundSlash1 = s1.find("/");
			size_t s2FoundSlash1 = s2.find("/");
			size_t s3FoundSlash1 = s3.find("/");

			unsigned int v1 = std::stoi(s1.substr(0, s1FoundSlash1)) - 1;
			unsigned int v2 = std::stoi(s2.substr(0, s2FoundSlash1)) - 1;
			unsigned int v3 = std::stoi(s3.substr(0, s3FoundSlash1)) - 1;

			triangle.vertexIndices[0] = v1;
			triangle.vertexIndices[1] = v2;
			triangle.vertexIndices[2] = v3;
			triangle.vertices[0] = mInputPositions[v1];
			triangle.vertices[1] = mInputPositions[v2];
			triangle.vertices[2] = mInputPositions[v3];


			size_t s1FoundSlash2 = s1.find("/", s1FoundSlash1 + 1, 1);
			size_t s2FoundSlash2 = s2.find("/", s2FoundSlash1 + 1, 1);
			size_t s3FoundSlash2 = s3.find("/", s3FoundSlash1 + 1, 1);

			if (containsTexCoords) {

				unsigned int vt1 = std::stoi(s1.substr(s1FoundSlash1 + 1, s1FoundSlash2 - s1FoundSlash1 - 1)) - 1;
				unsigned int vt2 = std::stoi(s2.substr(s2FoundSlash1 + 1, s2FoundSlash2 - s2FoundSlash1 - 1)) - 1;
				unsigned int vt3 = std::stoi(s3.substr(s3FoundSlash1 + 1, s3FoundSlash2 - s3FoundSlash1 - 1)) - 1;
				triangle.texCoordIndices[0] = vt1;
				triangle.texCoordIndices[1] = vt2;
				triangle.texCoordIndices[2] = vt3;
				triangle.texCoords[0] = mInputTexCoords[vt1];
				triangle.texCoords[1] = mInputTexCoords[vt2];
				triangle.texCoords[2] = mInputTexCoords[vt3];
			}
			if (containsNormals) {
				unsigned int vn1 = std::stoi(s1.substr(s1FoundSlash2 + 1)) - 1;
				unsigned int vn2 = std::stoi(s2.substr(s2FoundSlash2 + 1)) - 1;
				unsigned int vn3 = std::stoi(s3.substr(s3FoundSlash2 + 1)) - 1;
				triangle.normalIndices[0] = vn1;
				triangle.normalIndices[1] = vn2;
				triangle.normalIndices[2] = vn3;
				triangle.normals[0] = mInputNormals[vn1];
				triangle.normals[1] = mInputNormals[vn2];
				triangle.normals[2] = mInputNormals[vn3];
			}
			mTriangles.push_back(triangle);
			mNumFaces++;
		}
	}
	file.close();
	gBoundingBox->add(mBoundingBox);
	createAttrib();
}

void ObjSurfaceMesh::saveObj2File(const std::string &filename) {
	std::ofstream file(filename, std::ofstream::out);
	for (int i = 0; i < mInputPositions.size(); i++) {
		file << "v " << mInputPositions[i].x() << " " << mInputPositions[i].y() << " " << mInputPositions[i].z() << std::endl;
 	}
	if (containsNormals) {
		for (int i = 0; i < mInputNormals.size(); i++) {
			file << "vn " << mInputNormals[i].x() << " " << mInputNormals[i].y() << " " << mInputNormals[i].z() << std::endl;
		}
	}
	if (containsTexCoords) {
		for (int i = 0; i < mInputTexCoords.size(); i++) {
			file << "vn " << mInputTexCoords[i].x() << " " << mInputTexCoords[i].y() << std::endl;
		}
	}
	file << "# mm_gid 0" << std::endl;
	file << "g mmGroup0" << std::endl;

	for (int i = 0; i < mTriangles.size(); i++) {
		for (int j = 0; j < 3; j++) {
			if (j == 0) {
				file << "f ";
			}
			file << mTriangles[i].vertexIndices[j] + 1 << "/";
			if (containsTexCoords) {
				file << mTriangles[i].texCoordIndices[j] + 1;
			}
			file << "/";
			if (containsNormals) {
				file << mTriangles[i].normalIndices[j] + 1;
			}
			if (j < 2) {
				file << " ";
			}
		}
		file << std::endl;
	}
	file.close();
}

ObjSurfaceMesh::ObjSurfaceMesh(const ObjSurfaceMesh *other) :mNumTexCoords(0), containsTexCoords(false) {
	if (other->containsNormals) {
		containsNormals = other->containsNormals;
		mNumNormals = other->mNumNormals;
		mNormals = other->mNormals;
	}

	if (other->containsTexCoords) {
		containsTexCoords = other->containsTexCoords;
		mNumTexCoords = other->mNumTexCoords;
		mTexCoords = other->mTexCoords;
	}

	mNumVertices = other->mNumVertices;
	mNumFaces = other->mNumFaces;

	mPositions = other->mPositions;
	mIndices = other->mIndices;
	mTriangles = other->mTriangles;
#if !defined(BUILD_MUSCLE_ACTIVATION_LIB)
	mBufferObjects = other->mBufferObjects;
	mMaterial = other->mMaterial->clone();
	glGenVertexArrays(1, &mVertexArrayObject);
#endif
}

void ObjSurfaceMesh::drawArray() const{
	mMaterial->bind();
	glBindVertexArray(mVertexArrayObject);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glDrawArrays(GL_TRIANGLES, 0, mNumFaces * 3);
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glBindVertexArray(0);
}

void ObjSurfaceMesh::translate(const Eigen::Vector3d &translation) {
	for (int i = 0; i < mTriangles.size(); i++) {
		for (int j = 0; j < 3; j++) {
			mTriangles[i].vertices[j] += translation;
		}
	}
	createAttrib();
}


void ObjSurfaceMesh::scale(double scale) {
	for (int i = 0; i < mTriangles.size(); i++) {
		for (int j = 0; j < 3; j++) {
			mTriangles[i].vertices[j] *= scale;
		}
	}
	createAttrib();
}


void ObjSurfaceMesh::setDiffuseColor(const Eigen::Vector3f &color) {
	mMaterial->setDiffuseColor(color);
}

MeshSurfaceMesh::MeshSurfaceMesh(const MeshSurfaceMesh *other) {
	if (!other) {
		std::cout << "base class SurfaceMesh pointer is null" << std::endl;
		return;
	}
	containsNormals = other->containsNormals;

	mNumVertices = other->mNumVertices;
	mNumNormals = other->mNumNormals;
	mNumFaces = other->mNumFaces;

	mPositions = other->mPositions;
	mNormals = other->mNormals;
	mIndices = other->mIndices;

	mTriangles = other->mTriangles;

#if !defined(BUILD_MUSCLE_ACTIVATION_LIB)
	mMaterial = other->mMaterial->clone();
	glGenVertexArrays(1, &mVertexArrayObject);
#endif
	mSurface2Tetrahedral = other->mSurface2Tetrahedral;
}

MeshSurfaceMesh &MeshSurfaceMesh::operator=(const MeshSurfaceMesh &rhs) {
	containsNormals = rhs.containsNormals;

	mNumVertices = rhs.mNumVertices;
	mNumNormals = rhs.mNumNormals;
	mNumFaces = rhs.mNumFaces;

	mPositions = rhs.mPositions;
	mNormals = rhs.mNormals;
	mIndices = rhs.mIndices;

	mTriangles = rhs.mTriangles;

#if !defined(BUILD_MUSCLE_ACTIVATION_LIB)
	mBufferObjects = rhs.mBufferObjects;
	if(rhs.mMaterial) mMaterial = rhs.mMaterial->clone();
	glGenVertexArrays(1, &mVertexArrayObject);
#endif
	mSurface2Tetrahedral = rhs.mSurface2Tetrahedral;
	return *this;
}

void MeshSurfaceMesh::drawModel() const {
	//TODO remove the hard coded attribId 0 and 1
	glBindVertexArray(mVertexArrayObject);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glDrawElements(GL_TRIANGLES, mNumFaces * 3, GL_UNSIGNED_INT, NULL);
	//glDrawElements(GL_LINES, mNumFaces * 3, GL_UNSIGNED_INT, NULL);
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glBindVertexArray(0);
}

void MeshSurfaceMesh::drawWireframe() const {
	//TODO remove the hard coded attribId 0 and 1
	glBindVertexArray(mVertexArrayObject);
	glEnableVertexAttribArray(0);
	glDrawElements(GL_LINES, mNumFaces * 3, GL_UNSIGNED_INT, NULL);
	glDisableVertexAttribArray(0);
	glBindVertexArray(0);
}





void MeshSurfaceMesh::draw() const {
	if (!mMaterial) {
		return;
	}
	mMaterial->bind();
	drawModel();
}

class WholeScreenQuad {
public:
	WholeScreenQuad() {
		pos.col(0) = Eigen::Vector3d(-1, -1, 0);
		pos.col(1) = Eigen::Vector3d(1, -1, 0);
		pos.col(2) = Eigen::Vector3d(1, 1, 0);
		pos.col(3) = Eigen::Vector3d(-1, 1, 0);
		indices.col(0) = Eigen::Vector3i(0, 1, 3);
		indices.col(1) = Eigen::Vector3i(3, 1, 2);
	}

	void uploadAttrib() {
		glGenVertexArrays(1, &mVertexArrayObject);
		glBindVertexArray(mVertexArrayObject);
		size_t totalVertexSize = pos.size() * sizeof(double);
		glGenBuffers(1, &mVertexBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, mVertexBuffer);
		glBufferData(GL_ARRAY_BUFFER, totalVertexSize, pos.data(), GL_STATIC_DRAW);

		size_t totalIndexSize = indices.size() * sizeof(int);
		glGenBuffers(1, &mIndexBuffer);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIndexBuffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, totalIndexSize, indices.data(), GL_STATIC_DRAW);
	}

	void draw() const {
		glBindVertexArray(mVertexArrayObject);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_DOUBLE, GL_FALSE, 0, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIndexBuffer);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, NULL);
		glDisableVertexAttribArray(0);
		glBindVertexArray(0);
	}

private:
	Eigen::Matrix<double, 3, 4> pos;
	Eigen::Matrix<int, 3, 2> indices;

	GLuint mVertexBuffer;
	GLuint mIndexBuffer;
	GLuint mVertexArrayObject;
};

void MeshSurfaceMesh::drawTransparent() const {
	//this is still a huge mess. TODO make it cleaner
	const Transparent *tp = dynamic_cast<const Transparent *>(mMaterial);

	static WholeScreenQuad quad;
	quad.uploadAttrib();
	
	// ---------------------------------------------------------------------
	// 1. Initialize Min Depth Buffer
	// ---------------------------------------------------------------------
	glBindFramebuffer(GL_FRAMEBUFFER, tp->getFrontColorBlenderFboId());

	glDrawBuffer(GL_COLOR_ATTACHMENT0);

	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glEnable(GL_DEPTH_TEST);

	mSimulatableMesh->drawInteriorSurface();
	tp->getShaderFrontInit()->bind();
	drawModel();
	tp->getShaderFrontInit()->unbind();

	// ---------------------------------------------------------------------
	// 2. Depth Peeling + Blending
	// ---------------------------------------------------------------------
	int numLayers = (tp->getNumPasses() - 1) * 2;
	for (int layer = 1; layer < numLayers; layer++) {
		int currId = layer % 2;
		int prevId = 1 - currId;
		glBindFramebuffer(GL_FRAMEBUFFER, tp->getFrontFboId(currId));
		glDrawBuffer(GL_COLOR_ATTACHMENT0);

		glClearColor(0, 0, 0, 0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glDisable(GL_BLEND);
		glEnable(GL_DEPTH_TEST);

		mSimulatableMesh->drawInteriorSurface();
		tp->getShaderFrontPeel()->bind();
		tp->getShaderFrontPeel()->bindTextureRECT("DepthTex", tp->getFrontDepthTexId(prevId), 0);
		drawModel();
		tp->getShaderFrontPeel()->unbind();

		/////////////////////////////////////////////////////////////////////////////////
		glBindFramebuffer(GL_FRAMEBUFFER, tp->getFrontColorBlenderFboId());
		glDrawBuffer(GL_COLOR_ATTACHMENT0);

		glDisable(GL_DEPTH_TEST);
		glEnable(GL_BLEND);

		glBlendEquation(GL_FUNC_ADD);
		glBlendFuncSeparate(GL_DST_ALPHA, GL_ONE,
			GL_ZERO, GL_ONE_MINUS_SRC_ALPHA);

		tp->getShaderFrontBlend()->bind();
		tp->getShaderFrontBlend()->bindTextureRECT("TempTex", tp->getFrontColorTexId(currId), 0);
		quad.draw();
		tp->getShaderFrontBlend()->unbind();
		glDisable(GL_BLEND);
	}

	// ---------------------------------------------------------------------
	// 3. Final Pass
	// ---------------------------------------------------------------------

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glDrawBuffer(GL_BACK);
	glDisable(GL_DEPTH_TEST);

	tp->getShaderFrontFinal()->bind();
	tp->getShaderFrontFinal()->bindTextureRECT("ColorTex", tp->getFrontColorBlenderTexId(), 0);
	quad.draw();
	tp->getShaderFrontFinal()->unbind();
}

void MeshSurfaceMesh::saveObj2File(const std::string &filename) {
	std::ofstream file(filename, std::ofstream::out);
	for (int i = 0; i < mPositions.cols(); i++) {
		file << "v " << mPositions.col(i).transpose() << std::endl;
	}
	if (containsNormals) {
		for (int i = 0; i < mNormals.cols(); i++) {
			file << "vn " << mNormals.col(i).transpose() << std::endl;
		}
	}

	file << "# mm_gid 0" << std::endl;
	file << "g mmGroup0" << std::endl;

	for (int i = 0; i < mTriangles.size(); i++) {
		for (int j = 0; j < 3; j++) {
			if (j == 0) {
				file << "f ";
			}
			file << mTriangles[i].vertexIndices[j] + 1 << "/";
			file << "/";
			if (containsNormals) {
				file << mTriangles[i].normalIndices[j] + 1;
			}
			if (j < 2) {
				file << " ";
			}
		}
		file << std::endl;
	}
	file.close();
}

int HandleSurfaceMesh::highlightSelectedHandles(const Ray *r) {
	int selectedIndex = intersectHandles(r);
	if (selectedIndex != -1) {
		mHandles[selectedIndex]->drag();
		mHandles[selectedIndex]->setDiffuseColor(Eigen::Vector3f(1.0, 1.0, 0.0));
	}
	return selectedIndex;
}

void HandleSurfaceMesh::highlightSelectedHandles(const std::vector<int> &surfaceIndices) {
	for (int surfaceIndex : surfaceIndices) {
		mHandles[surfaceIndex]->drag();
		mHandles[surfaceIndex]->setDiffuseColor(Eigen::Vector3f(1.0, 1.0, 0.0));
	}
}


int HandleSurfaceMesh::fixSelectedHandles(const Ray *r) {
	int selectedIndex = intersectHandles(r);
	if (selectedIndex != -1) {
		mHandles[selectedIndex]->fix();
		mHandles[selectedIndex]->setDiffuseColor(Eigen::Vector3f(0.0, 1.0, 0.0));
		return mHandles[selectedIndex]->getTetrahedralIndex();
	}
	return -1;
}

void HandleSurfaceMesh::unfixUnDragAllHandles() {
	for (int i = 0; i < mHandles.size(); i++) {
		mHandles[i]->unfix();
		mHandles[i]->undrag();
		mHandles[i]->setDiffuseColor(Eigen::Vector3f(1.0, 0.0, 0.0));
	}
}

void HandleSurfaceMesh::fixSelectedHandles(const std::vector<int> &surfaceIndices) {
	for (int surfaceIndex : surfaceIndices) {
		mHandles[surfaceIndex]->fix();
		mHandles[surfaceIndex]->setDiffuseColor(Eigen::Vector3f(0.0, 1.0, 0.0));
	}
}

int HandleSurfaceMesh::intersectHandles(const Ray *r) {
	Intersection minTimeIntersection;
	minTimeIntersection.t = std::numeric_limits<float>::max();
	int selectedIndex = -1;
	int selectedTetrahedralIndex = -1;
	for (int i = 0; i < mHandles.size(); i++) {
		Intersection intersection;
		if (mHandles[i]->intersect(r, intersection)) {
			if (intersection.t < minTimeIntersection.t) {
				minTimeIntersection.t = intersection.t;
				selectedIndex = i;
				selectedTetrahedralIndex = mHandles[i]->getTetrahedralIndex();
			}
		}
	}
	return selectedIndex;
}

void HandleSurfaceMesh::createHandles(const std::vector<Eigen::Vector3d> &mInputPositions) {
	//initialize handles on each vertices
	if (mHandles.size() == 0) {
		mHandles.reserve(mNumVertices);
		for (int i = 0; i < mNumVertices; i++) {
			mHandles.push_back(new Handle(mInputPositions[i], mSurface2Tetrahedral[i]));
		}
	}
	else {
		for (int i = 0; i < mNumVertices; i++) {
			mHandles[i]->translate(mInputPositions[i]);
		}
	}
}

void HandleSurfaceMesh::turnOnVertices() {
	for (int i = 0; i < mNumVertices; i++) {
		mHandles[i]->draw();
	}
}

void HandleSurfaceMesh::updateTransformationUniform(const MyGLCanvas *canvas) {
	SurfaceMesh::updateTransformationUniform(canvas);
	for (int i = 0; i < mHandles.size(); i++) {
		mHandles[i]->updateTransformationUniform(canvas);
	}
}

void HandleSurfaceMesh::addPoint(double x, double y, double z) {
	mBoundingBox.addPoint(x, y, z);
}

void HandleSurfaceMesh::createAttrib(const std::vector<Eigen::Vector3d> &inputPositions,
	const std::vector<Eigen::Vector3i> &inputIndices) {
	MeshSurfaceMesh::createAttrib(inputPositions, inputIndices);
	createHandles(inputPositions);
}

HandleSurfaceMesh::HandleSurfaceMesh(const HandleSurfaceMesh *other):MeshSurfaceMesh(dynamic_cast<const MeshSurfaceMesh *>(other)) {
	mHasHandles = other->mHasHandles;
	for (auto h : other->mHandles) {
		mHandles.push_back(new Handle(h));
	}
	mBoundingBox = other->mBoundingBox;
}

HandleSurfaceMesh &HandleSurfaceMesh::operator=(const HandleSurfaceMesh &rhs) {
	containsNormals = rhs.containsNormals;

	mNumVertices = rhs.mNumVertices;
	mNumNormals = rhs.mNumNormals;
	mNumFaces = rhs.mNumFaces;

	mPositions = rhs.mPositions;
	mNormals = rhs.mNormals;
	mIndices = rhs.mIndices;

	mTriangles = rhs.mTriangles;

#if !defined(BUILD_MUSCLE_ACTIVATION_LIB)
	mMaterial = rhs.mMaterial->clone();
	glGenVertexArrays(1, &mVertexArrayObject);
#endif
	mSurface2Tetrahedral = rhs.mSurface2Tetrahedral;
	return *this;
	mHasHandles = rhs.mHasHandles;
	for (auto h : rhs.mHandles) {
		mHandles.push_back(new Handle(h));
	}
	mBoundingBox = rhs.mBoundingBox;
	return *this;
}






