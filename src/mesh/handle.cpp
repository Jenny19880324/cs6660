#include "handle.h"
#include "mesh.h"

extern BoundingBox *gBoundingBox;
double Handle::sScale;
ObjSurfaceMesh* Handle::sSphere;
Handle::LoadSphereFromFile Handle::sLoader;  //define our static loader, which will call LoadSphereFromFile constructor, which will initilize mSphere

Handle::Handle(const Handle &other) {
	mIsFixed = other.mIsFixed;
	mIsDragged = other.mIsDragged;
	mCenter = other.mCenter;
	mtetrahedralIndex = other.mtetrahedralIndex;
	mSphere = new ObjSurfaceMesh(sSphere);
	mSphere->setMaterial(sSphere->getMaterial());
	mSphere->scale(sScale);
	mSphere->translate(mCenter);
}

Handle::Handle(const Handle *other) {
	mIsFixed = other->mIsFixed;
	mIsDragged = other->mIsDragged;
	mCenter = other->mCenter;
	mtetrahedralIndex = other->mtetrahedralIndex;
	mSphere = new ObjSurfaceMesh(sSphere);
	mSphere->setMaterial(sSphere->getMaterial());
	mSphere->scale(sScale);
	mSphere->translate(mCenter);
}

Handle::LoadSphereFromFile::LoadSphereFromFile() {
	sSphere = new ObjSurfaceMesh("../model/sphere.obj");
	sSphere->createBlinnPhongMaterialFromFile("sphere.mtl");
}

Handle::Handle(const Eigen::Vector3d &center, int tetrahedralIndex):
	mCenter(center), 
	mtetrahedralIndex(tetrahedralIndex),
	mIsFixed(false), mIsDragged(false){
#if !defined(BUILD_MUSCLE_ACTIVATION_LIB)
	mSphere = new ObjSurfaceMesh(sSphere);
	mSphere->setMaterial(sSphere->getMaterial());
	mSphere->scale(sScale);
	mSphere->translate(mCenter);
#endif
}

void Handle::translate(const Eigen::Vector3d &translation){
	mSphere->translate(-mCenter);
	mSphere->translate(translation);
	mCenter = translation;
}

void Handle::scale(float scale){
	mSphere->scale(scale);
}

void Handle::draw(){
	mSphere->drawArray();
}

void Handle::setDiffuseColor(const Eigen::Vector3f &color){
	mSphere->setDiffuseColor(color);
};

void Handle::fix(){
	mIsFixed = true;
}

void Handle::unfix() {
	mIsFixed = false;
}

bool Handle::isFixed() const {
	return mIsFixed;
}

void Handle::drag() {
	mIsDragged = true;
}

void Handle::undrag() {
	mIsDragged = false;
}

bool Handle::isDragged() const {
	return mIsDragged;
}

bool Handle::intersect(const Ray *r, Intersection &minTimeIntersection) const {
	return mSphere->intersect(r, minTimeIntersection);
}

void Handle::updateTransformationUniform(const MyGLCanvas *canvas) {
	mSphere->updateTransformationUniform(canvas);
}

