#pragma once
#include <Eigen/Core>
#include "handle.h"
#include "typedef.h"
#include "tet.h"
#include "material.h"

class SimulatableMesh {
public:
	SimulatableMesh(const SimulatableMesh *other);
	virtual ~SimulatableMesh() {};
	virtual void loadMeshFromFile(const std::string &filename) = 0;
	virtual void saveConfigToFile(const std::string &filename) = 0;
	virtual void loadConfigFromFile(const std::string &filename) = 0;
	virtual void saveObjs() = 0;
	virtual int getNumTetrahedra() const = 0;
	virtual int getNumHandles() const = 0;
	virtual int getNumVertices() const = 0;
	virtual const Tet &getTetrahedra(int i) const = 0;
	virtual const Eigen::Vector3d &getVertices(int i) const = 0;
	virtual const Handle *getHandles(int i) const = 0;
	virtual Handle *getHandles(int i) = 0;
	virtual void setVertices(const VectorX &x) = 0;
	virtual int highlightSelectedHandles(const Ray *r) = 0;
	virtual int fixSelectedHandles(const Ray *r) = 0;
	virtual void fixSelectedHandles(const std::vector<int> &surfaceIndices) = 0;
	virtual void highlightSelectedHandles(const std::vector<int> &surfaceIndices) = 0;
	virtual void moveVertices(const std::vector<int> &tetrahedralIndex, const Eigen::Vector3d &translation) = 0;
	virtual void moveVertex(int tetrahedralIndex, const Eigen::Vector3d &translation) = 0;
	virtual void turnOnVertices() = 0;
	virtual void updateTransformationUniform(const MyGLCanvas *canvas) = 0;
	virtual void drawModel() const = 0;
	virtual void drawExteriorSurface() const = 0;
	virtual void drawInteriorSurface() const = 0;
	virtual void drawTetrahedra() const = 0;
	virtual void drawTransparent() const = 0;
	virtual bool getHasTexture() const = 0;
	virtual int getSurfaceIndex(int tetrahedralIndex) const = 0;
	virtual int getTetrahedralIndex(int surfaceIndex) const = 0;
	virtual void surfaceMeshCreateAttrib() = 0;
	virtual void unfixUnDragAllHandles() = 0;
	virtual void copyVerticesFrom(SimulatableMesh *mesh);

protected:
	SimulatableMesh() {}
	bool hasTexture;
	int mNumVertices;
	int mNumTriangles;
	int mNumTetrahedra;
	float mVolumeRatio;
	std::vector<Eigen::Vector3d> mVertices;
	std::vector<Eigen::Vector3i> mTriangles;
	std::vector<Tet> mTetrahedra;

	Eigen::Matrix3Xd mPositions;
	Eigen::Matrix3Xi mIndices;
};
