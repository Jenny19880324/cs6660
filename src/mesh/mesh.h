#pragma once

#include <Eigen/core>
#include <limits>
#include <vector>
#include <unordered_map>
#include <memory>

#include "boundingBox.h"
#include "material.h"
#include "handle.h"

class Camera;
class SimulatableMesh;

struct Ray{
	Eigen::Vector3d e;
	Eigen::Vector3d d;
	Ray(const Eigen::Vector3d &eye, const Eigen::Vector3d &dir): e(eye), d(dir){}
};

struct Intersection{
	Eigen::Vector3d pos;
	Eigen::Vector3d bary; // triangle barycentric coords
	double t;
};

struct Triangle{
	Eigen::Vector3d vertices[3];
	Eigen::Vector3d normals[3];
	Eigen::Vector2d texCoords[3];
	unsigned int vertexIndices[3];
	unsigned int normalIndices[3];
	unsigned int texCoordIndices[3];

	bool intersect(const Ray *r, Intersection &intersection) const {
		Eigen::Vector3d v0 = vertices[0];
		Eigen::Vector3d v1 = vertices[1];
		Eigen::Vector3d v2 = vertices[2];

		Eigen::Vector3d e1 = v1 - v0;
		Eigen::Vector3d e2 = v2 - v0;
	    Eigen::Vector3d h = r->d.cross(e2);

	    double a = e1.dot(h);
	    if (std::abs(a) < 1e-8){
			return false;
	    }

		double f = 1 / a;
		Eigen::Vector3d s = r->e - v0;
		double u = f * s.dot(h);
		if (u < 0.0 || u > 1.0){
			return false ;
		}

		Eigen::Vector3d q = s.cross(e1);
		double v = f * r->d.dot(q);
		if (v < 0.0 || u + v > 1.0){
			return false;
		}
		
		double time = f * e2.dot(q);
		if (time < 0.00001){
			return false;
		}
		
		intersection.t = time;
		intersection.bary.x() = 1 - u - v;
		intersection.bary.y() = u;
		intersection.bary.z() = v;
		return true;
	}
};


class SurfaceMesh {
public:
	SurfaceMesh(const SurfaceMesh *other);
	SurfaceMesh(const SurfaceMesh &other);
	~SurfaceMesh();

	void setNumVertices(int numVertices) { mNumVertices = numVertices; }
	void setNumFaces(int numFaces) { mNumFaces = numFaces; }

	int getNumVertices() const { return mNumVertices; }
	int getNumNormals() const { return mNumNormals; }
	int getNumFaces() const { return mNumFaces; }
	bool getContainsNormals() const { return containsNormals; }

	const Eigen::Matrix3Xd getPositions() const { return mPositions; }
	const Eigen::Matrix3Xd getNormals() const { return mNormals; }
	const Eigen::Matrix3Xi getIndices() const { return mIndices; }
	std::vector<Triangle> getTriangles() const { return mTriangles; }

	void setMaterial(const Material *material);
	const Material *getMaterial() const { return mMaterial; }
	Material *getMaterial() { return mMaterial; }
	void updateTransformationUniform(const MyGLCanvas *canvas);
	template <typename Matrix> void uploadAttrib(const std::string &name, const Matrix &M, int version = -1);
	void uploadAttrib(const std::string &name, size_t size, int dim,
		uint32_t compSize, GLuint glType, bool integral,
		const void *data, int version);

	void createBlinnPhongMaterialFromFile(const std::string &filename);
	void createTexturedBlinnPhongMaterialFromFile(const std::string &filename);
	void createTransparentMaterial();

	Material *mMaterial;

protected:
	SurfaceMesh() :mNumVertices(0), mNumNormals(0), mNumFaces(0), containsNormals(false), mMaterial(0) {} // Surface Mesh is a base class which has no instance.
	bool containsNormals;

	int mNumVertices;
	int mNumNormals;
	int mNumFaces;

	Eigen::Matrix3Xd mPositions;
	Eigen::Matrix3Xd mNormals;
	Eigen::Matrix3Xi mIndices;

	std::vector<Triangle> mTriangles;

	struct Buffer {
		GLuint id;
		GLuint glType;
		GLuint dim;
		GLuint compSize;
		GLuint size;
		int version;
	};

	GLuint mVertexArrayObject;
	std::map<std::string, Buffer> mBufferObjects;

	void free();
};


class ObjSurfaceMesh : public SurfaceMesh {
public:
	//use this constructor when read obj from file
	ObjSurfaceMesh():mNumTexCoords(0), containsTexCoords(false) { glGenVertexArrays(1, &mVertexArrayObject); }
	//use this constructor when initializing ssphere. Because no OpenGL code is here.
	ObjSurfaceMesh(const std::string &filename) :mNumTexCoords(0), containsTexCoords(false) { loadObjFromFile(filename); }
	//use this constructor when copying ssphere to msphere
	ObjSurfaceMesh(const ObjSurfaceMesh *other);
	void translate(const Eigen::Vector3d &translation);
	void scale(double scale);
	void drawArray() const;
	void setDiffuseColor(const Eigen::Vector3f &color);

	void loadObjFromFile(const std::string &filename);
	void addPoint(double x, double y, double z);
	void loadBoundingBoxedObjFromFile(const std::string &filename);
	void loadMtlFromFile(const std::string &filename) { mMaterial->loadMtlFromFile(filename); }
	void saveObj2File(const std::string &filename);
	bool intersect(const Ray *r, Intersection &minTimeIntersection) const;
	const Eigen::Matrix2Xd getTexCoords() const { return mTexCoords; }
	int getNumTexCoords() const { return mNumTexCoords; }
	bool getContainsTexCoords() const { return containsTexCoords; }

private:
	std::vector<Eigen::Vector3d> mInputPositions; //to save the output .obj
	std::vector<Eigen::Vector3d> mInputNormals;   //to save the output .obj
	std::vector<Eigen::Vector2d> mInputTexCoords; //to save the output .obj

	bool containsTexCoords;
	int mNumTexCoords;
	Eigen::Matrix2Xd mTexCoords;
	BoundingBox mBoundingBox;

	void createAttrib();
};


class MeshSurfaceMesh : public SurfaceMesh {
public:
#if !defined(BUILD_MUSCLE_ACTIVATION_LIB)
	MeshSurfaceMesh() { containsNormals = true; glGenVertexArrays(1, &mVertexArrayObject); }
#else
	MeshSurfaceMesh() {}
#endif

	MeshSurfaceMesh(const MeshSurfaceMesh *other);
	MeshSurfaceMesh &operator=(const MeshSurfaceMesh &other);
	void setSurface2Tetrahedral(const std::unordered_map<int, int> &surface2Tetrahedral) { mSurface2Tetrahedral = surface2Tetrahedral; }
	const std::unordered_map<int, int> &getSurface2Tetrahedral() const { return mSurface2Tetrahedral; }
	void setSimulatableMesh(SimulatableMesh *simulatableMesh) { mSimulatableMesh = simulatableMesh; }

	void drawModel() const;
	void draw() const;
	void drawTransparent() const;
	void drawWireframe() const;
	void createAttrib(const std::vector<Eigen::Vector3d> &inputPositions,
		const std::vector<Eigen::Vector3i> &inputIndices);
	void saveObj2File(const std::string &filename);

protected:
	std::unordered_map<int, int> mSurface2Tetrahedral;
	SimulatableMesh *mSimulatableMesh;
};

class HandleSurfaceMesh : public MeshSurfaceMesh {
public:
	HandleSurfaceMesh() {}
	HandleSurfaceMesh(const HandleSurfaceMesh *other);
	HandleSurfaceMesh &operator=(const HandleSurfaceMesh &other);
	void updateTransformationUniform(const MyGLCanvas *canvas);
	int highlightSelectedHandles(const Ray *r);
	void highlightSelectedHandles(const std::vector<int> &surfaceIndices);
	int fixSelectedHandles(const Ray *r);
	void fixSelectedHandles(const std::vector<int> &surfaceIndices);
	int intersectHandles(const Ray *r);
	void turnOnVertices();
	void createAttrib(const std::vector<Eigen::Vector3d> &inputPositions,
		const std::vector<Eigen::Vector3i> &inputIndices);
	void createHandles(const std::vector<Eigen::Vector3d> &inputPositions);
	bool getHasHandles() const { return mHasHandles; }
	int getNumHandles() const { return mHandles.size(); }
	const Handle *getHandles(int i) const { return mHandles[i]; }
	Handle *getHandles(int i) { return mHandles[i]; }
	const std::vector<Handle *> &getHandles() const { return mHandles; }
	const BoundingBox &getBoundingBox() const { return mBoundingBox; }
	void addPoint(double x, double y, double z);
	void unfixUnDragAllHandles();
private:
	bool mHasHandles;
	std::vector<Handle *> mHandles;
	BoundingBox mBoundingBox;
};

