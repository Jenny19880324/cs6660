#include <iostream>
#include <cassert>

#include "tetrahedralMesh.h"

extern BoundingBox *gBoundingBox;

TetrahedralMesh::TetrahedralMesh(const SimulatableMesh *base) {
	const TetrahedralMesh *other = dynamic_cast<const TetrahedralMesh *>(base);
	if (!other) {
		std::cout << "Couldn't convert to TetrahedralMesh" << std::endl;
		return;
	}
	mSurfaceVertices = other->mSurfaceVertices;
	mSurfaceTriangles = other->mSurfaceTriangles;
	mSurfaceMesh = new HandleSurfaceMesh(other->mSurfaceMesh);
	mTetrahedral2Surface = other->mTetrahedral2Surface;
	mSurface2Tetrahedral = other->mSurface2Tetrahedral;
	mSurfaceMesh->createAttrib(mSurfaceVertices, mSurfaceTriangles);
}

void TetrahedralMesh::loadMeshFromFile(const std::string &filename){
	SimulatableMesh::loadMeshFromFile(filename);

	for (int i = 0; i < mTriangles.size(); i++) {
		Eigen::Vector3i idx;
		for (int j = 0; j < 3; j++) {
			bool ok = mTetrahedral2Surface.insert({ mTriangles[i][j], (int)mSurfaceVertices.size() }).second;
			if (ok) {
				mSurface2Tetrahedral.insert({ (int)mSurfaceVertices.size(), mTriangles[i][j] });
				idx(j) = mSurfaceVertices.size();
				mSurfaceVertices.push_back(mVertices[mTriangles[i][j]]);
			}
			else {
				idx(j) = mTetrahedral2Surface[mTriangles[i][j]];
			}
		}
		mSurfaceTriangles.push_back(idx);
	}

#if !defined(BUILD_MUSCLE_ACTIVATION_LIB)
	mSurfaceMesh->createBlinnPhongMaterialFromFile("bone.mtl");
	for (int i = 0; i < mSurfaceVertices.size(); i++) {
		mSurfaceMesh->addPoint(mSurfaceVertices[i].x(), mSurfaceVertices[i].y(), mSurfaceVertices[i].z());
	}
	gBoundingBox->add(mSurfaceMesh->getBoundingBox());
	Handle::setScale(std::min(gBoundingBox->getXLength(), gBoundingBox->getYLength()) * 0.02);
	mSurfaceMesh->setNumVertices(mSurfaceVertices.size());
	mSurfaceMesh->setNumFaces(mSurfaceTriangles.size());
	mSurfaceMesh->setSurface2Tetrahedral(mSurface2Tetrahedral);
	mSurfaceMesh->createAttrib(mSurfaceVertices, mSurfaceTriangles);

#else
	mSurfaceMesh->setNumVertices(mSurfaceVertices.size());
	mSurfaceMesh->setNumFaces(mSurfaceTriangles.size());
	mSurfaceMesh->setSurface2Tetrahedral(mSurface2Tetrahedral);
	mSurfaceMesh->createHandles(mSurfaceVertices);
#endif


}

void TetrahedralMesh::moveVertex(int tetrahedralIndex, const Eigen::Vector3d &translation){
	int surfaceIndex = mTetrahedral2Surface[tetrahedralIndex];
	mVertices[tetrahedralIndex] += translation;
	mSurfaceVertices[surfaceIndex] += translation;
	mSurfaceMesh->createAttrib(mSurfaceVertices, mSurfaceTriangles);
}

void TetrahedralMesh::moveVertices(const std::vector<int> &tetrahedralIndices, const Eigen::Vector3d &translation) {
	for (int tetrahedralIndex : tetrahedralIndices) {
		int surfaceIndex = mTetrahedral2Surface[tetrahedralIndex];
		mVertices[tetrahedralIndex] += translation;
		mSurfaceVertices[surfaceIndex] += translation;
	}
	mSurfaceMesh->createAttrib(mSurfaceVertices, mSurfaceTriangles);
}

void TetrahedralMesh::setVertices(const VectorX &x){
	for(int i = 0; i < x.rows() / 3; i++){
		mVertices[i](0) = x(i * 3 + 0);
		mVertices[i](1) = x(i * 3 + 1);
		mVertices[i](2) = x(i * 3 + 2);
	}
}

void TetrahedralMesh::surfaceMeshCreateAttrib()  {
	for (int i = 0; i < mSurfaceVertices.size(); i++) {
		mSurfaceVertices[i] = mVertices[mSurface2Tetrahedral[i]];
	}
	mSurfaceMesh->createAttrib(mSurfaceVertices, mSurfaceTriangles);
}

void TetrahedralMesh::turnOnVertices(){
	mSurfaceMesh->turnOnVertices();
}

void TetrahedralMesh::updateTransformationUniform(const MyGLCanvas *canvas) {
	mSurfaceMesh->updateTransformationUniform(canvas); 
}

void TetrahedralMesh::saveConfigToFile(const std::string &filename) {
	std::ofstream configFile(filename);
	if (configFile.is_open())
	{
		configFile << "# Fixed Handles\n";
		std::vector<int> handles;
		for (int i = 0; i < mSurfaceMesh->getNumHandles(); i++) {
			if (mSurfaceMesh->getHandles(i)->isFixed()) {
				handles.push_back(i);
			}
		}
		configFile << "Handles " << handles.size() << std::endl;
		for (int i = 0; i < handles.size(); i++) {
			if (i == handles.size() - 1) {
				configFile << i << std::endl;
			}
			else {
				configFile << i << " ";
			}
		}
		configFile.close();
	}
	else {
		std::cout << "Unable to open file";
	}
}

void TetrahedralMesh::loadConfigFromFile(const std::string &filename) {
	std::ifstream file;
	file.open(filename);

	if (!file.is_open()) {
		std::cout << "Could not open CONFIG file " << filename << std::endl;
		exit(1);
	}

	std::cout << "Loading config file " << filename << std::endl;

	std::string s;
	//find fixed handles section
	do { std::getline(file, s); } while (s.substr(0, 15).compare("# Fixed Handles"));
	file >> s; assert(s.compare("Handles") == 0);
	int numFixedHandles;
	std::vector<int> fixedHandles;
	file >> numFixedHandles;
	for (int i = 0; i < numFixedHandles; i++) {
		int handleIndex;
		file >> handleIndex;
		mSurfaceMesh->getHandles(handleIndex)->fix();
		mSurfaceMesh->getHandles(handleIndex)->setDiffuseColor(Eigen::Vector3f(0.0, 1.0, 0.0));
	}
}

int TetrahedralMesh::getSurfaceIndex(int tetrahedralIndex) const {
	std::unordered_map<int, int>::const_iterator got = mTetrahedral2Surface.find(tetrahedralIndex);
	if (got == mTetrahedral2Surface.end()) {
		return -1;
	}
	else {
		return got->second;
	}
}

int TetrahedralMesh::getTetrahedralIndex(int surfaceIndex) const {
	std::unordered_map<int, int>::const_iterator got = mSurface2Tetrahedral.find(surfaceIndex);
	if (got == mSurface2Tetrahedral.end()) {
		return -1;
	}
	else {
		return got->second;
	}
}

int TetrahedralMesh::highlightSelectedHandles(const Ray *r) {
	int surfaceIndex = mSurfaceMesh->highlightSelectedHandles(r);
	if (mSurface2Tetrahedral.find(surfaceIndex) != mSurface2Tetrahedral.end()) {
		return mSurface2Tetrahedral[surfaceIndex];
	}
	return -1;
}

int TetrahedralMesh::fixSelectedHandles(const Ray *r) {
	int surfaceIndex = mSurfaceMesh->fixSelectedHandles(r);
	if (mSurface2Tetrahedral.find(surfaceIndex) != mSurface2Tetrahedral.end()) {
		return mSurface2Tetrahedral[surfaceIndex];
	}
	return -1;
}
