#pragma once
#include <fstream>
#include <string>
#include <vector>
#include <unordered_map>
#include <Eigen/Dense>
#include "mesh.h"
#include "handle.h"
#include "canvas.h"
#include "simulatableMesh.h"
#include "tet.h"
class Camera;

class TetrahedralMesh:public SimulatableMesh{
public:

	TetrahedralMesh() { mSurfaceMesh = new HandleSurfaceMesh(); }
	TetrahedralMesh(const std::string &filename) { mSurfaceMesh = new HandleSurfaceMesh(); loadMeshFromFile(filename); }
	TetrahedralMesh(const SimulatableMesh *base);
	~TetrahedralMesh() { delete mSurfaceMesh; }

	virtual void loadMeshFromFile(const std::string &filename) override;
	virtual void saveConfigToFile(const std::string &filename) override;
	virtual void loadConfigFromFile(const std::string &filename) override;
	virtual void saveObjs() override { /* TODO */ }
	
	const HandleSurfaceMesh *getSurfaceMesh() const {return mSurfaceMesh;}
	int getNumTriangles() const {return mNumTriangles;} // surface triangle
	const Eigen::Vector3i &getTriangles(int i) const {return mTriangles[i];}
	virtual int getNumTetrahedra() const override { return mNumTetrahedra; }
	virtual int getNumHandles() const override { return mSurfaceMesh->getNumHandles(); }
	virtual int getNumVertices() const override { return mNumVertices; }
	virtual const Tet &getTetrahedra(int i) const override { return mTetrahedra[i]; }
	virtual const Eigen::Vector3d &getVertices(int i) const override { return mVertices[i]; }
	virtual const Handle *getHandles(int i) const override { return mSurfaceMesh->getHandles(i); }
	virtual Handle *getHandles(int i) override { return mSurfaceMesh->getHandles(i); }
	virtual void setVertices(const VectorX &x) override;
	virtual int highlightSelectedHandles(const Ray *r) override;
	virtual int fixSelectedHandles(const Ray *r) override;
	virtual void fixSelectedHandles(const std::vector<int> &surfaceIndices) override { return mSurfaceMesh->fixSelectedHandles(surfaceIndices); }
	virtual void highlightSelectedHandles(const std::vector<int> &surfaceIndices) override { return mSurfaceMesh->highlightSelectedHandles(surfaceIndices); }
	virtual void moveVertices(const std::vector<int> &tetrahedralIndices, const Eigen::Vector3d &translation) override;
	virtual void moveVertex(int tetrahedralIndex, const Eigen::Vector3d &translation) override;
	virtual void turnOnVertices() override;
	virtual void updateTransformationUniform(const MyGLCanvas *canvas) override;
	virtual void drawModel() const override { mSurfaceMesh->drawModel(); }
	virtual void drawExteriorSurface() const override { mSurfaceMesh->draw(); }
	virtual void drawInteriorSurface() const override {}
	virtual void drawTetrahedra() const override { mTetrahedralMesh->draw(); }
	virtual void drawTransparent() const override { mSurfaceMesh->drawTransparent(); }
	virtual bool getHasTexture() const override { return false; }
	virtual int getSurfaceIndex(int tetrahedralIndex) const override;
	virtual int getTetrahedralIndex(int surfaceIndex) const override;
	virtual void surfaceMeshCreateAttrib() override;
	virtual void unfixUnDragAllHandles() override { mSurfaceMesh->unfixUnDragAllHandles(); }

protected:
	std::vector<Eigen::Vector3d> mSurfaceVertices;
	std::vector<Eigen::Vector3i> mSurfaceTriangles;

	HandleSurfaceMesh *mSurfaceMesh;
	MeshSurfaceMesh *mTetrahedralMesh;
	std::unordered_map<int, int> mTetrahedral2Surface;
	std::unordered_map<int, int> mSurface2Tetrahedral;
};