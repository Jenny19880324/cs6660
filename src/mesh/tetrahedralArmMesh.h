#pragma once
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include "simulatableMesh.h"
#include "tet.h"
#include "mesh.h"
#include "muscle.h"

class TetrahedralArmMesh : public SimulatableMesh {
public:
	TetrahedralArmMesh() {}
	TetrahedralArmMesh(const SimulatableMesh *base);
	int getSurfaceIndex(int tetrahedralIndex) const;
	int getTetrahedralIndex(int surfaceIndex) const;

	virtual void loadMeshFromFile(const std::string &filename) override;
	virtual void saveConfigToFile(const std::string &filename) override;
	virtual void loadConfigFromFile(const std::string &filename) override;
	virtual void saveObjs() override;
	virtual int getNumTetrahedra() const override { return mNumTetrahedra; }
	virtual int getNumHandles() const override { return mSkinSurfaceMesh.getNumHandles(); }
	virtual int getNumVertices() const override { return mNumVertices; }
	virtual const Tet &getTetrahedra(int i) const override { return mTetrahedra[i]; }
	virtual const Eigen::Vector3d &getVertices(int i) const override { return mVertices[i]; }
	virtual const Handle *getHandles(int i) const override { return mSkinSurfaceMesh.getHandles(i); }
	virtual Handle *getHandles(int i) override { return mSkinSurfaceMesh.getHandles(i); }
	virtual void setVertices(const VectorX &x) override;
	virtual int highlightSelectedHandles(const Ray *r) override;
	virtual int fixSelectedHandles(const Ray *r) override { return mSkinSurfaceMesh.fixSelectedHandles(r); }
	virtual void fixSelectedHandles(const std::vector<int> &surfaceIndices) override { return mSkinSurfaceMesh.fixSelectedHandles(surfaceIndices); }
	virtual void highlightSelectedHandles(const std::vector<int> &surfaceIndices) override { return mSkinSurfaceMesh.highlightSelectedHandles(surfaceIndices); }
	virtual void moveVertices(const std::vector<int> &tetrahedralIndices, const Eigen::Vector3d &translation) override;
	virtual void moveVertex(int tetrahedralIndex, const Eigen::Vector3d &translation) override;
	virtual void turnOnVertices() override;
	virtual void updateTransformationUniform(const MyGLCanvas *canvas) override;
	virtual void drawModel() const override;
	virtual void drawExteriorSurface() const override;
	virtual void drawInteriorSurface() const override;
	virtual void drawTetrahedra() const override;
	virtual void drawTransparent() const override;
	virtual bool getHasTexture() const override { return true; }
	virtual void surfaceMeshCreateAttrib() override;
	virtual void unfixUnDragAllHandles() override { mSkinSurfaceMesh.unfixUnDragAllHandles(); }

	void drawFiberDirection();
	bool computeFiberDirection();
	void writeFiberDirection(const std::vector<Tet> &muscleTets);
	void loadFiberDirectionFromFile(const std::string &filename, std::vector<Tet> &muscleTets);
	void createTetrahedralMeshes();
	void updateTetrahedraTransformationUniform(const MyGLCanvas *canvas);
	void updateSurfaceTransformationUniform(const MyGLCanvas *canvas);
	void tetraheralMeshCreateAttrib();
	void writeTriCorrelationFile(const std::string &filename);
	void loadTriCorrelationFromFile(const std::string &filename);
	void createRegionBasedTetrahedraForFiberDirection(
		const std::unordered_set<int> &tetrahedralIndices,
		std::vector<Tet> &tets,
		std::vector<Eigen::Vector3d> &tetrahedralVertices,
		std::unordered_map<int, int> &whole2Region,
		std::unordered_map<int, int> &region2Whole);
	std::vector<Eigen::Vector3i> loadFacesFromFile(const std::string &filename);
	std::vector<Eigen::Vector3d> loadNodesFromFile(const std::string &filename);
	std::vector<Eigen::Vector3i> findTriCorelationFromFile(const std::string &nodefile, const std::string &facefile);
	int findNodeCorrelationFromFile(const std::string &nodefile, int indexOfSurfaceMesh);

private:
	int mNumUlnaVertices;
	int mNumRadiusVertices;
	int mNumHumerusVertices;
	int mNumBicepVertices;
	int mNumTricepVertices;
	int mNumSkinVertices;

	int mNumUlnaTets;
	int mNumRadiusTets;
	int mNumHumerusTets;
	int mNumBicepTets;
	int mNumTricepTets;
	int mNumSkinTets;

	int mNumUlnaSurfaceTris;
	int mNumRadiusSurfaceTris;
	int mNumHumerusSurfaceTris;
	int mNumBicepSurfaceTris;
	int mNumTricepSurfaceTris;
	int mNumSkinSurfaceTris;

	std::vector<Tet> mUlnaTets;     //the indices is of whole arm
	std::vector<Tet> mRadiusTets;   //the indices is of whole arm
	std::vector<Tet> mHumerusTets;  //the indices is of whole arm
	std::vector<Tet> mBicepTets;    //the indices is of whole arm
	std::vector<Tet> mTricepTets;   //the indices is of whole arm
	std::vector<Tet> mSkinTets;     //the indices is of whole arm

	MeshSurfaceMesh mUlnaSurfaceMesh;
	MeshSurfaceMesh mRadiusSurfaceMesh;
	MeshSurfaceMesh mHumerusSurfaceMesh;
	MeshSurfaceMesh mBicepSurfaceMesh;
	MeshSurfaceMesh mTricepSurfaceMesh;
	HandleSurfaceMesh mSkinSurfaceMesh;

	MeshSurfaceMesh mUlnaTetrahedralMesh;
	MeshSurfaceMesh mRadiusTetrahedralMesh;
	MeshSurfaceMesh mHumerusTetrahedralMesh;
	MeshSurfaceMesh mBicepTetrahedralMesh;
	MeshSurfaceMesh mTricepTetrahedralMesh;
	MeshSurfaceMesh mSkinTetrahedralMesh;

	std::vector<Eigen::Vector3d> mUlnaSurfaceVertices;
	std::vector<Eigen::Vector3d> mRadiusSurfaceVertices;
	std::vector<Eigen::Vector3d> mHumerusSurfaceVertices;
	std::vector<Eigen::Vector3d> mBicepSurfaceVertices;
	std::vector<Eigen::Vector3d> mTricepSurfaceVertices;
	std::vector<Eigen::Vector3d> mSkinSurfaceVertices;

	std::vector<Eigen::Vector3i> mUlnaSurfaceTriangles;
	std::vector<Eigen::Vector3i> mRadiusSurfaceTriangles;
	std::vector<Eigen::Vector3i> mHumerusSurfaceTriangles;
	std::vector<Eigen::Vector3i> mBicepSurfaceTriangles;
	std::vector<Eigen::Vector3i> mTricepSurfaceTriangles;
	std::vector<Eigen::Vector3i> mSkinSurfaceTriangles;

	std::vector<Eigen::Vector3d> mUlnaTetrahedralVertices;
	std::vector<Eigen::Vector3d> mRadiusTetrahedralVertices;
	std::vector<Eigen::Vector3d> mHumerusTetrahedralVertices;
	std::vector<Eigen::Vector3d> mBicepTetrahedralVertices;
	std::vector<Eigen::Vector3d> mTricepTetrahedralVertices;
	std::vector<Eigen::Vector3d> mSkinTetrahedralVertices;

	std::vector<Eigen::Vector3i> mUlnaTetrahedralTriangles;     //the indices is of whole arm
	std::vector<Eigen::Vector3i> mRadiusTetrahedralTriangles;   //the indices is of whole arm
	std::vector<Eigen::Vector3i> mHumerusTetrahedralTriangles;  //the indices is of whole arm
	std::vector<Eigen::Vector3i> mBicepTetrahedralTriangles;    //the indices is of whole arm
	std::vector<Eigen::Vector3i> mTricepTetrahedralTriangles;   //the indices is of whole arm
	std::vector<Eigen::Vector3i> mSkinTetrahedralTriangles;     //the indices is of whole arm

	std::unordered_set<int> mUlnaTetrahedralIndices;
	std::unordered_set<int> mRadiusTetrahedralIndices;
	std::unordered_set<int> mHumerusTetrahedralIndices;
	std::unordered_set<int> mBicepTetrahedralIndices;
	std::unordered_set<int> mTricepTetrahedralIndices;
	std::unordered_set<int> mSkinTetrahedralIndices;

	std::unordered_map<int, int> mUlnaTetrahedral2Surface;
	std::unordered_map<int, int> mUlnaSurface2Tetrahedral;
	std::unordered_map<int, int> mRadiusTetrahedral2Surface;
	std::unordered_map<int, int> mRadiusSurface2Tetrahedral;
	std::unordered_map<int, int> mHumerusTetrahedral2Surface;
	std::unordered_map<int, int> mHumerusSurface2Tetrahedral;
	std::unordered_map<int, int> mBicepTetrahedral2Surface;
	std::unordered_map<int, int> mBicepSurface2Tetrahedral;
	std::unordered_map<int, int> mTricepTetrahedral2Surface;
	std::unordered_map<int, int> mTricepSurface2Tetrahedral;
	std::unordered_map<int, int> mSkinTetrahedral2Surface;
	std::unordered_map<int, int> mSkinSurface2Tetrahedral;

	std::unordered_map<int, int> mBicepTetrahedral2Whole;
	std::unordered_map<int, int> mBicepWhole2Tetrahedral;
	std::unordered_map<int, int> mTricepTetrahedral2Whole;
	std::unordered_map<int, int> mTricepWhole2Tetrahedral;

	Muscle mBicep;
};
