#include <fstream>
#include <iostream>
#include "simulatableMesh.h"
#include "canvas.h"

SimulatableMesh::SimulatableMesh(const SimulatableMesh *other) {
	hasTexture = other->hasTexture;
	mNumVertices = other->mNumVertices;
	mNumTriangles = other->mNumTriangles;
	mNumTetrahedra = other->mNumTetrahedra;
	mVolumeRatio = other->mVolumeRatio;
	mVertices = other->mVertices;
	mTriangles = other->mTriangles;
	mTetrahedra = other->mTetrahedra;
	mPositions = other->mPositions;
	mIndices = other->mIndices;
}

void SimulatableMesh::loadMeshFromFile(const std::string &filename) {
	std::ifstream file;
	file.open(filename);

	if (!file.is_open()) {
		std::cout << "Could not open MESH file " << filename << std::endl;
		exit(1);
	}

	std::cout << "Loading mesh file " << filename << std::endl;

	std::string s;
	//find vertices section
	do { std::getline(file, s); } while (s.compare("Vertices"));
	file >> mNumVertices;
	for (int i = 0; i < mNumVertices; i++) {
		double x, y, z;
		int ref;
		file >> x >> y >> z >> ref;

		mVertices.push_back(Eigen::Vector3d(x, y, z));
#if !defined(BUILD_MUSCLE_ACTIVATION_LIB)
		gBoundingBox->addPoint(x, y, z);
#endif
	}

	//find triangle section
	do { std::getline(file, s); } while (s.compare("Triangles"));
	file >> mNumTriangles;
	int i = 0;
	for (int i = 0; i < mNumTriangles; i++) {
		int v1, v2, v3, ref;
		file >> v1 >> v2 >> v3 >> ref;
		//if (ref != 1) continue;
		mTriangles.push_back(Eigen::Vector3i(v1 - 1, v2 - 1, v3 - 1));
	}
	//find tetrahedral section
	do { std::getline(file, s); } while (s.compare("Tetrahedra"));
	file >> mNumTetrahedra;
	float minVolume = std::numeric_limits<float>::max();
	float maxVolume = std::numeric_limits<float>::min();
	for (int i = 0; i < mNumTetrahedra; i++) {
		int v1, v2, v3, v4, t;
		file >> v1 >> v2 >> v3 >> v4 >> t;
		TetType tetType = static_cast<TetType>(t);
		Tet tet(tetType, i, Eigen::Vector4i(v1 - 1, v2 - 1, v3 - 1, v4 - 1), mVertices);
		mTetrahedra.push_back(tet);
		float volume = tet.getVolume();
		if (volume < minVolume) {
			minVolume = volume;
		}
		if (volume > maxVolume) {
			maxVolume = volume;
		}
	}

	mVolumeRatio = maxVolume / minVolume;
	std::cout << "mVolumeRatio = " << mVolumeRatio << std::endl;
	
	file.close();
}

void SimulatableMesh::copyVerticesFrom(SimulatableMesh *mesh) {
	assert(mNumVertices == mesh->getNumVertices());
	for (int i = 0; i < mVertices.size(); i++) {
		mVertices[i] = mesh->getVertices(i);
	}
}





