#pragma once
#include <Eigen/Core>
#include <nanogui/glutil.h>
#include <memory>

struct Ray;
struct Intersection;
class MyGLCanvas;
class ObjSurfaceMesh;

class Handle{
public:
	Handle(const Eigen::Vector3d &center, int index);
	Handle(const Handle &);
	Handle(const Handle *);
	~Handle() { delete mSphere; }
	bool intersect(const Ray *r, Intersection &minTimeIntersection) const;
	void updateTransformationUniform(const MyGLCanvas *canvas);
	void translate(const Eigen::Vector3d &translation);
	void scale(float scale);
	void draw();
	int getTetrahedralIndex() const {return mtetrahedralIndex;}
	const Eigen::Vector3d getCenter() const { return mCenter; }
	void setDiffuseColor(const Eigen::Vector3f &color);
	void fix();
	void unfix();
	void drag();
	void undrag();
	bool isFixed() const;
	bool isDragged() const;
	static void setScale(float scale) { sScale = scale; }
	static float getScale() { return sScale; }

	class LoadSphereFromFile {
	public:
		LoadSphereFromFile();
		~LoadSphereFromFile() { /*delete Handle::sphere;*/ }
	};

private:
	bool mIsFixed;
	bool mIsDragged;
	Eigen::Vector3d mCenter;
	int mtetrahedralIndex;
	ObjSurfaceMesh *mSphere;
	static double sScale;
	static ObjSurfaceMesh *sSphere;
	static LoadSphereFromFile sLoader;
};