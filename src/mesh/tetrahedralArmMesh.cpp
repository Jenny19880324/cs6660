#define IGL_NO_MOSEK
#ifdef IGL_NO_MOSEK
#ifdef IGL_STATIC_LIBRARY
#undef IGL_STATIC_LIBRARY
#endif
#endif
#include <igl/boundary_conditions.h>
#include <igl/colon.h>
#include <igl/column_to_quats.h>
#include <igl/directed_edge_parents.h>
#include <igl/forward_kinematics.h>
#include <igl/jet.h>
#include <igl/lbs_matrix.h>
#include <igl/deform_skeleton.h>
#include <igl/normalize_row_sums.h>
#include <igl/readDMAT.h>
#include <igl/readMESH.h>
#include <igl/readTGF.h>
#include <igl/bbw.h>
//#include <igl/embree/bone_heat.h>

#include <Eigen/Geometry>
#include <Eigen/StdVector>
#include <vector>
#include <algorithm>
#include <iostream>

#include <fstream>
#include <unordered_map>
#include "tetrahedralArmMesh.h"
#include "simulation.h"
#include "canvas.h" //TODO remove this and move transparency to material


extern BoundingBox *gBoundingBox;
extern Simulation *gSimulation;
extern Handle *gHandle;

void saveMesh2File(const std::string &filename,
	const std::vector<Eigen::Vector3d> &tetrahedralVertices,
	const std::vector<Tet> &tets,
	const std::vector<Eigen::Vector3i> &surfaceTriangles,
	const std::unordered_map<int, int> &whole2Region,
	const std::unordered_map<int, int> &surface2Whole,
	int region) {
	std::ofstream file;
	file.open(filename);

	if (!file.is_open()) {
		std::cout << "Could not open MESH file " << filename << std::endl;
		exit(1);
	}

	file << "MeshVersionFormatted 1" << std::endl << std::endl;
	file << "Dimension" << std::endl << 3 << std::endl << std::endl << std::endl;

	file << "# Set of mesh vertices" << std::endl;
	file << "Vertices" << std::endl;
	file << tetrahedralVertices.size() << std::endl;
	for (int i = 0; i < tetrahedralVertices.size(); i++) {
		file << tetrahedralVertices[i].transpose() << "  0" << std::endl;
	}

	file << std::endl << "# Set of Triangles" << std::endl;
	file << "Triangles" << std::endl;
	file << surfaceTriangles.size() << std::endl;
	for (int i = 0; i < surfaceTriangles.size(); i++) {
		Eigen::Vector3i indices = surfaceTriangles[i];
		for (int j = 0; j < 3; j++) {
			int index = indices(j);
			auto it = surface2Whole.find(index);
			if (it == surface2Whole.end()) {
				std::cout << "Couldn't find corresponding whole index" << std::endl;
				exit(1);
			}
			index = surface2Whole.at(indices(j));
			it = whole2Region.find(index);
			if (it == whole2Region.end()) {
				std::cout << "Couldn't find corresponding whole index" << std::endl;
				exit(1);
			}
			int regionIndex = whole2Region.at(index);
			file << regionIndex + 1 << " ";
		}
		file << "1" << std::endl;
		//TODO add internal triangles
	}

	file << std::endl << "# Set of Tetrahedra" << std::endl;
	file << "Tetrahedra" << std::endl;
	file << tets.size() << std::endl;
	for (int i = 0; i < tets.size(); i++) {
		Eigen::Vector4i indices = tets[i].getRegionIndices();
		for (int j = 0; j < 4; j++) {
			file << indices(j) + 1 << " ";
		}
		file << region << std::endl;
	}

	file.close();
}

TetrahedralArmMesh::TetrahedralArmMesh(const SimulatableMesh *base):SimulatableMesh(base) {
	const TetrahedralArmMesh *other = dynamic_cast<const TetrahedralArmMesh *>(base);
	if (!other) {
		std::cout << "Couldn't convert to TetrahedralArmMesh" << std::endl;
		return;
	}
	mNumUlnaVertices = other->mNumUlnaVertices;
	mNumRadiusVertices = other->mNumRadiusVertices;
	mNumHumerusVertices = other->mNumRadiusVertices;
	mNumBicepVertices = other->mNumBicepVertices;
	mNumTricepVertices = other->mNumTricepVertices;
	mNumSkinVertices = other->mNumSkinVertices;

	mNumUlnaTets = other->mNumUlnaTets;
	mNumRadiusTets = other->mNumRadiusTets;
	mNumHumerusTets = other->mNumHumerusTets;
	mNumBicepTets = other->mNumBicepTets;
	mNumTricepTets = other->mNumTricepTets;
	mNumSkinTets = other->mNumSkinTets;

	mNumUlnaSurfaceTris = other->mNumUlnaSurfaceTris;
	mNumRadiusSurfaceTris = other->mNumRadiusSurfaceTris;
	mNumHumerusSurfaceTris = other->mNumHumerusSurfaceTris;
	mNumBicepSurfaceTris = other->mNumBicepSurfaceTris;
	mNumTricepSurfaceTris = other->mNumTricepSurfaceTris;
	mNumSkinSurfaceTris = other->mNumSkinSurfaceTris;

	mUlnaTets = other->mUlnaTets;
	mRadiusTets = other->mRadiusTets;
	mHumerusTets = other->mHumerusTets;
	mBicepTets = other->mBicepTets;
	mTricepTets = other->mTricepTets;
	mSkinTets = other->mSkinTets;

	mUlnaSurfaceMesh = other->mUlnaSurfaceMesh;
	mRadiusSurfaceMesh = other->mRadiusSurfaceMesh;
	mHumerusSurfaceMesh = other->mHumerusSurfaceMesh;
	mBicepSurfaceMesh = other->mBicepSurfaceMesh;
	mTricepSurfaceMesh = other->mTricepSurfaceMesh;
	mSkinSurfaceMesh = other->mSkinSurfaceMesh;

	mUlnaTetrahedralMesh = other->mUlnaTetrahedralMesh;
	mRadiusTetrahedralMesh = other->mRadiusTetrahedralMesh;
	mHumerusTetrahedralMesh = other->mHumerusTetrahedralMesh;
	mBicepTetrahedralMesh = other->mBicepTetrahedralMesh;
	mTricepTetrahedralMesh = other->mTricepTetrahedralMesh;
	mSkinTetrahedralMesh = other->mSkinTetrahedralMesh;

	mUlnaSurfaceVertices = other->mUlnaSurfaceVertices;
	mRadiusSurfaceVertices = other->mRadiusSurfaceVertices;
	mHumerusSurfaceVertices = other->mHumerusSurfaceVertices;
	mBicepSurfaceVertices = other->mBicepSurfaceVertices;
	mTricepSurfaceVertices = other->mTricepSurfaceVertices;
	mSkinSurfaceVertices = other->mSkinSurfaceVertices;

	mUlnaSurfaceTriangles = other->mUlnaSurfaceTriangles;
	mRadiusSurfaceTriangles = other->mRadiusSurfaceTriangles;
	mHumerusSurfaceTriangles = other->mHumerusSurfaceTriangles;
	mBicepSurfaceTriangles = other->mBicepSurfaceTriangles;
	mTricepSurfaceTriangles = other->mTricepSurfaceTriangles;
	mSkinSurfaceTriangles = other->mSkinSurfaceTriangles;

	mUlnaTetrahedralVertices = other->mUlnaTetrahedralVertices;
	mRadiusTetrahedralVertices = other->mRadiusTetrahedralVertices;
	mHumerusTetrahedralVertices = other->mHumerusTetrahedralVertices;
	mBicepTetrahedralVertices = other->mBicepTetrahedralVertices;
	mTricepTetrahedralVertices = other->mTricepTetrahedralVertices;
	mSkinTetrahedralVertices = other->mSkinTetrahedralVertices;

	mUlnaTetrahedralTriangles = other->mUlnaTetrahedralTriangles;
	mRadiusTetrahedralTriangles = other->mRadiusTetrahedralTriangles;
	mHumerusTetrahedralTriangles = other->mHumerusTetrahedralTriangles;
	mBicepTetrahedralTriangles = other->mBicepTetrahedralTriangles;
	mTricepTetrahedralTriangles = other->mTricepTetrahedralTriangles;
	mSkinTetrahedralTriangles = other->mSkinTetrahedralTriangles;

	mUlnaTetrahedralIndices = other->mUlnaTetrahedralIndices;
	mRadiusTetrahedralIndices = other->mRadiusTetrahedralIndices;
	mHumerusTetrahedralIndices = other->mHumerusTetrahedralIndices;
	mBicepTetrahedralIndices = other->mBicepTetrahedralIndices;
	mTricepTetrahedralIndices = other->mTricepTetrahedralIndices;
	mSkinTetrahedralIndices = other->mSkinTetrahedralIndices;

	mUlnaTetrahedral2Surface = other->mUlnaTetrahedral2Surface;
	mUlnaSurface2Tetrahedral = other->mUlnaSurface2Tetrahedral;
	mRadiusTetrahedral2Surface = other->mRadiusTetrahedral2Surface;
	mRadiusSurface2Tetrahedral = other->mRadiusSurface2Tetrahedral;
	mHumerusTetrahedral2Surface = other->mHumerusTetrahedral2Surface;
	mHumerusSurface2Tetrahedral = other->mHumerusSurface2Tetrahedral;
	mBicepTetrahedral2Surface = other->mBicepTetrahedral2Surface;
	mBicepSurface2Tetrahedral = other->mBicepSurface2Tetrahedral;
	mTricepTetrahedral2Surface = other->mTricepTetrahedral2Surface;
	mTricepSurface2Tetrahedral = other->mTricepSurface2Tetrahedral;
	mSkinTetrahedral2Surface = other->mSkinTetrahedral2Surface;
	mSkinSurface2Tetrahedral = other->mSkinSurface2Tetrahedral;

	mBicepTetrahedral2Whole = other->mBicepTetrahedral2Whole;
	mBicepWhole2Tetrahedral = other->mBicepWhole2Tetrahedral;
	mTricepTetrahedral2Whole = other->mTricepTetrahedral2Whole;
	mTricepWhole2Tetrahedral = other->mTricepWhole2Tetrahedral;

	mBicep = other->mBicep;

	mUlnaTetrahedralMesh.createAttrib(mVertices, mUlnaTetrahedralTriangles);
	mRadiusTetrahedralMesh.createAttrib(mVertices, mRadiusTetrahedralTriangles);
	mHumerusTetrahedralMesh.createAttrib(mVertices, mHumerusTetrahedralTriangles);
	mBicepTetrahedralMesh.createAttrib(mVertices, mBicepTetrahedralTriangles);
	mTricepTetrahedralMesh.createAttrib(mVertices, mTricepTetrahedralTriangles);
	mSkinTetrahedralMesh.createAttrib(mVertices, mSkinTetrahedralTriangles);
	mUlnaSurfaceMesh.createAttrib(mUlnaSurfaceVertices, mUlnaSurfaceTriangles);
	mRadiusSurfaceMesh.createAttrib(mRadiusSurfaceVertices, mRadiusSurfaceTriangles);
	mHumerusSurfaceMesh.createAttrib(mHumerusSurfaceVertices, mHumerusSurfaceTriangles);
	mBicepSurfaceMesh.createAttrib(mBicepSurfaceVertices, mBicepSurfaceTriangles);
	mTricepSurfaceMesh.createAttrib(mTricepSurfaceVertices, mTricepSurfaceTriangles);
	mSkinSurfaceMesh.createAttrib(mSkinSurfaceVertices, mSkinSurfaceTriangles);

	mSkinSurfaceMesh.setSimulatableMesh(dynamic_cast<SimulatableMesh *>(this));
}

void TetrahedralArmMesh::loadMeshFromFile(const std::string &filename) {
	SimulatableMesh::loadMeshFromFile(filename);
	//writeTriCorrelationFile("../model/tri_correlation.txt");
	std::size_t pathPos = filename.find_last_of("\\");
	std::size_t meshPos = filename.find(".mesh");
	std::string triCorrelationFilename = filename.substr(pathPos + 1, meshPos - pathPos - 1) + ".tricorrelation";
	loadTriCorrelationFromFile(triCorrelationFilename);
	createTetrahedralMeshes();

	//int p1 = findNodeCorrelationFromFile("../model/adjusted/mesh/bicep.1.node", 67);
	//std::cout << "p1 = " << p1 << std::endl;
	//gHandle = new Handle(mBicepTetrahedralVertices[p1], p1);
	//computeFiberDirection();
	//loadFiberDirectionFromFile("../model/bicep_fiber_direction.txt", mBicepTets);
	//mBicep.setMuscleTet(mBicepTets);
	mSkinSurfaceMesh.setSimulatableMesh(dynamic_cast<SimulatableMesh *>(this));

}


void TetrahedralArmMesh::createTetrahedralMeshes() {
	for (int i = 0; i < mTetrahedra.size(); i++) {
		switch (mTetrahedra[i].getType()) {
		case ULNA: {
			mUlnaTets.push_back(mTetrahedra[i]);
			for (int j = 0; j < 4; j++) {
				mUlnaTetrahedralTriangles.push_back(mTetrahedra[i].getTriangles(j));
				mUlnaTetrahedralIndices.insert(mTetrahedra[i].getIndices(j));
			}
			break;
		}
		case RADIUS: {
			mRadiusTets.push_back(mTetrahedra[i]);
			for (int j = 0; j < 4; j++) {
				mRadiusTetrahedralTriangles.push_back(mTetrahedra[i].getTriangles(j));
				mRadiusTetrahedralIndices.insert(mTetrahedra[i].getIndices(j));
			}
			break;
		}
		case HUMERUS: {
			mHumerusTets.push_back(mTetrahedra[i]);
			for (int j = 0; j < 4; j++) {
				mHumerusTetrahedralTriangles.push_back(mTetrahedra[i].getTriangles(j));
				mHumerusTetrahedralIndices.insert(mTetrahedra[i].getIndices(j));
			}
			break;
		}
		case BICEP: {
			mBicepTets.push_back(Tet(mTetrahedra[i]));
			for (int j = 0; j < 4; j++) {
				mBicepTetrahedralTriangles.push_back(mTetrahedra[i].getTriangles(j));
				mBicepTetrahedralIndices.insert(mTetrahedra[i].getIndices(j));
			}
			break;
		}
		case TRICEP: {
			mTricepTets.push_back(Tet(mTetrahedra[i]));
			for (int j = 0; j < 4; j++) {
				mTricepTetrahedralTriangles.push_back(mTetrahedra[i].getTriangles(j));
				mTricepTetrahedralIndices.insert(mTetrahedra[i].getIndices(j));
			}
			break;
		}
		case SKIN: {
			mSkinTets.push_back(mTetrahedra[i]);
			for (int j = 0; j < 4; j++) {
				mSkinTetrahedralTriangles.push_back(mTetrahedra[i].getTriangles(j));
				mSkinTetrahedralIndices.insert(mTetrahedra[i].getIndices(j));
			}
			break;
		}
		}
	}

	createRegionBasedTetrahedraForFiberDirection(mBicepTetrahedralIndices,   mBicepTets,   mBicepTetrahedralVertices, mBicepWhole2Tetrahedral, mBicepTetrahedral2Whole);
	createRegionBasedTetrahedraForFiberDirection(mTricepTetrahedralIndices,  mTricepTets,  mTricepTetrahedralVertices, mTricepWhole2Tetrahedral, mTricepTetrahedral2Whole);

	mUlnaTetrahedralMesh.createBlinnPhongMaterialFromFile("bone.mtl");
	mUlnaTetrahedralMesh.createAttrib(mVertices, mUlnaTetrahedralTriangles);
	mRadiusTetrahedralMesh.createBlinnPhongMaterialFromFile("bone.mtl");
	mRadiusTetrahedralMesh.createAttrib(mVertices, mRadiusTetrahedralTriangles);
	mHumerusTetrahedralMesh.createBlinnPhongMaterialFromFile("bone.mtl");
	mHumerusTetrahedralMesh.createAttrib(mVertices, mHumerusTetrahedralTriangles);
	mBicepTetrahedralMesh.createBlinnPhongMaterialFromFile("muscle.mtl");
	mBicepTetrahedralMesh.createAttrib(mVertices, mBicepTetrahedralTriangles);
	mTricepTetrahedralMesh.createBlinnPhongMaterialFromFile("muscle.mtl");
	mTricepTetrahedralMesh.createAttrib(mVertices, mTricepTetrahedralTriangles);
	mSkinTetrahedralMesh.createBlinnPhongMaterialFromFile("skin.mtl");
	mSkinTetrahedralMesh.createAttrib(mVertices, mSkinTetrahedralTriangles);

	//saveMesh2File("../model/bicep.mesh", mBicepTetrahedralVertices, mBicepTets, mBicepSurfaceTriangles, mBicepWhole2Tetrahedral, mBicepSurface2Tetrahedral, BICEP);
}

void TetrahedralArmMesh::createRegionBasedTetrahedraForFiberDirection(
	const std::unordered_set<int> &tetrahedralIndices, 
	std::vector<Tet> &tets,
	std::vector<Eigen::Vector3d> &tetrahedralVertices,
	std::unordered_map<int, int> &whole2Region,
	std::unordered_map<int, int> &region2Whole) {
	for (auto it : tetrahedralIndices) {
		whole2Region.insert({ it, (int)tetrahedralVertices.size() });
		region2Whole.insert({ (int)tetrahedralVertices.size(), it });
		tetrahedralVertices.push_back(mVertices[it]);
	}
	for (int i = 0; i < tets.size(); i++) {
		Eigen::Vector4i wholeIndices = tets[i].getIndices();
		Eigen::Vector4i regionIndices;
		for (int j = 0; j < 4; j++) {
			regionIndices[j] = whole2Region[wholeIndices[j]];
		}
		tets[i].setRegionIndices(regionIndices);
	}
}

int TetrahedralArmMesh::findNodeCorrelationFromFile(const std::string &nodefile, int indexOfSurfaceMesh) {
	std::vector<Eigen::Vector3d> nodes = loadNodesFromFile(nodefile);
	Eigen::Vector3d vertex = nodes[indexOfSurfaceMesh];
	for (int k = 0; k < mBicepTetrahedralVertices.size(); k++) {
		if (mBicepTetrahedralVertices[k].isApprox(vertex, 1e-3)) {
			return k;
		}
	}
	return -1;
}

std::vector<Eigen::Vector3i> TetrahedralArmMesh::findTriCorelationFromFile(const std::string &nodefile, const std::string &facefile) {
	std::vector<Eigen::Vector3d> nodes = loadNodesFromFile(nodefile);
	std::vector<Eigen::Vector3i> faces = loadFacesFromFile(facefile);
	std::vector<Eigen::Vector3i> triangles;
	for (int i = 0; i < faces.size(); i++) {
		Eigen::Vector3i triangle(-1, -1, -1);
		for (int j = 0; j < 3; j++) {
			int index = faces[i](j);
			index--;
			Eigen::Vector3d vertex = nodes[index];
			for (int k = 0; k < mNumVertices; k++) {
				if (mVertices[k].isApprox(vertex, 1e-3)) {
					triangle[j] = k;
					break;
				}
			}
		}
		assert(triangle[0] != -1);
		assert(triangle[1] != -1);
		assert(triangle[2] != -1);
		if((triangle[0] != -1) && (triangle[1] != -1) && (triangle[2] != -1)){
			triangles.push_back(triangle);
		}
	}
	return triangles;
}

void TetrahedralArmMesh::writeTriCorrelationFile(const std::string &filename) {
	std::ofstream file;
	file.open(filename);

	if (!file.is_open()) {
		std::cout << "Could not open CORRELATION file " << filename << std::endl;
		exit(1);
	}
	
	///////////////////////////////////////////////////////////////////////////
	//Ulna section
	///////////////////////////////////////////////////////////////////////////
	mUlnaSurfaceTriangles = findTriCorelationFromFile("../model/adjusted2/mesh/ulna.1.node", "../model/adjusted2/mesh/ulna.1.face");
	file << "# Triangles in Ulna" << std::endl;
	file << "Ulna " << mUlnaSurfaceTriangles.size() << std::endl;
	for (int i = 0; i < mUlnaSurfaceTriangles.size(); i++) {
		file << mUlnaSurfaceTriangles[i].transpose() << std::endl;
	}

	///////////////////////////////////////////////////////////////////////////
	//Radius section
	///////////////////////////////////////////////////////////////////////////
	mRadiusSurfaceTriangles = findTriCorelationFromFile("../model/adjusted2/mesh/radius.1.node", "../model/adjusted2/mesh/radius.1.face");
	file << std::endl <<  "# Triangles in Radius" << std::endl;
	file << "Radius " << mRadiusSurfaceTriangles.size() << std::endl;
	for (int i = 0; i < mRadiusSurfaceTriangles.size(); i++) {
		file << mRadiusSurfaceTriangles[i].transpose() << std::endl;
	}

	///////////////////////////////////////////////////////////////////////////
	//Humerus section
	///////////////////////////////////////////////////////////////////////////
	mHumerusSurfaceTriangles = findTriCorelationFromFile("../model/adjusted2/mesh/humerus.1.node", "../model/adjusted2/mesh/humerus.1.face");
	file << std::endl << "# Triangles in Humerus" << std::endl;
	file << "Humerus " << mHumerusSurfaceTriangles.size() << std::endl;
	for (int i = 0; i < mHumerusSurfaceTriangles.size(); i++) {
		file << mHumerusSurfaceTriangles[i].transpose() << std::endl;
	}

	///////////////////////////////////////////////////////////////////////////
	//Bicep section
	///////////////////////////////////////////////////////////////////////////
	mBicepSurfaceTriangles = findTriCorelationFromFile("../model/adjusted2/mesh/bicep.1.node", "../model/adjusted2/mesh/bicep.1.face");
	file << std::endl << "# Triangles in Bicep" << std::endl;
	file << "Bicep " << mBicepSurfaceTriangles.size() << std::endl;
	for (int i = 0; i < mBicepSurfaceTriangles.size(); i++) {
		file << mBicepSurfaceTriangles[i].transpose() << std::endl;
	}

	///////////////////////////////////////////////////////////////////////////
	//Tricep section
	///////////////////////////////////////////////////////////////////////////
	mTricepSurfaceTriangles = findTriCorelationFromFile("../model/adjusted2/mesh/tricep.1.node", "../model/adjusted2/mesh/tricep.1.face");
	file << std::endl << "# Triangles in Tricep" << std::endl;
	file << "Tricep " << mTricepSurfaceTriangles.size() << std::endl;
	for (int i = 0; i < mTricepSurfaceTriangles.size(); i++) {
		file << mTricepSurfaceTriangles[i].transpose() << std::endl;
	}

	///////////////////////////////////////////////////////////////////////////
	//Skin section
	///////////////////////////////////////////////////////////////////////////
	mSkinSurfaceTriangles = findTriCorelationFromFile("../model/adjusted2/mesh/skin.1.node", "../model/adjusted2/mesh/skin.1.face");
	file << std::endl << "# Triangles in Skin" << std::endl;
	file << "Skin " << mSkinSurfaceTriangles.size() << std::endl;
	for (int i = 0; i < mSkinSurfaceTriangles.size(); i++) {
		file << mSkinSurfaceTriangles[i].transpose() << std::endl;
	}
	file.close();
}

std::vector<Eigen::Vector3d> TetrahedralArmMesh::loadNodesFromFile(const std::string &filename) {
	std::ifstream file;
	file.open(filename);

	if (!file.is_open()) {
		std::cout << "Could not open NODE file " << filename << std::endl;
		exit(1);
	}

	std::cout << "Loading NODE file " << filename << std::endl;

	int numNodes, dimension, attribute, boundaryMarker;
	file >> numNodes >> dimension >> attribute >> boundaryMarker;
	assert(dimension == 3);
	assert(attribute == 0);
	assert(boundaryMarker == 0);

	std::vector<Eigen::Vector3d> nodes;
	for (int i = 0; i < numNodes; i++) {
		int index;
		double x, y, z;
		file >> index >> x >> y >> z;
		nodes.push_back(Eigen::Vector3d(x, y, z));
	}
	file.close();
	return nodes;
}

std::vector<Eigen::Vector3i> TetrahedralArmMesh::loadFacesFromFile(const std::string &filename) {
	std::ifstream file;
	file.open(filename);

	if (!file.is_open()) {
		std::cout << "Could not open FACE file " << filename << std::endl;
		exit(1);
	}

	std::cout << "Loading FACE file " << filename << std::endl;

	int numFaces, boundaryMarker;
	file >> numFaces >> boundaryMarker;
	assert(boundaryMarker == 1);

	std::vector<Eigen::Vector3i> surfaceTriangles;
	for (int i = 0; i < numFaces; i++) {
		int index, v1, v2, v3, b;
		file >> index >> v1 >> v2 >> v3 >> b;
		surfaceTriangles.push_back(Eigen::Vector3i(v1, v2, v3));
	}
	file.close();
	return surfaceTriangles;
}

void TetrahedralArmMesh::loadTriCorrelationFromFile(const std::string &filename) {
	std::ifstream file;
	file.open(filename);
	if (!file.is_open()) {
		std::cout << "Could not open TRICORRELATION file" << filename << std::endl;
		exit(1);
	}
	std::cout << "Loading TRICORRELATION file " << filename << std::endl;

	std::string s;
	//find Ulna section
	do {
		std::getline(file, s);
		if (!s.substr(0, 14).compare("# Triangles in")) {
			if (!s.substr(15, 18).compare("Ulna")) {
				file >> s; assert(s.compare("Ulna") == 0);
				file >> mNumUlnaSurfaceTris;
				for (int i = 0; i < mNumUlnaSurfaceTris; i++) {
					int v[3];
					file >> v[0] >> v[1] >> v[2];

					Eigen::Vector3i idx;
					for (int j = 0; j < 3; j++) {
						bool ok = mUlnaTetrahedral2Surface.insert({ v[j], (int)mUlnaSurfaceVertices.size() }).second;
						if (ok) {
							mUlnaSurface2Tetrahedral.insert({ (int)mUlnaSurfaceVertices.size(), v[j] });
							idx(j) = (int)mUlnaSurfaceVertices.size();
							mUlnaSurfaceVertices.push_back(mVertices[v[j]]);
						}
						else {
							idx(j) = mUlnaTetrahedral2Surface[v[j]];
						}
					}
					mUlnaSurfaceTriangles.push_back(idx);
				}
				mUlnaSurfaceMesh.createBlinnPhongMaterialFromFile("bone.mtl");
				mUlnaSurfaceMesh.setNumVertices(mUlnaSurfaceVertices.size());
				mUlnaSurfaceMesh.setNumFaces(mUlnaSurfaceTriangles.size());
				mUlnaSurfaceMesh.setSurface2Tetrahedral(mUlnaSurface2Tetrahedral);
				mUlnaSurfaceMesh.createAttrib(mUlnaSurfaceVertices, mUlnaSurfaceTriangles);
			}
			else if (!s.substr(15, 20).compare("Radius")) {
				file >> s; assert(s.compare("Radius") == 0);
				file >> mNumRadiusSurfaceTris;
				for (int i = 0; i < mNumRadiusSurfaceTris; i++) {
					int v[3];
					file >> v[0] >> v[1] >> v[2];

					Eigen::Vector3i idx;
					for (int j = 0; j < 3; j++) {
						bool ok = mRadiusTetrahedral2Surface.insert({ v[j], (int)mRadiusSurfaceVertices.size() }).second;
						if (ok) {
							mRadiusSurface2Tetrahedral.insert({ (int)mRadiusSurfaceVertices.size(), v[j] });
							idx(j) = (int)mRadiusSurfaceVertices.size();
							mRadiusSurfaceVertices.push_back(mVertices[v[j]]);
						}
						else {
							idx(j) = mRadiusTetrahedral2Surface[v[j]];
						}
					}
					mRadiusSurfaceTriangles.push_back(idx);
				}
				mRadiusSurfaceMesh.createBlinnPhongMaterialFromFile("bone.mtl");
				mRadiusSurfaceMesh.setNumVertices(mRadiusSurfaceVertices.size());
				mRadiusSurfaceMesh.setNumFaces(mRadiusSurfaceTriangles.size());
				mRadiusSurfaceMesh.setSurface2Tetrahedral(mRadiusSurface2Tetrahedral);
				mRadiusSurfaceMesh.createAttrib(mRadiusSurfaceVertices, mRadiusSurfaceTriangles);
			}
			else if (!s.substr(15, 21).compare("Humerus")) {
				file >> s; assert(s.compare("Humerus") == 0);
				file >> mNumHumerusSurfaceTris;
				for (int i = 0; i < mNumHumerusSurfaceTris; i++) {
					int v[3];
					file >> v[0] >> v[1] >> v[2];

					Eigen::Vector3i idx;
					for (int j = 0; j < 3; j++) {
						bool ok = mHumerusTetrahedral2Surface.insert({ v[j], (int)mHumerusSurfaceVertices.size() }).second;
						if (ok) {
							mHumerusSurface2Tetrahedral.insert({ (int)mHumerusSurfaceVertices.size(), v[j] });
							idx(j) = (int)mHumerusSurfaceVertices.size();
							mHumerusSurfaceVertices.push_back(mVertices[v[j]]);
						}
						else {
							idx(j) = mHumerusTetrahedral2Surface[v[j]];
						}
					}
					mHumerusSurfaceTriangles.push_back(idx);
				}
				mHumerusSurfaceMesh.createBlinnPhongMaterialFromFile("bone.mtl");
				mHumerusSurfaceMesh.setNumVertices(mHumerusSurfaceVertices.size());
				mHumerusSurfaceMesh.setNumFaces(mHumerusSurfaceTriangles.size());
				mHumerusSurfaceMesh.setSurface2Tetrahedral(mHumerusSurface2Tetrahedral);
				mHumerusSurfaceMesh.createAttrib(mHumerusSurfaceVertices, mHumerusSurfaceTriangles);
			}
			else if (!s.substr(15, 19).compare("Bicep")) {
				file >> s; assert(s.compare("Bicep") == 0);
				file >> mNumBicepSurfaceTris;

				for (int i = 0; i < mNumBicepSurfaceTris; i++) {
					int v[3];
					file >> v[0] >> v[1] >> v[2];
					Eigen::Vector3i idx;
					for (int j = 0; j < 3; j++) {
						bool ok = mBicepTetrahedral2Surface.insert({ v[j], (int)mBicepSurfaceVertices.size() }).second;
						if (ok) {
							mBicepSurface2Tetrahedral.insert({ (int)mBicepSurfaceVertices.size(), v[j] });
							idx(j) = (int)mBicepSurfaceVertices.size();
							mBicepSurfaceVertices.push_back(mVertices[v[j]]);
						}
						else {
							idx(j) = mBicepTetrahedral2Surface[v[j]];
						}
					}
					mBicepSurfaceTriangles.push_back(idx);
				}
				mBicepSurfaceMesh.createBlinnPhongMaterialFromFile("muscle.mtl");
				mBicepSurfaceMesh.setNumVertices(mBicepSurfaceVertices.size());
				mBicepSurfaceMesh.setNumFaces(mBicepSurfaceTriangles.size());
				mBicepSurfaceMesh.setSurface2Tetrahedral(mBicepSurface2Tetrahedral);
				mBicepSurfaceMesh.createAttrib(mBicepSurfaceVertices, mBicepSurfaceTriangles);
			}
			else if (!s.substr(15, 20).compare("Tricep")) {
				file >> s; assert(s.compare("Tricep") == 0);
				file >> mNumTricepSurfaceTris;
				for (int i = 0; i < mNumTricepSurfaceTris; i++) {
					int v[3];
					file >> v[0] >> v[1] >> v[2];
					Eigen::Vector3i idx;
					for (int j = 0; j < 3; j++) {
						bool ok = mTricepTetrahedral2Surface.insert({ v[j], (int)mTricepSurfaceVertices.size() }).second;
						if (ok) {
							mTricepSurface2Tetrahedral.insert({ (int)mTricepSurfaceVertices.size(), v[j] });
							idx(j) = (int)mTricepSurfaceVertices.size();
							mTricepSurfaceVertices.push_back(mVertices[v[j]]);
						}
						else {
							idx(j) = mTricepTetrahedral2Surface[v[j]];
						}
					}
					mTricepSurfaceTriangles.push_back(idx);
				}
				mTricepSurfaceMesh.createBlinnPhongMaterialFromFile("muscle.mtl");
				mTricepSurfaceMesh.setNumVertices(mTricepSurfaceVertices.size());
				mTricepSurfaceMesh.setNumFaces(mTricepSurfaceTriangles.size());
				mTricepSurfaceMesh.setSurface2Tetrahedral(mTricepSurface2Tetrahedral);
				mTricepSurfaceMesh.createAttrib(mTricepSurfaceVertices, mTricepSurfaceTriangles);
			}
			else if (!s.substr(15, 18).compare("Skin")) {
				file >> s; assert(s.compare("Skin") == 0);
				file >> mNumSkinSurfaceTris;
				for (int i = 0; i < mNumSkinSurfaceTris; i++) {
					int v[3];
					file >> v[0] >> v[1] >> v[2];
					Eigen::Vector3i idx;
					for (int j = 0; j < 3; j++) {
						//v[j]--;
						bool ok = mSkinTetrahedral2Surface.insert({ v[j], (int)mSkinSurfaceVertices.size() }).second;
						if (ok) {
							mSkinSurface2Tetrahedral.insert({ (int)mSkinSurfaceVertices.size(), v[j] });
							idx(j) = (int)mSkinSurfaceVertices.size();
							mSkinSurfaceVertices.push_back(mVertices[v[j]]);
						}
						else {
							idx(j) = mSkinTetrahedral2Surface[v[j]];
						}
					}
					mSkinSurfaceTriangles.push_back(idx);
				}

				for (int i = 0; i < mSkinSurfaceVertices.size(); i++) {
					mSkinSurfaceMesh.addPoint(mSkinSurfaceVertices[i].x(), mSkinSurfaceVertices[i].y(), mSkinSurfaceVertices[i].z());
				}
				gBoundingBox->add(mSkinSurfaceMesh.getBoundingBox());
				Handle::setScale(std::min(gBoundingBox->getXLength(), gBoundingBox->getYLength()) * 0.02);
				mSkinSurfaceMesh.createTransparentMaterial();
				mSkinSurfaceMesh.setNumVertices(mSkinSurfaceVertices.size());
				mSkinSurfaceMesh.setNumFaces(mSkinSurfaceTriangles.size());
				mSkinSurfaceMesh.setSurface2Tetrahedral(mSkinSurface2Tetrahedral);
				mSkinSurfaceMesh.createAttrib(mSkinSurfaceVertices, mSkinSurfaceTriangles);
			}
		}
	} while (file);

}

void TetrahedralArmMesh::updateTetrahedraTransformationUniform(const MyGLCanvas *canvas) {
	mUlnaTetrahedralMesh.updateTransformationUniform(canvas);
	mRadiusTetrahedralMesh.updateTransformationUniform(canvas);
	mHumerusTetrahedralMesh.updateTransformationUniform(canvas);
	mHumerusTetrahedralMesh.updateTransformationUniform(canvas);
	mBicepTetrahedralMesh.updateTransformationUniform(canvas);
	mTricepTetrahedralMesh.updateTransformationUniform(canvas);
	mSkinTetrahedralMesh.updateTransformationUniform(canvas);
}

void TetrahedralArmMesh::updateSurfaceTransformationUniform(const MyGLCanvas *canvas) {
	if (gHandle) {
		gHandle->updateTransformationUniform(canvas);
	}
	mUlnaSurfaceMesh.updateTransformationUniform(canvas);
	mRadiusSurfaceMesh.updateTransformationUniform(canvas);
	mHumerusSurfaceMesh.updateTransformationUniform(canvas);
	mBicepSurfaceMesh.updateTransformationUniform(canvas);
	mTricepSurfaceMesh.updateTransformationUniform(canvas);
	mSkinSurfaceMesh.updateTransformationUniform(canvas);
}

void TetrahedralArmMesh::updateTransformationUniform(const MyGLCanvas *canvas) { 
	updateSurfaceTransformationUniform(canvas);
	updateTetrahedraTransformationUniform(canvas);
	mBicep.updateTransformationUniform(canvas); //for drawing fiber direction

}

void TetrahedralArmMesh::drawInteriorSurface() const {
	mUlnaSurfaceMesh.draw();
	mRadiusSurfaceMesh.draw();
	mHumerusSurfaceMesh.draw();
	mBicepSurfaceMesh.draw();
	mTricepSurfaceMesh.draw();
}

void TetrahedralArmMesh::drawExteriorSurface() const {
	mSkinSurfaceMesh.draw();
}

void TetrahedralArmMesh::drawTetrahedra() const {
	mUlnaTetrahedralMesh.draw();
	mRadiusTetrahedralMesh.draw();
	mHumerusTetrahedralMesh.draw();
	mBicepTetrahedralMesh.draw();
	mTricepTetrahedralMesh.draw();
}

void TetrahedralArmMesh::drawModel() const {
	mUlnaSurfaceMesh.drawModel();
	mRadiusSurfaceMesh.drawModel();
	mHumerusSurfaceMesh.drawModel();
	mBicepSurfaceMesh.drawModel();
	mTricepSurfaceMesh.drawModel();
}

void TetrahedralArmMesh::drawTransparent() const {
	mSkinSurfaceMesh.drawTransparent();
}

void TetrahedralArmMesh::drawFiberDirection() {
	mBicep.drawFiberDirection();
}

void TetrahedralArmMesh::moveVertex(int tetrahedralIndex, const Eigen::Vector3d &translation) {
	int surfaceIndex = mSkinTetrahedral2Surface[tetrahedralIndex];
	mVertices[tetrahedralIndex] += translation;
	mSkinSurfaceVertices[surfaceIndex] += translation;
	mSkinSurfaceMesh.createAttrib(mSkinSurfaceVertices, mSkinSurfaceTriangles);
}

void TetrahedralArmMesh::moveVertices(const std::vector<int> &tetrahedralIndices, const Eigen::Vector3d &translation) {
	for (int tetrahedralIndex : tetrahedralIndices) {
		int surfaceIndex = mSkinTetrahedral2Surface[tetrahedralIndex];
		mVertices[tetrahedralIndex] += translation;
		mSkinSurfaceVertices[surfaceIndex] += translation;
	}
	mSkinSurfaceMesh.createAttrib(mSkinSurfaceVertices, mSkinSurfaceTriangles);
}

void TetrahedralArmMesh::turnOnVertices() {
	mSkinSurfaceMesh.turnOnVertices();
}

void TetrahedralArmMesh::setVertices(const VectorX &x) {
	for (int i = 0; i < x.rows() / 3; i++) {
		mVertices[i](0) = x(i * 3 + 0);
		mVertices[i](1) = x(i * 3 + 1);
		mVertices[i](2) = x(i * 3 + 2);
	}
}


void TetrahedralArmMesh::surfaceMeshCreateAttrib() {
	for (int i = 0; i < mUlnaSurfaceVertices.size(); i++) {
		mUlnaSurfaceVertices[i] = mVertices[mUlnaSurface2Tetrahedral[i]];
	}
	for (int i = 0; i < mRadiusSurfaceVertices.size(); i++) {
		mRadiusSurfaceVertices[i] = mVertices[mRadiusSurface2Tetrahedral[i]];
	}
	for (int i = 0; i < mHumerusSurfaceVertices.size(); i++) {
		mHumerusSurfaceVertices[i] = mVertices[mHumerusSurface2Tetrahedral[i]];
	}
	for (int i = 0; i < mBicepSurfaceVertices.size(); i++) {
		mBicepSurfaceVertices[i] = mVertices[mBicepSurface2Tetrahedral[i]];
	}
	for (int i = 0; i < mTricepSurfaceVertices.size(); i++) {
		mTricepSurfaceVertices[i] = mVertices[mTricepSurface2Tetrahedral[i]];
	}
	for (int i = 0; i < mSkinSurfaceVertices.size(); i++) {
		mSkinSurfaceVertices[i] = mVertices[mSkinSurface2Tetrahedral[i]];
	}
	mUlnaSurfaceMesh.createAttrib(mUlnaSurfaceVertices, mUlnaSurfaceTriangles);
	mRadiusSurfaceMesh.createAttrib(mRadiusSurfaceVertices, mRadiusSurfaceTriangles);
	mHumerusSurfaceMesh.createAttrib(mHumerusSurfaceVertices, mHumerusSurfaceTriangles);
	mBicepSurfaceMesh.createAttrib(mBicepSurfaceVertices, mBicepSurfaceTriangles);
	mTricepSurfaceMesh.createAttrib(mTricepSurfaceVertices, mTricepSurfaceTriangles);
	mSkinSurfaceMesh.createAttrib(mSkinSurfaceVertices, mSkinSurfaceTriangles);
}

void TetrahedralArmMesh::tetraheralMeshCreateAttrib() {
	mUlnaTetrahedralMesh.createAttrib(mVertices, mUlnaTetrahedralTriangles);
	mRadiusTetrahedralMesh.createAttrib(mVertices, mRadiusTetrahedralTriangles);
	mHumerusTetrahedralMesh.createAttrib(mVertices, mHumerusTetrahedralTriangles);
	mBicepTetrahedralMesh.createAttrib(mVertices, mBicepTetrahedralTriangles);
	mTricepTetrahedralMesh.createAttrib(mVertices, mTricepTetrahedralTriangles);
	mSkinTetrahedralMesh.createAttrib(mVertices, mSkinTetrahedralTriangles);
}


int TetrahedralArmMesh::getSurfaceIndex(int tetrahedralIndex) const {
	std::unordered_map<int, int>::const_iterator got = mSkinTetrahedral2Surface.find(tetrahedralIndex);
	if (got == mSkinTetrahedral2Surface.end()) {
		return -1;
	}
	else {
		return got->second;
	}
}

int TetrahedralArmMesh::getTetrahedralIndex(int surfaceIndex) const {
	std::unordered_map<int, int>::const_iterator got = mSkinSurface2Tetrahedral.find(surfaceIndex);
	if (got == mSkinSurface2Tetrahedral.end()) {
		return -1;
	}
	else {
		return got->second;
	}
}

int TetrahedralArmMesh::highlightSelectedHandles(const Ray *r) { 
	int surfaceIndex = mSkinSurfaceMesh.highlightSelectedHandles(r);
	return getTetrahedralIndex(surfaceIndex);
}

void TetrahedralArmMesh::saveConfigToFile(const std::string &filename) {
	std::ofstream configFile(filename);
	if (configFile.is_open())
	{
		///////////////////////////////////////////////////////////////////////
		//fixed handles section
		///////////////////////////////////////////////////////////////////////
		configFile << "# Fixed Handles\n";
		std::vector<int> fixedHandles;
		for (int i = 0; i < mSkinSurfaceMesh.getNumHandles(); i++) {
			if (mSkinSurfaceMesh.getHandles(i)->isFixed()) {
				fixedHandles.push_back(i);
			}
		}
		configFile << "Handles " << fixedHandles.size() << std::endl;
		for (int i = 0; i < fixedHandles.size(); i++) {
			if (i == fixedHandles.size() - 1) {
				configFile << fixedHandles[i] << std::endl;
			}
			else {
				configFile << fixedHandles[i] << " ";
			}
		}
		/////////////////////////////////////////////////////////////////////////////
		//dragged handles section
		/////////////////////////////////////////////////////////////////////////////
		configFile << "# Dragged Handles\n";
		std::vector<int> draggedHandles;
		for (int i = 0; i < mSkinSurfaceMesh.getNumHandles(); i++) {
			if (mSkinSurfaceMesh.getHandles(i)->isDragged()) {
				draggedHandles.push_back(i);
			}
		}
		configFile << "Handles " << draggedHandles.size() << std::endl;
		for (int i = 0; i < draggedHandles.size(); i++) {
			configFile << draggedHandles[i] <<  " " << mVertices[mSkinSurface2Tetrahedral[draggedHandles[i]]].transpose() << std::endl;
		}
		configFile.close();
	}
	else {
		std::cout << "Unable to open file";
	}
}

void TetrahedralArmMesh::loadConfigFromFile(const std::string &filename) {
	std::ifstream file;
	file.open(filename);

	if (!file.is_open()) {
		std::cout << "Could not open CONFIG file " << filename << std::endl;
		exit(1);
	}

	std::cout << "Loading config file " << filename << std::endl;

	std::string s;
	///////////////////////////////////////////////////////////////////////////
	//find fixed handles section
	///////////////////////////////////////////////////////////////////////////
	do { std::getline(file, s); } while (s.substr(0, 15).compare("# Fixed Handles"));
	file >> s; assert(s.compare("Handles") == 0);
	int numFixedHandles;
	std::vector<int> fixedHandles;
	file >> numFixedHandles;
	for (int i = 0; i < numFixedHandles; i++) {
		int fixedSurfaceIndex;
		file >> fixedSurfaceIndex;
		int fixedTetrahedralIndex = mSkinSurface2Tetrahedral[fixedSurfaceIndex];
		mSkinSurfaceMesh.getHandles(fixedSurfaceIndex)->fix();
		mSkinSurfaceMesh.getHandles(fixedSurfaceIndex)->setDiffuseColor(Eigen::Vector3f(0.0, 1.0, 0.0));
		gSimulation->addAttachmentConstraint(fixedTetrahedralIndex, mVertices[fixedTetrahedralIndex]);
	}
	///////////////////////////////////////////////////////////////////////////
	//find dragged handles section
	///////////////////////////////////////////////////////////////////////////
	do { std::getline(file, s); } while (s.substr(0, 17).compare("# Dragged Handles"));
	file >> s; assert(s.compare("Handles") == 0);
	int numDraggedHandles;
	std::vector<int> draggedHandles;
	file >> numDraggedHandles;
	for (int i = 0; i < numDraggedHandles; i++) {
		int draggedSurfaceIndex;
		file >> draggedSurfaceIndex;
		int draggedTetrahedralIndex = mSkinSurface2Tetrahedral[draggedSurfaceIndex];
		mSkinSurfaceMesh.getHandles(draggedSurfaceIndex)->drag();
		double x, y, z;
		file >> x >> y >> z;
		Eigen::Vector3d pos(0, 0, 0);
		pos << x, y, z;
		mVertices[draggedTetrahedralIndex] = pos;
		mSkinSurfaceVertices[draggedSurfaceIndex] = pos;
		mSkinSurfaceMesh.getHandles(draggedSurfaceIndex)->setDiffuseColor(Eigen::Vector3f(1.0, 1.0, 0.0));
		gSimulation->addAttachmentConstraint(draggedTetrahedralIndex, mVertices[draggedTetrahedralIndex]);
	}
	mSkinSurfaceMesh.createAttrib(mSkinSurfaceVertices, mSkinSurfaceTriangles);
}

bool TetrahedralArmMesh::computeFiberDirection() {
	Eigen::MatrixXd V, C, W;
	Eigen::MatrixXi T;
	Eigen::VectorXd ww;
	V.conservativeResize(mBicepTetrahedralVertices.size(), 3);
	for (int i = 0; i < mBicepTetrahedralVertices.size(); i++) {
		V.row(i) = mBicepTetrahedralVertices[i];
	}
	T.conservativeResize(mBicepTets.size(), 4);
	for (int i = 0; i < mBicepTets.size(); i++) {
		T.row(i) = mBicepTets[i].getRegionIndices();
	}
	//tendon of bicep
	C.conservativeResize(3, 3);
	int p1 = findNodeCorrelationFromFile("../model/adjusted/mesh/bicep.1.node", 67);
	int p2 = findNodeCorrelationFromFile("../model/adjusted/mesh/bicep.1.node", 27);
	int p3 = findNodeCorrelationFromFile("../model/adjusted/mesh/bicep.1.node", 14);
	C.row(0) = mBicepTetrahedralVertices[p1];
	C.row(1) = mBicepTetrahedralVertices[p2];
	C.row(2) = mBicepTetrahedralVertices[p3];
	Eigen::VectorXi P;
	P.conservativeResize(3, 1);
	P(0) = 0;
	P(1) = 1;
	P(2) = 2;

	// List of boundary indices (aka fixed value indices into VV)
	Eigen::VectorXi b;
	// List of boundary conditions of each weight function
	Eigen::MatrixXd bc;
	igl::boundary_conditions(V, T, C, P, Eigen::MatrixXi(), Eigen::MatrixXi(), b, bc);

	std::cout << "b = " << std::endl << b << std::endl;
	std::cout << "bc = " << std::endl << bc << std::endl;

	// compute BBW weights matrix
	igl::BBWData bbw_data;
	// only a few iterations for sake of demo
	bbw_data.active_set_params.max_iter = 16;
	bbw_data.verbosity = 2;
	if (!igl::bbw(V, T, b, bc, bbw_data, W))
	{
		return false;
	}

	std::cout << "mBicepTetrahedralVertices.size() = " << mBicepTetrahedralVertices.size() << std::endl;
	std::cout << "W.rows() = " << W.rows() << std::endl;
	//std::cout << "W = " << std::endl << W << std::endl;
	//std::cout << "W = " << std::endl << W.rowwise().sum() << std::endl;
	//std::cout << "ww = " << std::endl << ww << std::endl;

	ww.conservativeResize(W.rows());
	for (int i = 0; i < W.rows(); i++) {
		ww(i) = W(i, 1) + W(i, 2);
		//std::cout << "W = " << W.row(i).transpose() << " ww = " << ww(i) << std::endl;
	}
	for (int i = 0; i < mBicepTets.size(); i++) {
		Eigen::Matrix3d A;
		Eigen::Vector4i indices = mBicepTets[i].getRegionIndices();
		Eigen::Vector3d v0 = mBicepTetrahedralVertices[indices(0)];
		Eigen::Vector3d v1 = mBicepTetrahedralVertices[indices(1)];
		Eigen::Vector3d v2 = mBicepTetrahedralVertices[indices(2)];
		Eigen::Vector3d v3 = mBicepTetrahedralVertices[indices(3)];
		A.row(0) = v1 - v0;
		A.row(1) = v2 - v0;
		A.row(2) = v3 - v0;

		Eigen::Vector3d b;
		b(0) = ww(indices(1)) - ww(indices(0));
		b(1) = ww(indices(2)) - ww(indices(0));
		b(2) = ww(indices(3)) - ww(indices(0));

		Eigen::Vector3d x = A.colPivHouseholderQr().solve(b);
		double relative_error = (A*x - b).norm() / b.norm(); // norm() is L2 norm
		assert(relative_error < 1e-8);
		x.normalize();
		mBicepTets[i].setFiberDirection(x.x(), x.y(), x.z());
		//std::cout << "x = " << x.transpose() << std::endl;
	}

	writeFiberDirection(mBicepTets);
}

void TetrahedralArmMesh::writeFiberDirection(const std::vector<Tet> &muscleTets) {
	std::ofstream file;
	std::string filename = "../model/bicep_fiber_direction.txt";
	file.open(filename);

	if (!file.is_open()) {
		std::cout << "Could not open file" << filename << std::endl;
		exit(1);
	}

	std::cout << "Writing file " << filename << std::endl;

	file << "fiberDirections " << muscleTets.size() << std::endl;
	for (int i = 0; i < muscleTets.size(); i++) {
		file << muscleTets[i].getFiberDirection().transpose() << std::endl;
	}
	file.close();

}

void TetrahedralArmMesh::loadFiberDirectionFromFile(const std::string &filename, std::vector<Tet> &muscleTets) {
	std::ifstream file;
	file.open(filename);

	if (!file.is_open()) {
		std::cout << "Could not open file" << filename << std::endl;
		exit(1);
	}

	std::cout << "Loading file " << filename << std::endl;
	std::string s;
	int numTets;
	file >> s >> numTets;
	assert(s == "fiberDirections");
	assert(numTets == muscleTets.size());
	for (int i = 0; i < numTets; i++) {
		double x, y, z;
		file >> x >> y >> z;
		muscleTets[i].setFiberDirection(x, y, z);
		mTetrahedra[muscleTets[i].getIndexOfWholeMesh()].setFiberDirection(x, y, z);
	}
}

void TetrahedralArmMesh::saveObjs() {
	mUlnaSurfaceMesh.saveObj2File("../model/outputObj/ulna.obj");
	mRadiusSurfaceMesh.saveObj2File("../model/outputObj/radius.obj");
	//mHumerusSurfaceMesh.saveObj2File("../model/outputObj/humerus.obj");
	//mBicepSurfaceMesh.saveObj2File("../model/outputObj/bicep.obj");
	//mTricepSurfaceMesh.saveObj2File("../model/outputObj/tricep.obj");
	mSkinSurfaceMesh.saveObj2File("../model/outputObj/skin.obj");
}


