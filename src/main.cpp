#include <nanogui/opengl.h>
#include <nanogui/glutil.h>
#include <iostream>
#include <string>

// Includes for the GLTexture class.
#include <cstdint>
#include <memory>
#include <utility>
#include <thread>

#include "application.h"
#include "simulation.h"

#if defined(__GNUC__)
#  pragma GCC diagnostic ignored "-Wmissing-field-initializers"
#endif
#if defined(_WIN32)
#  pragma warning(push)
#  pragma warning(disable: 4457 4456 4005 4312)
#endif

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#if defined(_WIN32)
#  pragma warning(pop)
#endif
#if defined(_WIN32)
#  if defined(APIENTRY)
#    undef APIENTRY
#  endif
#  include <windows.h>
#endif

using std::cout;
using std::cerr;
using std::endl;
using std::string;
using std::vector;
using std::pair;
using std::to_string;

extern Simulation *gSimulation;
extern std::mutex gMutex;
extern std::condition_variable gIntegrateCondVar;
extern std::condition_variable gRenderCondVar;
extern bool gDone;
extern bool gIntegrateNotified;
extern bool gRenderNotified;

int render() {
	try {
		nanogui::init();

		/* scoped variables */ {
			nanogui::ref<Application> app = new Application();
			app->drawAll();
			app->setVisible(true);
			nanogui::mainloop();
		}

		nanogui::shutdown();
	}
	catch (const std::runtime_error &e) {
		std::string error_msg = std::string("Caught a fatal error: ") + std::string(e.what());
#if defined(_WIN32)
		MessageBoxA(nullptr, error_msg.c_str(), NULL, MB_ICONERROR | MB_OK);
#else
		std::cerr << error_msg << endl;
#endif
		return -1;
	}
}

void integrate() {
	std::unique_lock<std::mutex> lock(gMutex);
	while (!gDone) {
		while (!gIntegrateNotified) { //loop to avoid spurious wakeups
			gIntegrateCondVar.wait(lock);
		}
		if (gSimulation) {
			gSimulation->updateVertices();
			//double alpha = 1.0;
			//gSimulation->contractBicep(alpha);
			int step = 100;
			for (int i = 0; i < step; i++) {
				gSimulation->integrateImplicitEulerNewton();
				gRenderNotified = true;
				gRenderCondVar.notify_one();
			}


		}
		gIntegrateNotified = false;
	}
}

int main(int /* argc */, char ** /* argv */) {

	std::thread renderThread(render);
	std::thread integrateThread(integrate);
	renderThread.join();
	integrateThread.join();

    return 0;
}
