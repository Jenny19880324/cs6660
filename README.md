# How to build MuscleActivation #

###############################################################################
## On Windows10 Visual Studio 2015 ##
###############################################################################
Install Microsoft Visual C++ Redistributable Packages for Visual Studio 2015 Update 3 to prevent crashes and memory corruption on multi-processor machines caused by std::string class
https://support.microsoft.com/en-us/help/2977003/the-latest-supported-visual-c-downloads
https://www.microsoft.com/en-us/download/confirmation.aspx?id=53840

Build MuscleActivation
1 cd build, cmake .. -G "Visual Studio 14 Win64" (or you might have to type cmake .. -G "Visual Studio 14 2015 Win64", use cmake --help to find out)
2 open MuscleActivation.sln,
build and run

to build the debug fiberDirection.exe, you need to put /bigobj in Project properties->C/C++/Command Line/Additional Options. 
See https://msdn.microsoft.com/en-us/library/ms173499.aspx for details



###############################################################################
## On Mac OSX ##
###############################################################################
Install Nanogui
1. git clone --recursive git@github.com:wjakob/nanogui.git
2. In nanogui, mkdir build, cd build, cmake .. && make && make install
   then .h is copied to /usr/local/include/nanogui
3 cp nanogui/ext/nanovg/src/nanovg.h /usr/local/include/nanovg.h
  cp nanogui/ext/nanovg/src/stb_image.h /usr/local/include/stb_image.h

Install Google Test
1. git clone git@github.com:google/googletest.git
2. In googletest, mkdir build, cd build, cmake .. && make && make install
then gtest is installed in /usr/local/include/gtest /usr/local/lib/gtest

Build MuscleActivation
1 cd build, cmake ..
2 make, ./MuscleActivation

How to run unit test
1 cd build, cmake ..
2 make UnitTest
3 ./UnitTest --gtest_filter=SimulationTest.ifCorrectSingularValueDecomposition




# find_correlation.cpp, tet_correlation.txt, and tri_correlation.txt #

Given the .node and .face files for the various meshes and the .mesh and .node files of the final mesh, find_correlation.cpp will output tet_correlation.txt with the corresponding indices of the tets (where each index making up the tet is in the same mesh object) in arm.2.mesh and tri_correlation.txt with the corresponding vertex indices of the triangles of arm.2.node. It is very important that the .node, .face, .mesh files are in the same directory as the script and that the number of points and faces and the file names are updated accordingly in the correct variable (ulna_file, ulna_face_file, NUM_ULNA_POINTS, and NUM_ULNA_FACES for the ulna mesh.)

The format of tet_correlation.txt is as follows:



```
#!txt

# Tet of Ulna
Ulna Vertices numIndices
index1 index2 ....
Ulna Tets numIndices
index3 index4 ....
# Tet of Radius
Radius Vertices numIndices
index5 index6 ....
Radius Tets numIndices
index7 index8 ....
# Tet of Humerus
Humerus Vertices numIndices
index9 index10 ....
Humerus Tets numIndices
index11 index12 ....
# Tet of Bicep
Bicep Vertices numIndices
index13 index14 ....
Bicep Tets numIndices
index15 index16 ....
# Tet of Tricep
Tricep Vertices numIndices
index17 index18 ....
Tricep Tets numIndices
index19 index20 ....
# Tet of Skin
Skin Vertices numIndices
index21 index22 ....
Skin Tets numIndices
index23 index24 ....
```

The format of tri_correlation.txt is as follows:

```
#!txt

# Triangles in Ulna
Ulna numTriangles
index1 index2 index3
index4 index5 index6
....
# Triangles in Radius
Radius numTriangles
index7 index8 index9
index10 index11 index12
....
# Triangles in Humerus
Humerus numTriangles
index13 index14 index15
index16 index17 index18
....
# Triangles in Bicep
Bicep numTriangles
index19 index20 index21
index22 index23 index24
....
# Triangles in Tricep
Tricep numTriangles
index25 index26 index27
index28 index29 index30
....
```