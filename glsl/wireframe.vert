#version 330
layout(location = 0) in vec3 position;
layout(location = 1) in vec3 vertexColor;

uniform mat4 object2World;
uniform mat4 modelViewProjection;

out vec3 color;

void main() {
	vec4 pos4 = object2World * vec4(position, 1.0);
	gl_Position = modelViewProjection * pos4;
	color = vertexColor;
}
