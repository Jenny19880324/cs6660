#version 330
layout(location = 0) in vec3 position;
layout(location = 1) in vec3 inputNormal;

out vec3 normalInterp;
out vec3 vertPos;

uniform mat4 object2World;
uniform mat4 modelViewProjection;
uniform mat4 modelView;
uniform mat4 normalTransform;

void main() {
	vec4 pos4 = object2World * vec4(position, 1.0);
	gl_Position = modelViewProjection * pos4;
	vec4 vertPos4 = modelView * pos4;
	vertPos = vec3(vertPos4) / vertPos4.w;
	normalInterp = (normalTransform * vec4(inputNormal, 0.0)).xyz;
}
