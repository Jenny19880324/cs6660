#version 330
uniform sampler2DRect TempTex;
out vec4 fragColor;

void main(void)
{
	fragColor = texture2DRect(TempTex, gl_FragCoord.xy);
}
