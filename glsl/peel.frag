#version 330

uniform sampler2DRect DepthTex;
uniform float Alpha;
uniform float Ns; //Specular exponent
uniform vec3 Ka; //Ambient color;
uniform vec3 Kd; //Diffuse color;
uniform vec3 Ks; //Specular color;
uniform vec3 lightPosition;

in vec3 normalInterp;
in vec3 vertPos;
out vec4 fragColor;

vec4 ShadeFragment(void){
    vec3 normal = normalize(normalInterp);
    vec3 lightDir = normalize(lightPosition - vertPos);
    vec3 viewDir = normalize(-vertPos);
    vec3 halfDir = normalize(lightDir + viewDir);

    float lambertian = max(dot(lightDir, normal), 0.0);
    float specAngle = max(dot(halfDir, normal), 0.0);
    float specular = pow(specAngle, Ns);
    vec4 color = vec4(Ka, 1.0) + 
                vec4(Kd, 1.0) * lambertian + 
                vec4(Ks, 1.0) * specular;
	color.a = Alpha;
				
	return color;
}

void main(void)
{
	// Bit-exact comparison between FP32 z-buffer and fragment depth
	float frontDepth = texture2DRect(DepthTex, gl_FragCoord.xy).r;
	if (gl_FragCoord.z <= frontDepth) {
		discard;
	}
	
	// Shade all the fragments behind the z-buffer
	vec4 color = ShadeFragment();
	fragColor = vec4(color.rgb * color.a, color.a);
}





