#version 330
in vec3 normalInterp;
in vec3 vertPos;
in vec2 texCoord;

out vec4 fragColor;

uniform float Ns; //Specular exponent
uniform vec3 Ka; //Ambient color;
uniform vec3 Kd; //Diffuse color;
uniform vec3 Ks; //Specular color;
uniform sampler2D map_Kd; //Diffuse color texture map

uniform vec3 lightPosition;

void main(void){
    vec3 normal = normalize(normalInterp);
    vec3 lightDir = normalize(lightPosition - vertPos);
    vec3 viewDir = normalize(-vertPos);
    vec3 halfDir = normalize(lightDir + viewDir);

    float lambertian = max(dot(lightDir, normal), 0.0);
    float specAngle = max(dot(halfDir, normal), 0.0);
    float specular = pow(specAngle, Ns);
    
    fragColor = vec4(Ka, 0.3) + 
                vec4(Kd, 0.3) * lambertian * texture(map_Kd, texCoord) + 
                vec4(Ks, 0.3) * specular;
				
	fragColor = vec4(Ka, 1.0) + 
			vec4(Kd, 1.0) * lambertian * texture(map_Kd, texCoord) + 
			vec4(Ks, 1.0) * specular;
}
