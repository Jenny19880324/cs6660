#version 330

uniform sampler2DRect ColorTex;
uniform vec3 BackgroundColor;

out vec4 fragColor;
void main(void)
{
	vec4 frontColor = texture2DRect(ColorTex, gl_FragCoord.xy);
	fragColor.rgb = frontColor.rgb + BackgroundColor * frontColor.a;
	fragColor = vec4(fragColor.rgb, 1.0);
}
